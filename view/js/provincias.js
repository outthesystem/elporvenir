

var provincia_list = [
   {value: 'Ciudad Autónoma de Buenos Aires'},
   {value: 'Buenos Aires'},
   {value: 'Catamarca'},
   {value: 'Chaco'},
   {value: 'Chubut'},
   {value: 'Córdoba'},
   {value: 'Corrientes'},
   {value: 'Entre Ríos'},
   {value: 'Formosa'},
   {value: 'Jujuy'},
   {value: 'La Pampa'},
   {value: 'La Rioja'},
   {value: 'Mendoza'},
   {value: 'Misiones'},
   {value: 'Neuquén'},
   {value: 'Río Negro'},
   {value: 'Salta'},
   {value: 'San Juan'},
   {value: 'San Luis'},
   {value: 'Santa Cruz'},
   {value: 'Santa Fe'},
   {value: 'Santiago del Estero'},
   {value: 'Tierra del Fuego'},
   {value: 'Tucumán'},
];

$(document).ready(function() {
   $("#ac_provincia, #ac_provincia2").autocomplete({
      lookup: provincia_list,
   });
});
