<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_model('cliente.php');
require_model('cliente_sms.php');
require_model('factura_cliente.php');
require_model('servicio_cliente.php');

/**
 * Description of sms
 *
 * @author Miguel San Martin cosouth.battle@gmail.com
 */

class clientes_sms extends fs_controller
{
    public $cliente;
    public $factura;
    public $smsc;
    public $apismsc;
    public $usersmsc;
    public $addNumero;    
    public $setMensaje;
    public $codcliente;
    public $paramsms;

    public function __construct() {
        parent::__construct(__CLASS__, 'Enviar SMS', '');
    }
    
    protected function private_core() 
    {
    
    $fsvar = new fs_var();
      $this->paramsms = array(
          'usersmsc' => '',
          'apikey' => '',
      );
      $this->paramsms = $fsvar->array_get($this->paramsms, FALSE);
      
      $this->share_extension();
       
      $fact0 = new factura_cliente();
      $this->factura = FALSE;
      $cliente = new cliente();
      $this->cliente = FALSE;
      $servicio = new servicio_cliente();
      $this->servicio = FALSE;
      if( isset($_REQUEST['id']) )
      {
      $this->factura = $fact0->get($_REQUEST['id']);
      $this->servicio = $servicio->get($_REQUEST['id']);
      $this->cliente = $cliente->get($this->factura->codcliente);
      $this->cliente = $cliente->get($this->servicio->codcliente);
      }
       if( isset($_POST['codcliente']) )
      {
         $this->cliente = $cliente->get( $_POST['codcliente'] );
      }
      else if( isset($_GET['cod']) )
      {
         $this->cliente = $cliente->get($_GET['cod']);
      }
    // almacenamos api y user
     if (isset($_POST["usersmsc"]))
    {
    $fsvar1 = new fs_var();
    $this->dato = $_POST['usersmsc'];
    $fsvar1->simple_save('usersmsc', $this->dato);
    $this->dato2 = $_POST['apikey'];
    $fsvar1->simple_save('apikey', $this->dato2);
    $this->new_message("Parámetros configurados correctamente.");      
   }
   
    $fsvar->simple_get('apikey');
    $fsvar->simple_get('usersmsc');

    //api de SMS
      try 
      {
          
    $smsc = new cliente_sms($fsvar->simple_get('usersmsc'), $fsvar->simple_get('apikey'));

    // Estado del servicio
    $this->new_advice('El estado del servicio es '.($smsc->getEstado()?'OK':'CAIDO').'. Quedan: '.$smsc->getSaldo().' sms. ');
    if (isset($_POST["addNumero"]))
    {
    $smsc->addNumero($_POST['addNumero']);
    
    }
    if (isset($_POST["addNumero"]))
    {
    $smsc->setMensaje($_POST['setMensaje']);
    }            
    if ($smsc->enviar())
        $this->new_message('Mensaje Enviado.');
    }     
    catch (Exception $e) 
    {
        echo  '';
    }
      
    }
    
    private function share_extension()
   {
      $fsxet = new fs_extension();
      $fsxet->name = 'tab_sms';
      $fsxet->from = __CLASS__;
      $fsxet->to = 'ventas_factura';
      $fsxet->type = 'tab';
      $fsxet->text = '<span class="glyphicon glyphicon-send" aria-hidden="true"></span>'
              . '<span class="hidden-xs">&nbsp; Enviar SMS</span>';
      $fsxet->save();
      
      $fsxet1 = new fs_extension();
      $fsxet1->name = 'tab_sms1';
      $fsxet1->from = __CLASS__;
      $fsxet1->to = 'ventas_servicio';
      $fsxet1->type = 'tab';
      $fsxet1->text = '<span class="glyphicon glyphicon-send" aria-hidden="true"></span>'
              . '<span class="hidden-xs">&nbsp; Enviar SMS</span>';
      $fsxet1->save();
   }
    
}