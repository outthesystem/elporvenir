-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-11-2016 a las 14:37:40
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sartin`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agenciastrans`
--

CREATE TABLE `agenciastrans` (
  `codtrans` varchar(8) COLLATE utf8_bin NOT NULL,
  `nombre` varchar(50) COLLATE utf8_bin NOT NULL,
  `telefono` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `web` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agentes`
--

CREATE TABLE `agentes` (
  `apellidos` varchar(100) COLLATE utf8_bin NOT NULL,
  `ciudad` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `codagente` varchar(10) COLLATE utf8_bin NOT NULL,
  `coddepartamento` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `codpais` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `codpostal` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `dnicif` varchar(15) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `fax` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `idprovincia` int(11) DEFAULT NULL,
  `idusuario` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `irpf` double DEFAULT NULL,
  `nombre` varchar(50) COLLATE utf8_bin NOT NULL,
  `nombreap` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `porcomision` double DEFAULT NULL,
  `provincia` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `seg_social` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `cargo` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `banco` varchar(34) COLLATE utf8_bin DEFAULT NULL,
  `f_alta` date DEFAULT NULL,
  `f_baja` date DEFAULT NULL,
  `f_nacimiento` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `agentes`
--

INSERT INTO `agentes` (`apellidos`, `ciudad`, `codagente`, `coddepartamento`, `codpais`, `codpostal`, `direccion`, `dnicif`, `email`, `fax`, `idprovincia`, `idusuario`, `irpf`, `nombre`, `nombreap`, `porcomision`, `provincia`, `telefono`, `seg_social`, `cargo`, `banco`, `f_alta`, `f_baja`, `f_nacimiento`) VALUES
('General', NULL, '1', NULL, NULL, NULL, NULL, '000000000', NULL, NULL, NULL, NULL, NULL, 'Administrador', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `albaranescli`
--

CREATE TABLE `albaranescli` (
  `apartado` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `cifnif` varchar(20) COLLATE utf8_bin NOT NULL,
  `ciudad` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `codagente` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codalmacen` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `codcliente` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `coddir` int(11) DEFAULT NULL,
  `coddivisa` varchar(3) COLLATE utf8_bin NOT NULL,
  `codejercicio` varchar(4) COLLATE utf8_bin NOT NULL,
  `codigo` varchar(20) COLLATE utf8_bin NOT NULL,
  `codpago` varchar(10) COLLATE utf8_bin NOT NULL,
  `codpais` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `codpostal` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codserie` varchar(2) COLLATE utf8_bin NOT NULL,
  `direccion` varchar(100) COLLATE utf8_bin NOT NULL,
  `fecha` date NOT NULL,
  `hora` time DEFAULT '00:00:00',
  `femail` date DEFAULT NULL,
  `idalbaran` int(11) NOT NULL,
  `idfactura` int(11) DEFAULT NULL,
  `idprovincia` int(11) DEFAULT NULL,
  `irpf` double NOT NULL DEFAULT '0',
  `neto` double NOT NULL DEFAULT '0',
  `nombrecliente` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `numero` varchar(12) COLLATE utf8_bin NOT NULL,
  `numero2` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `observaciones` text COLLATE utf8_bin,
  `porcomision` double DEFAULT NULL,
  `provincia` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `ptefactura` tinyint(1) NOT NULL DEFAULT '1',
  `recfinanciero` double NOT NULL DEFAULT '0',
  `tasaconv` double NOT NULL DEFAULT '1',
  `total` double NOT NULL DEFAULT '0',
  `totaleuros` double NOT NULL DEFAULT '0',
  `totalirpf` double NOT NULL DEFAULT '0',
  `totaliva` double NOT NULL DEFAULT '0',
  `totalrecargo` double NOT NULL DEFAULT '0',
  `codtrans` varchar(8) COLLATE utf8_bin DEFAULT NULL,
  `codigoenv` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `nombreenv` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `apellidosenv` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `direccionenv` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `codpostalenv` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `ciudadenv` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `provinciaenv` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `albaranesprov`
--

CREATE TABLE `albaranesprov` (
  `cifnif` varchar(20) COLLATE utf8_bin NOT NULL,
  `codagente` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codalmacen` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `coddivisa` varchar(3) COLLATE utf8_bin NOT NULL,
  `codejercicio` varchar(4) COLLATE utf8_bin NOT NULL,
  `codigo` varchar(20) COLLATE utf8_bin NOT NULL,
  `codpago` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codproveedor` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `codserie` varchar(2) COLLATE utf8_bin NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL DEFAULT '00:00:00',
  `idalbaran` int(11) NOT NULL,
  `idfactura` int(11) DEFAULT NULL,
  `irpf` double NOT NULL DEFAULT '0',
  `neto` double NOT NULL DEFAULT '0',
  `nombre` varchar(100) COLLATE utf8_bin NOT NULL,
  `numero` varchar(12) COLLATE utf8_bin NOT NULL,
  `numproveedor` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `observaciones` text COLLATE utf8_bin,
  `ptefactura` tinyint(1) NOT NULL DEFAULT '1',
  `recfinanciero` double NOT NULL DEFAULT '0',
  `tasaconv` double NOT NULL DEFAULT '1',
  `total` double NOT NULL DEFAULT '0',
  `totaleuros` double NOT NULL DEFAULT '0',
  `totalirpf` double NOT NULL DEFAULT '0',
  `totaliva` double NOT NULL DEFAULT '0',
  `totalrecargo` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacenes`
--

CREATE TABLE `almacenes` (
  `apartado` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codalmacen` varchar(4) COLLATE utf8_bin NOT NULL,
  `codpais` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `codpostal` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `contacto` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `fax` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `idprovincia` int(11) DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_bin NOT NULL,
  `observaciones` text COLLATE utf8_bin,
  `poblacion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `porpvp` double DEFAULT NULL,
  `provincia` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `telefono` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `tipovaloracion` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `almacenes`
--

INSERT INTO `almacenes` (`apartado`, `codalmacen`, `codpais`, `codpostal`, `contacto`, `direccion`, `fax`, `idprovincia`, `nombre`, `observaciones`, `poblacion`, `porpvp`, `provincia`, `telefono`, `tipovaloracion`) VALUES
(NULL, 'ALG', NULL, '', '', '', '', NULL, 'ALMACEN GENERAL', NULL, '', NULL, NULL, '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `factualizado` date DEFAULT NULL,
  `bloqueado` tinyint(1) DEFAULT '0',
  `equivalencia` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `idsubcuentairpfcom` int(11) DEFAULT NULL,
  `idsubcuentacom` int(11) DEFAULT NULL,
  `stockmin` double DEFAULT '0',
  `observaciones` text COLLATE utf8_bin,
  `codbarras` varchar(18) COLLATE utf8_bin DEFAULT NULL,
  `codimpuesto` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `stockfis` double DEFAULT '0',
  `stockmax` double DEFAULT '0',
  `costemedio` double DEFAULT '0',
  `preciocoste` double DEFAULT '0',
  `tipocodbarras` varchar(8) COLLATE utf8_bin DEFAULT 'Code39',
  `nostock` tinyint(1) DEFAULT NULL,
  `codsubcuentacom` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `descripcion` text COLLATE utf8_bin NOT NULL,
  `codsubcuentairpfcom` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `secompra` tinyint(1) DEFAULT NULL,
  `codfamilia` varchar(8) COLLATE utf8_bin DEFAULT NULL,
  `imagen` text COLLATE utf8_bin,
  `controlstock` tinyint(1) DEFAULT '0',
  `referencia` varchar(18) COLLATE utf8_bin NOT NULL,
  `tipo` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `pvp` double DEFAULT '0',
  `sevende` tinyint(1) DEFAULT NULL,
  `publico` tinyint(1) DEFAULT '0',
  `partnumber` varchar(38) COLLATE utf8_bin DEFAULT NULL,
  `trazabilidad` tinyint(1) DEFAULT '0',
  `codfabricante` varchar(8) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulosprov`
--

CREATE TABLE `articulosprov` (
  `id` int(11) NOT NULL,
  `referencia` varchar(18) COLLATE utf8_bin DEFAULT NULL,
  `codproveedor` varchar(6) COLLATE utf8_bin NOT NULL,
  `refproveedor` varchar(25) COLLATE utf8_bin NOT NULL,
  `descripcion` text COLLATE utf8_bin,
  `precio` double DEFAULT NULL,
  `dto` double DEFAULT NULL,
  `codimpuesto` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `stock` double DEFAULT NULL,
  `nostock` tinyint(1) DEFAULT '1',
  `nombre` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `coddivisa` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `codbarras` varchar(18) COLLATE utf8_bin DEFAULT NULL,
  `partnumber` varchar(38) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `atributos`
--

CREATE TABLE `atributos` (
  `codatributo` varchar(8) COLLATE utf8_bin NOT NULL,
  `nombre` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `capitalimpagado` double DEFAULT NULL,
  `cifnif` varchar(20) COLLATE utf8_bin NOT NULL,
  `codagente` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codcliente` varchar(6) COLLATE utf8_bin NOT NULL,
  `codcontacto` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `codcuentadom` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `codcuentarem` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `coddivisa` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `codedi` varchar(17) COLLATE utf8_bin DEFAULT NULL,
  `codgrupo` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `codpago` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codserie` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuenta` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codtiporappel` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `contacto` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `copiasfactura` int(11) DEFAULT NULL,
  `debaja` tinyint(1) DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `fax` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `fechabaja` date DEFAULT NULL,
  `fechaalta` date DEFAULT NULL,
  `idsubcuenta` int(11) DEFAULT NULL,
  `ivaincluido` tinyint(1) DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_bin NOT NULL,
  `razonsocial` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `observaciones` text COLLATE utf8_bin,
  `recargo` tinyint(1) DEFAULT NULL,
  `regimeniva` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `riesgoalcanzado` double DEFAULT NULL,
  `riesgomax` double DEFAULT NULL,
  `telefono1` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `telefono2` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `tipoidfiscal` varchar(25) COLLATE utf8_bin NOT NULL DEFAULT 'NIF',
  `personafisica` tinyint(1) DEFAULT '1',
  `web` varchar(250) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`capitalimpagado`, `cifnif`, `codagente`, `codcliente`, `codcontacto`, `codcuentadom`, `codcuentarem`, `coddivisa`, `codedi`, `codgrupo`, `codpago`, `codserie`, `codsubcuenta`, `codtiporappel`, `contacto`, `copiasfactura`, `debaja`, `email`, `fax`, `fechabaja`, `fechaalta`, `idsubcuenta`, `ivaincluido`, `nombre`, `razonsocial`, `observaciones`, `recargo`, `regimeniva`, `riesgoalcanzado`, `riesgomax`, `telefono1`, `telefono2`, `tipoidfiscal`, `personafisica`, `web`) VALUES
(NULL, '', NULL, '000001', NULL, NULL, NULL, 'ARS', NULL, NULL, 'CONT', NULL, NULL, NULL, NULL, NULL, 0, '', '', NULL, '2016-11-29', NULL, NULL, 'YPF', 'YPF', '', 0, 'General', NULL, NULL, '', '', 'Dni/Cuit', 1, ''),
(NULL, '', NULL, '000002', NULL, NULL, NULL, 'ARS', NULL, NULL, 'CONT', 'A', NULL, NULL, NULL, NULL, 0, '', '', NULL, '2016-11-29', NULL, NULL, 'SNP', 'SNP', '', 0, 'General', NULL, NULL, '', '', 'Dni/Cuit', 1, ''),
(NULL, '', NULL, '000003', NULL, NULL, NULL, 'ARS', NULL, NULL, 'CONT', 'A', NULL, NULL, NULL, NULL, 0, '', '', NULL, '2016-11-29', NULL, NULL, 'PAE', 'PAE', '', 0, 'General', NULL, NULL, '', '', 'Dni/Cuit', 1, ''),
(NULL, '', NULL, '000004', NULL, NULL, NULL, 'ARS', NULL, NULL, 'CONT', 'A', NULL, NULL, NULL, NULL, 0, '', '', NULL, '2016-11-29', NULL, NULL, 'GOLCORP', 'GOLCORP', '', 0, 'General', NULL, NULL, '', '', 'Dni/Cuit', 1, ''),
(NULL, '', NULL, '000005', NULL, NULL, NULL, 'ARS', NULL, NULL, 'CONT', 'A', NULL, NULL, NULL, NULL, 0, '', '', NULL, '2016-11-29', NULL, NULL, 'JOMAR', 'JOMAR', '', 0, 'General', NULL, NULL, '', '', 'Dni/Cuit', 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratoservicioscli`
--

CREATE TABLE `contratoservicioscli` (
  `idcontrato` int(11) NOT NULL,
  `codcliente` varchar(6) COLLATE utf8_bin NOT NULL,
  `codagente` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `fecha_alta` date NOT NULL,
  `fecha_renovacion` date NOT NULL,
  `observaciones` text COLLATE utf8_bin,
  `codpago` varchar(10) COLLATE utf8_bin NOT NULL,
  `importe_anual` double DEFAULT NULL,
  `periodo` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `fsiguiente_servicio` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `co_asientos`
--

CREATE TABLE `co_asientos` (
  `codejercicio` varchar(4) COLLATE utf8_bin NOT NULL,
  `codplanasiento` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `concepto` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `documento` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `editable` tinyint(1) NOT NULL,
  `fecha` date NOT NULL,
  `idasiento` int(11) NOT NULL,
  `idconcepto` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `importe` double DEFAULT NULL,
  `numero` int(11) NOT NULL,
  `tipodocumento` varchar(25) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `co_conceptospar`
--

CREATE TABLE `co_conceptospar` (
  `concepto` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `idconceptopar` varchar(4) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `co_cuentas`
--

CREATE TABLE `co_cuentas` (
  `codbalance` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codcuenta` varchar(6) COLLATE utf8_bin NOT NULL,
  `codejercicio` varchar(4) COLLATE utf8_bin NOT NULL,
  `codepigrafe` varchar(6) COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `idcuenta` int(11) NOT NULL,
  `idcuentaesp` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `idepigrafe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `co_cuentasesp`
--

CREATE TABLE `co_cuentasesp` (
  `codcuenta` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuenta` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `idcuentaesp` varchar(6) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `co_cuentasesp`
--

INSERT INTO `co_cuentasesp` (`codcuenta`, `codsubcuenta`, `descripcion`, `idcuentaesp`) VALUES
(NULL, NULL, 'Cuentas de acreedores', 'ACREED'),
(NULL, NULL, 'Cuentas de caja', 'CAJA'),
(NULL, NULL, 'Cuentas de diferencias negativas de cambio', 'CAMNEG'),
(NULL, NULL, 'Cuentas de diferencias positivas de cambio', 'CAMPOS'),
(NULL, NULL, 'Cuentas de clientes', 'CLIENT'),
(NULL, NULL, 'Cuentas de compras', 'COMPRA'),
(NULL, NULL, 'Devoluciones de compras', 'DEVCOM'),
(NULL, NULL, 'Devoluciones de ventas', 'DEVVEN'),
(NULL, NULL, 'Cuentas por diferencias positivas en divisa extranjera', 'DIVPOS'),
(NULL, NULL, 'Cuentas por diferencias negativas de conversión a la moneda local', 'EURNEG'),
(NULL, NULL, 'Cuentas por diferencias positivas de conversión a la moneda local', 'EURPOS'),
(NULL, NULL, 'Gastos por recargo financiero', 'GTORF'),
(NULL, NULL, 'Ingresos por recargo financiero', 'INGRF'),
(NULL, NULL, 'Cuentas de retenciones IRPF', 'IRPF'),
(NULL, NULL, 'Cuentas de retenciones para proveedores IRPFPR', 'IRPFPR'),
(NULL, NULL, 'Cuentas acreedoras de IVA en la regularización', 'IVAACR'),
(NULL, NULL, 'Cuentas deudoras de IVA en la regularización', 'IVADEU'),
(NULL, NULL, 'IVA en entregas intracomunitarias U.E.', 'IVAEUE'),
(NULL, NULL, 'Cuentas de IVA repercutido', 'IVAREP'),
(NULL, NULL, 'Cuentas de IVA repercutido para clientes exentos de IVA', 'IVAREX'),
(NULL, NULL, 'Cuentas de IVA soportado UE', 'IVARUE'),
(NULL, NULL, 'Cuentas de IVA repercutido en exportaciones', 'IVARXP'),
(NULL, NULL, 'Cuentas de IVA soportado en importaciones', 'IVASIM'),
(NULL, NULL, 'Cuentas de IVA soportado', 'IVASOP'),
(NULL, NULL, 'Cuentas de IVA soportado UE', 'IVASUE'),
(NULL, NULL, 'Cuentas relativas al ejercicio previo', 'PREVIO'),
(NULL, NULL, 'Cuentas de proveedores', 'PROVEE'),
(NULL, NULL, 'Pérdidas y ganancias', 'PYG'),
(NULL, NULL, 'Cuentas de ventas', 'VENTAS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `co_epigrafes`
--

CREATE TABLE `co_epigrafes` (
  `codejercicio` varchar(4) COLLATE utf8_bin NOT NULL,
  `codepigrafe` varchar(6) COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `idepigrafe` int(11) NOT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `idpadre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `co_gruposepigrafes`
--

CREATE TABLE `co_gruposepigrafes` (
  `codejercicio` varchar(4) COLLATE utf8_bin NOT NULL,
  `codgrupo` varchar(6) COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `idgrupo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `co_regiva`
--

CREATE TABLE `co_regiva` (
  `codejercicio` varchar(4) COLLATE utf8_bin NOT NULL,
  `codsubcuentaacr` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuentadeu` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `fechaasiento` date NOT NULL,
  `fechafin` date NOT NULL,
  `fechainicio` date NOT NULL,
  `idasiento` int(11) NOT NULL,
  `idregiva` int(11) NOT NULL,
  `idsubcuentaacr` int(11) DEFAULT NULL,
  `idsubcuentadeu` int(11) DEFAULT NULL,
  `periodo` varchar(8) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `co_secuencias`
--

CREATE TABLE `co_secuencias` (
  `codejercicio` varchar(4) COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `idsecuencia` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_bin NOT NULL,
  `valor` int(11) DEFAULT NULL,
  `valorout` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `co_subcuentas`
--

CREATE TABLE `co_subcuentas` (
  `codcuenta` varchar(6) COLLATE utf8_bin NOT NULL,
  `coddivisa` varchar(3) COLLATE utf8_bin NOT NULL,
  `codejercicio` varchar(4) COLLATE utf8_bin NOT NULL,
  `codimpuesto` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuenta` varchar(15) COLLATE utf8_bin NOT NULL,
  `debe` double NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `haber` double NOT NULL,
  `idcuenta` int(11) DEFAULT NULL,
  `idsubcuenta` int(11) NOT NULL,
  `iva` double NOT NULL,
  `recargo` double NOT NULL,
  `saldo` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `co_subcuentascli`
--

CREATE TABLE `co_subcuentascli` (
  `codcliente` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `codejercicio` varchar(4) COLLATE utf8_bin NOT NULL,
  `codsubcuenta` varchar(15) COLLATE utf8_bin NOT NULL,
  `id` int(11) NOT NULL,
  `idsubcuenta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentasbanco`
--

CREATE TABLE `cuentasbanco` (
  `codsubcuenta` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `descripcion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `iban` varchar(34) COLLATE utf8_bin DEFAULT NULL,
  `codcuenta` varchar(6) COLLATE utf8_bin NOT NULL,
  `swift` varchar(11) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentasbcocli`
--

CREATE TABLE `cuentasbcocli` (
  `descripcion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `swift` varchar(11) COLLATE utf8_bin DEFAULT NULL,
  `ctaentidad` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `iban` varchar(34) COLLATE utf8_bin DEFAULT NULL,
  `agencia` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `entidad` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `codcliente` varchar(6) COLLATE utf8_bin NOT NULL,
  `ctaagencia` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `codcuenta` varchar(6) COLLATE utf8_bin NOT NULL,
  `cuenta` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `principal` tinyint(1) DEFAULT NULL,
  `fmandato` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentasbcopro`
--

CREATE TABLE `cuentasbcopro` (
  `agencia` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `codcuenta` varchar(6) COLLATE utf8_bin NOT NULL,
  `codproveedor` varchar(6) COLLATE utf8_bin NOT NULL,
  `ctaagencia` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `ctaentidad` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `cuenta` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `descripcion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `entidad` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `iban` varchar(34) COLLATE utf8_bin DEFAULT NULL,
  `swift` varchar(11) COLLATE utf8_bin DEFAULT NULL,
  `principal` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_servicios`
--

CREATE TABLE `detalles_servicios` (
  `id` int(11) NOT NULL,
  `descripcion` text COLLATE utf8_bin NOT NULL,
  `idservicio` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time DEFAULT '00:00:00',
  `nick` varchar(12) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `detalles_servicios`
--

INSERT INTO `detalles_servicios` (`id`, `descripcion`, `idservicio`, `fecha`, `hora`, `nick`) VALUES
(1, 'Se ha cambiado el estado a: Pendiente', 6323, '2016-11-29', '09:05:13', 'admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dirclientes`
--

CREATE TABLE `dirclientes` (
  `codcliente` varchar(6) COLLATE utf8_bin NOT NULL,
  `codpais` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `apartado` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `idprovincia` int(11) DEFAULT NULL,
  `provincia` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `ciudad` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `codpostal` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8_bin NOT NULL,
  `domenvio` tinyint(1) DEFAULT NULL,
  `domfacturacion` tinyint(1) DEFAULT NULL,
  `descripcion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `dirclientes`
--

INSERT INTO `dirclientes` (`codcliente`, `codpais`, `apartado`, `idprovincia`, `provincia`, `ciudad`, `codpostal`, `direccion`, `domenvio`, `domfacturacion`, `descripcion`, `id`, `fecha`) VALUES
('000001', 'ARG', '', NULL, '', '', '', '', 1, 1, 'Principal', 1, '2016-11-29'),
('000002', 'ARG', '', NULL, '', '', '', '', 1, 1, 'Principal', 2, '2016-11-29'),
('000003', 'ARG', '', NULL, '', '', '', '', 1, 1, 'Principal', 3, '2016-11-29'),
('000004', 'ARG', '', NULL, '', '', '', '', 1, 1, 'Principal', 4, '2016-11-29'),
('000005', 'ARG', '', NULL, '', '', '', '', 1, 1, 'Principal', 5, '2016-11-29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `divisas`
--

CREATE TABLE `divisas` (
  `bandera` text COLLATE utf8_bin,
  `coddivisa` varchar(3) COLLATE utf8_bin NOT NULL,
  `codiso` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `descripcion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `simbolo` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `tasaconv` double NOT NULL,
  `tasaconv_compra` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `divisas`
--

INSERT INTO `divisas` (`bandera`, `coddivisa`, `codiso`, `descripcion`, `fecha`, `simbolo`, `tasaconv`, `tasaconv_compra`) VALUES
(NULL, 'ARS', '32', 'PESOS (ARG)', NULL, '$', 10.83, NULL),
(NULL, 'CLP', '152', 'PESOS (CLP)', NULL, '$', 755.73, NULL),
(NULL, 'COP', '170', 'PESOS (COP)', NULL, '$', 2573, NULL),
(NULL, 'EUR', '978', 'EUROS', NULL, '€', 1, NULL),
(NULL, 'MXN', '484', 'PESOS (MXN)', NULL, '$', 18.1, NULL),
(NULL, 'PAB', '590', 'BALBOAS', NULL, 'B', 38.17, NULL),
(NULL, 'PEN', '604', 'NUEVOS SOLES', NULL, 'S/.', 3.52, NULL),
(NULL, 'USD', '840', 'DÓLARES EE.UU.', NULL, '$', 1.36, NULL),
(NULL, 'VEF', '937', 'BOLÍVARES', NULL, 'Bs', 38.17, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentosfac`
--

CREATE TABLE `documentosfac` (
  `id` int(11) NOT NULL,
  `ruta` varchar(300) COLLATE utf8_bin NOT NULL,
  `nombre` varchar(250) COLLATE utf8_bin NOT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT '00:00:00',
  `tamano` int(11) DEFAULT NULL,
  `usuario` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `idfactura` int(11) DEFAULT NULL,
  `idalbaran` int(11) DEFAULT NULL,
  `idpedido` int(11) DEFAULT NULL,
  `idpresupuesto` int(11) DEFAULT NULL,
  `idfacturaprov` int(11) DEFAULT NULL,
  `idalbaranprov` int(11) DEFAULT NULL,
  `idpedidoprov` int(11) DEFAULT NULL,
  `idservicio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `documentosfac`
--

INSERT INTO `documentosfac` (`id`, `ruta`, `nombre`, `fecha`, `hora`, `tamano`, `usuario`, `idfactura`, `idalbaran`, `idpedido`, `idpresupuesto`, `idfacturaprov`, `idalbaranprov`, `idpedidoprov`, `idservicio`) VALUES
(2, 'documentos/HZ3DwE_LISTADO DE PERSONAL EL PORVENIR.xlsx', 'LISTADO DE PERSONAL EL PORVENIR.xlsx', '2016-11-29', '09:03:37', 23861, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6328);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ejercicios`
--

CREATE TABLE `ejercicios` (
  `idasientocierre` int(11) DEFAULT NULL,
  `idasientopyg` int(11) DEFAULT NULL,
  `idasientoapertura` int(11) DEFAULT NULL,
  `plancontable` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `longsubcuenta` int(11) DEFAULT NULL,
  `estado` varchar(15) COLLATE utf8_bin NOT NULL,
  `fechafin` date NOT NULL,
  `fechainicio` date NOT NULL,
  `nombre` varchar(100) COLLATE utf8_bin NOT NULL,
  `codejercicio` varchar(4) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `ejercicios`
--

INSERT INTO `ejercicios` (`idasientocierre`, `idasientopyg`, `idasientoapertura`, `plancontable`, `longsubcuenta`, `estado`, `fechafin`, `fechainicio`, `nombre`, `codejercicio`) VALUES
(NULL, NULL, NULL, '08', 10, 'ABIERTO', '2016-12-31', '2016-01-01', '2016', '2016');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `administrador` varchar(100) COLLATE utf8_bin NOT NULL,
  `apartado` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `cifnif` varchar(20) COLLATE utf8_bin NOT NULL,
  `ciudad` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `codalmacen` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `codcuentarem` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `coddivisa` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `codedi` varchar(17) COLLATE utf8_bin DEFAULT NULL,
  `codejercicio` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `codpago` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codpais` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `codpostal` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codserie` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `contintegrada` tinyint(1) DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `fax` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `horario` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `id` int(11) NOT NULL,
  `idprovincia` int(11) DEFAULT NULL,
  `xid` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `lema` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `logo` text COLLATE utf8_bin,
  `nombre` varchar(100) COLLATE utf8_bin NOT NULL,
  `nombrecorto` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `pie_factura` text COLLATE utf8_bin,
  `provincia` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `recequivalencia` tinyint(1) DEFAULT NULL,
  `stockpedidos` tinyint(1) DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `web` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `inicioact` date DEFAULT NULL,
  `regimeniva` varchar(20) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`administrador`, `apartado`, `cifnif`, `ciudad`, `codalmacen`, `codcuentarem`, `coddivisa`, `codedi`, `codejercicio`, `codpago`, `codpais`, `codpostal`, `codserie`, `contintegrada`, `direccion`, `email`, `fax`, `horario`, `id`, `idprovincia`, `xid`, `lema`, `logo`, `nombre`, `nombrecorto`, `pie_factura`, `provincia`, `recequivalencia`, `stockpedidos`, `telefono`, `web`, `inicioact`, `regimeniva`) VALUES
('', '', '000000000', '', 'ALG', NULL, 'ARS', NULL, '0001', 'CONT', 'ARG', '', 'A', 0, 'C/ Falsa, 123', '', '', '', 1, NULL, '0n8dJuBHQc9YDTXZrmywx3NhqFK1pR', '', NULL, 'Empresa 5791 S.L.', 'E-5791', '', '', 0, 0, '', '', '1969-12-31', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados_servicios`
--

CREATE TABLE `estados_servicios` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `color` varchar(6) COLLATE utf8_bin NOT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `albaran` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `estados_servicios`
--

INSERT INTO `estados_servicios` (`id`, `descripcion`, `color`, `activo`, `albaran`) VALUES
(1, 'Pendiente', 'FFFBD9', 1, 0),
(2, 'En proceso', 'D9EDF7', 1, 0),
(100, 'Terminado', 'DFF0D8', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fabricantes`
--

CREATE TABLE `fabricantes` (
  `nombre` varchar(100) COLLATE utf8_bin NOT NULL,
  `codfabricante` varchar(8) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `fabricantes`
--

INSERT INTO `fabricantes` (`nombre`, `codfabricante`) VALUES
('OEM', 'OEM');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturascli`
--

CREATE TABLE `facturascli` (
  `apartado` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `automatica` tinyint(1) DEFAULT NULL,
  `cifnif` varchar(20) COLLATE utf8_bin NOT NULL,
  `ciudad` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `codagente` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codalmacen` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `codcliente` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `coddir` int(11) DEFAULT NULL,
  `coddivisa` varchar(3) COLLATE utf8_bin NOT NULL,
  `codejercicio` varchar(4) COLLATE utf8_bin NOT NULL,
  `codigo` varchar(20) COLLATE utf8_bin NOT NULL,
  `codigorect` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `codpago` varchar(10) COLLATE utf8_bin NOT NULL,
  `codpais` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `codpostal` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codserie` varchar(2) COLLATE utf8_bin NOT NULL,
  `deabono` tinyint(1) DEFAULT '0',
  `direccion` varchar(100) COLLATE utf8_bin NOT NULL,
  `editable` tinyint(1) DEFAULT '0',
  `fecha` date NOT NULL,
  `vencimiento` date DEFAULT NULL,
  `femail` date DEFAULT NULL,
  `hora` time NOT NULL DEFAULT '00:00:00',
  `idasiento` int(11) DEFAULT NULL,
  `idasientop` int(11) DEFAULT NULL,
  `idfactura` int(11) NOT NULL,
  `idfacturarect` int(11) DEFAULT NULL,
  `idpagodevol` int(11) DEFAULT NULL,
  `idprovincia` int(11) DEFAULT NULL,
  `irpf` double NOT NULL,
  `neto` double NOT NULL,
  `nogenerarasiento` tinyint(1) DEFAULT NULL,
  `nombrecliente` varchar(100) COLLATE utf8_bin NOT NULL,
  `numero` varchar(12) COLLATE utf8_bin NOT NULL,
  `numero2` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `observaciones` text COLLATE utf8_bin,
  `pagada` tinyint(1) NOT NULL DEFAULT '0',
  `anulada` tinyint(1) NOT NULL DEFAULT '0',
  `porcomision` double DEFAULT NULL,
  `provincia` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `recfinanciero` double DEFAULT NULL,
  `tasaconv` double NOT NULL,
  `total` double NOT NULL,
  `totaleuros` double NOT NULL,
  `totalirpf` double NOT NULL,
  `totaliva` double NOT NULL,
  `totalrecargo` double DEFAULT NULL,
  `tpv` tinyint(1) DEFAULT NULL,
  `codtrans` varchar(8) COLLATE utf8_bin DEFAULT NULL,
  `codigoenv` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `nombreenv` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `apellidosenv` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `direccionenv` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `codpostalenv` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `ciudadenv` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `provinciaenv` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `idimprenta` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturasprov`
--

CREATE TABLE `facturasprov` (
  `automatica` tinyint(1) DEFAULT NULL,
  `cifnif` varchar(20) COLLATE utf8_bin NOT NULL,
  `codagente` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codalmacen` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `coddivisa` varchar(3) COLLATE utf8_bin NOT NULL,
  `codejercicio` varchar(4) COLLATE utf8_bin NOT NULL,
  `codigo` varchar(20) COLLATE utf8_bin NOT NULL,
  `codigorect` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `codpago` varchar(10) COLLATE utf8_bin NOT NULL,
  `codproveedor` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `codserie` varchar(2) COLLATE utf8_bin NOT NULL,
  `deabono` tinyint(1) DEFAULT '0',
  `editable` tinyint(1) DEFAULT '0',
  `fecha` date NOT NULL,
  `hora` time NOT NULL DEFAULT '00:00:00',
  `idasiento` int(11) DEFAULT NULL,
  `idasientop` int(11) DEFAULT NULL,
  `idfactura` int(11) NOT NULL,
  `idfacturarect` int(11) DEFAULT NULL,
  `idpagodevol` int(11) DEFAULT NULL,
  `irpf` double DEFAULT NULL,
  `neto` double DEFAULT NULL,
  `nogenerarasiento` tinyint(1) DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_bin NOT NULL,
  `numero` varchar(12) COLLATE utf8_bin NOT NULL,
  `numproveedor` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `observaciones` text COLLATE utf8_bin,
  `pagada` tinyint(1) NOT NULL DEFAULT '0',
  `anulada` tinyint(1) NOT NULL DEFAULT '0',
  `recfinanciero` double DEFAULT NULL,
  `tasaconv` double NOT NULL,
  `total` double DEFAULT NULL,
  `totaleuros` double DEFAULT NULL,
  `totalirpf` double DEFAULT NULL,
  `totaliva` double DEFAULT NULL,
  `totalrecargo` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familias`
--

CREATE TABLE `familias` (
  `descripcion` varchar(100) COLLATE utf8_bin NOT NULL,
  `codfamilia` varchar(8) COLLATE utf8_bin NOT NULL,
  `madre` varchar(8) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `familias`
--

INSERT INTO `familias` (`descripcion`, `codfamilia`, `madre`) VALUES
('VARIOS', 'VARI', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formaspago`
--

CREATE TABLE `formaspago` (
  `codpago` varchar(10) COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `genrecibos` varchar(10) COLLATE utf8_bin NOT NULL,
  `codcuenta` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `domiciliado` tinyint(1) DEFAULT NULL,
  `vencimiento` varchar(20) COLLATE utf8_bin DEFAULT '+1month'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `formaspago`
--

INSERT INTO `formaspago` (`codpago`, `descripcion`, `genrecibos`, `codcuenta`, `domiciliado`, `vencimiento`) VALUES
('CONT', 'Al contado', 'Pagados', NULL, 0, '+1month'),
('PAYPAL', 'PayPal', 'Pagados', NULL, 0, '+1week'),
('TRANS', 'Transferencia bancaria', 'Emitidos', NULL, 0, '+1month');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fs_extensions2`
--

CREATE TABLE `fs_extensions2` (
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `page_from` varchar(30) COLLATE utf8_bin NOT NULL,
  `page_to` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `text` text COLLATE utf8_bin,
  `params` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `fs_extensions2`
--

INSERT INTO `fs_extensions2` (`name`, `page_from`, `page_to`, `type`, `text`, `params`) VALUES
('agrupar_albaranes', 'compras_agrupar_remitos', 'compras_remitos', 'button', '<span class="glyphicon glyphicon-duplicate"></span><span class="hidden-xs">&nbsp; Agrupar</span>', ''),
('agrupar_albaranes', 'ventas_agrupar_remitos', 'ventas_remitos', 'button', '<span class="glyphicon glyphicon-duplicate"></span><span class="hidden-xs">&nbsp; Agrupar</span>', ''),
('albaranes_agente', 'compras_remitos', 'admin_agente', 'button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Remitos de proveedor', ''),
('albaranes_agente', 'ventas_remitos', 'admin_agente', 'button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Remitos de cliente', ''),
('albaranes_articulo', 'compras_remitos', 'ventas_articulo', 'tab_button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Remitos de proveedor', ''),
('albaranes_articulo', 'ventas_remitos', 'ventas_articulo', 'tab_button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Remitos de cliente', ''),
('albaranes_cliente', 'ventas_remitos', 'ventas_cliente', 'button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Remitos', ''),
('albaranes_proveedor', 'compras_remitos', 'compras_proveedor', 'button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Remitos', ''),
('btn_albaran', 'compras_actualiza_arts', 'compras_remito', 'tab', '<span class="glyphicon glyphicon-share" aria-hidden="true"></span><span class="hidden-xs">&nbsp; Actualizar</span>', '&doc=albaran'),
('btn_atributos', 'ventas_atributos', 'ventas_articulos', 'button', '<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span><span class="hidden-xs"> &nbsp; Atributos</span>', NULL),
('btn_fabricantes', 'ventas_fabricantes', 'ventas_articulos', 'button', '<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span><span class="hidden-xs"> &nbsp; Fabricantes</span>', NULL),
('btn_familias', 'ventas_categorias', 'ventas_articulos', 'button', '<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span><span class="hidden-xs"> &nbsp; Categorias</span>', NULL),
('btn_modificar', 'ventas_articulos_masivo', 'ventas_articulos', 'button', '<span class="glyphicon glyphicon-edit" aria-hidden="true"></span><span class="hidden-xs">&nbsp; Modificar en masivo</span>', NULL),
('btn_pedido', 'compras_actualiza_arts', 'compras_pedido', 'tab', '<span class="glyphicon glyphicon-share" aria-hidden="true"></span><span class="hidden-xs">&nbsp; Actualizar</span>', '&doc=pedido'),
('btn_servicios', 'servicios_contratados', 'ventas_servicios', 'button', '<span class="glyphicon glyphicon-file" aria-hidden="true"></span><span class="hidden-xs">&nbsp; Contratos</span>', NULL),
('cosmo', 'admin_user', 'admin_user', 'css', 'view/css/bootstrap-cosmo.min.css', ''),
('darkly', 'admin_user', 'admin_user', 'css', 'view/css/bootstrap-darkly.min.css', ''),
('documentos_albaranescli', 'documentos_facturas', 'ventas_albaran', 'tab', '<span class="glyphicon glyphicon-file" aria-hidden="true" title="Documentos"></span>', '&folder=albaranescli'),
('documentos_albaranesprov', 'documentos_facturas', 'compras_albaran', 'tab', '<span class="glyphicon glyphicon-file" aria-hidden="true" title="Documentos"></span>', '&folder=albaranesprov'),
('documentos_facturascli', 'documentos_facturas', 'ventas_factura', 'tab', '<span class="glyphicon glyphicon-file" aria-hidden="true" title="Documentos"></span>', '&folder=facturascli'),
('documentos_facturasprov', 'documentos_facturas', 'compras_factura', 'tab', '<span class="glyphicon glyphicon-file" aria-hidden="true" title="Documentos"></span>', '&folder=facturasprov'),
('documentos_pedidoscli', 'documentos_facturas', 'ventas_pedido', 'tab', '<span class="glyphicon glyphicon-file" aria-hidden="true" title="Documentos"></span>', '&folder=pedidoscli'),
('documentos_pedidosprov', 'documentos_facturas', 'compras_pedido', 'tab', '<span class="glyphicon glyphicon-file" aria-hidden="true" title="Documentos"></span>', '&folder=pedidosprov'),
('documentos_presupuestoscli', 'documentos_facturas', 'ventas_presupuesto', 'tab', '<span class="glyphicon glyphicon-file" aria-hidden="true" title="Documentos"></span>', '&folder=presupuestoscli'),
('documentos_servicioscli', 'documentos_facturas', 'ventas_servicio', 'tab', '<span class="glyphicon glyphicon-file" aria-hidden="true" title="Documentos"></span>', '&folder=servicioscli'),
('email_albaran', 'ventas_imprimir', 'ventas_remito', 'email', 'Remito simple', '&albaran=TRUE'),
('email_albaran_proveedor', 'compras_imprimir', 'compras_remito', 'email', 'Remito simple', '&albaran=TRUE'),
('email_factura', 'ventas_imprimir', 'ventas_factura', 'email', 'Factura simple', '&factura=TRUE&tipo=simple'),
('email_servicio', 'ayuda_imprimir', 'ayuda_home', 'email', ' simple', ''),
('email_servicio', 'imprimir_servicio', 'ventas_servicio', 'email', 'Servicio simple', ''),
('facturas_agente', 'compras_facturas', 'admin_agente', 'button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Facturas de proveedor', ''),
('facturas_agente', 'ventas_facturas', 'admin_agente', 'button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Facturas de cliente', ''),
('facturas_articulo', 'compras_facturas', 'ventas_articulo', 'tab_button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Facturas de proveedor', ''),
('facturas_articulo', 'ventas_facturas', 'ventas_articulo', 'tab_button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Facturas de cliente', ''),
('facturas_cliente', 'ventas_facturas', 'ventas_cliente', 'button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Facturas', ''),
('facturas_proveedor', 'compras_facturas', 'compras_proveedor', 'button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Facturas', ''),
('flatly', 'admin_user', 'admin_user', 'css', 'view/css/bootstrap-flatly.min.css', ''),
('imprimir_albaran', 'ventas_imprimir', 'ventas_remito', 'pdf', 'Remito simple', '&albaran=TRUE'),
('imprimir_albaran_noval', 'ventas_imprimir', 'ventas_remito', 'pdf', 'Remito sin valorar', '&albaran=TRUE&noval=TRUE'),
('imprimir_albaran_proveedor', 'compras_imprimir', 'compras_remito', 'pdf', 'Remito simple', '&albaran=TRUE'),
('imprimir_factura', 'ventas_imprimir', 'ventas_factura', 'pdf', 'Factura simple', '&factura=TRUE&tipo=simple'),
('imprimir_factura_carta', 'ventas_imprimir', 'ventas_factura', 'pdf', 'Modelo carta', '&factura=TRUE&tipo=carta'),
('imprimir_factura_proveedor', 'compras_imprimir', 'compras_factura', 'pdf', 'Factura simple', '&factura=TRUE'),
('imprimir_servicio', 'ayuda_imprimir', '', 'pdf', ' simple', ''),
('imprimir_servicio', 'imprimir_servicio', 'ventas_servicio', 'pdf', 'Servicio simple', ''),
('imprimir_servicio_sin_detalle', 'imprimir_rapido', 'ventas_servicio', 'pdf', 'Servicio sin líneas', ''),
('imprimir_servicio_sin_detalles_horizontal', 'imprimir_rapido_horizontal', 'ventas_servicio', 'pdf', '2 Servicio sin líneas en 1 página', ''),
('informe_articulo', 'informe_articulos', 'ventas_articulo', 'tab_button', '<span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> &nbsp; Informe', '&tab=varios'),
('lumen', 'admin_user', 'admin_user', 'css', 'view/css/bootstrap-lumen.min.css', ''),
('minicron', 'servicios_contratados', 'ventas_servicios', 'minicron', NULL, '&minicron=TRUE'),
('opciones_clientes', 'ventas_clientes_opciones', 'ventas_clientes', 'button', '<span class="glyphicon glyphicon-cog" aria-hidden="true" title="Opciones para nuevos clientes"></span>', NULL),
('opciones_servicios', 'opciones_servicios', 'ventas_servicios', 'button', '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span><span class="hidden-xs">&nbsp; Opciones</span>', NULL),
('paper', 'admin_user', 'admin_user', 'css', 'view/css/bootstrap-paper.min.css', ''),
('sandstone', 'admin_user', 'admin_user', 'css', 'view/css/bootstrap-sandstone.min.css', ''),
('servicios_agente', 'ventas_servicios', 'admin_agente', 'button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Servicio de cliente', ''),
('servicios_articulo', 'ventas_servicios', 'ventas_articulo', 'tab_button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Servicio de cliente', ''),
('servicios_cliente', 'ventas_servicios', 'ventas_cliente', 'button', '<span class="glyphicon glyphicon-list" aria-hidden="true"></span> &nbsp; Servicio', ''),
('simplex', 'admin_user', 'admin_user', 'css', 'view/css/bootstrap-simplex.min.css', ''),
('spacelab', 'admin_user', 'admin_user', 'css', 'view/css/bootstrap-spacelab.min.css', ''),
('tab_devoluciones', 'ventas_factura_devolucion', 'ventas_factura', 'tab', '<span class="glyphicon glyphicon-share" aria-hidden="true"></span><span class="hidden-xs">&nbsp; Devoluciones</span>', NULL),
('tab_sms', 'clientes_sms', 'ventas_factura', 'tab', '<span class="glyphicon glyphicon-send" aria-hidden="true"></span><span class="hidden-xs">&nbsp; Enviar SMS</span>', NULL),
('tab_sms1', 'clientes_sms', 'ventas_servicio', 'tab', '<span class="glyphicon glyphicon-send" aria-hidden="true"></span><span class="hidden-xs">&nbsp; Enviar SMS</span>', NULL),
('united', 'admin_user', 'admin_user', 'css', 'view/css/bootstrap-united.min.css', ''),
('ventas_servicios_calendario', 'ventas_servicios_calendario', 'ventas_servicios', 'button', '<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span><span class="hidden-xs">&nbsp; Calendario</span>', NULL),
('yeti', 'admin_user', 'admin_user', 'css', 'view/css/bootstrap-yeti.min.css', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fs_logs`
--

CREATE TABLE `fs_logs` (
  `id` int(11) NOT NULL,
  `tipo` varchar(50) COLLATE utf8_bin NOT NULL,
  `detalle` text COLLATE utf8_bin NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuario` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `ip` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `alerta` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `fs_logs`
--

INSERT INTO `fs_logs` (`id`, `tipo`, `detalle`, `fecha`, `usuario`, `ip`, `alerta`) VALUES
(1, 'login', 'Login correcto.', '2016-11-29 05:47:44', 'admin', '::1', 0),
(2, 'error', 'Empleado no encontrado.', '2016-11-29 05:47:53', 'admin', '::1', 0),
(3, 'error', 'Usuario no encontrado.', '2016-11-29 05:47:57', 'admin', '::1', 0),
(4, 'error', 'Artículo no encontrado.', '2016-11-29 05:47:58', 'admin', '::1', 0),
(5, 'error', 'Artículo no encontrado.', '2016-11-29 05:47:58', 'admin', '::1', 0),
(6, 'error', 'Empleado no encontrado.', '2016-11-29 05:48:02', 'admin', '::1', 0),
(7, 'error', 'Artículo no encontrado.', '2016-11-29 05:48:03', 'admin', '::1', 0),
(8, 'error', 'Artículo no encontrado.', '2016-11-29 05:48:03', 'admin', '::1', 0),
(9, 'error', 'Faltan datos.', '2016-11-29 05:48:04', 'admin', '::1', 0),
(10, 'error', '¡Factura de proveedor no encontrada!', '2016-11-29 05:48:05', 'admin', '::1', 0),
(11, 'error', '¡Proveedor no encontrado!', '2016-11-29 05:48:07', 'admin', '::1', 0),
(12, 'error', '¡Remito de compra no encontrado!', '2016-11-29 05:48:07', 'admin', '::1', 0),
(13, 'error', 'Asiento no encontrado.', '2016-11-29 05:48:09', 'admin', '::1', 0),
(14, 'error', 'Cuenta no encontrada.', '2016-11-29 05:48:09', 'admin', '::1', 0),
(15, 'error', 'Ejercicio no encontrado.', '2016-11-29 05:48:11', 'admin', '::1', 0),
(16, 'error', 'Subcuenta no encontrada.', '2016-11-29 05:48:12', 'admin', '::1', 0),
(17, 'error', 'Artículo no encontrado.', '2016-11-29 05:48:23', 'admin', '::1', 0),
(18, 'error', 'Contrato no encontrado.', '2016-11-29 05:48:56', 'admin', '::1', 0),
(19, 'error', 'Tabla desconocida.', '2016-11-29 05:48:56', 'admin', '::1', 0),
(20, 'error', 'Contrato no encontrado.', '2016-11-29 05:48:59', 'admin', '::1', 0),
(21, 'error', '¡Servicio de cliente no encontrado!', '2016-11-29 05:49:01', 'admin', '::1', 0),
(22, 'error', 'Categoria no encontrada.', '2016-11-29 05:49:05', 'admin', '::1', 0),
(23, 'error', '¡Cliente no encontrado!', '2016-11-29 05:49:06', 'admin', '::1', 0),
(24, 'error', 'Fabricante no encontrado.', '2016-11-29 05:49:07', 'admin', '::1', 0),
(25, 'error', '¡Factura de cliente no encontrada!', '2016-11-29 05:49:07', 'admin', '::1', 0),
(26, 'error', 'Factura no encontrada.', '2016-11-29 05:49:08', 'admin', '::1', 0),
(27, 'error', 'Grupo no encontrado.', '2016-11-29 05:49:08', 'admin', '::1', 0),
(28, 'error', '¡Remito de venta no encontrado!', '2016-11-29 05:49:09', 'admin', '::1', 0),
(29, 'error', '¡Servicio de cliente no encontrado!', '2016-11-29 05:49:10', 'admin', '::1', 0),
(30, 'error', '¡Cliente no encontrado!', '2016-11-29 07:22:03', 'admin', '::1', 0),
(31, 'error', '¡Cliente no encontrado!', '2016-11-29 07:23:29', 'admin', '::1', 0),
(32, 'error', '¡Cliente no encontrado!', '2016-11-29 07:24:38', 'admin', '::1', 0),
(33, 'login', 'El usuario ha cerrado la sesión.', '2016-11-29 12:00:46', 'admin', '::1', 0),
(34, 'login', 'Login correcto.', '2016-11-29 12:00:58', 'admin', '::1', 0),
(35, 'error', '¡Imposible guardar el Servicio!', '2016-11-29 13:23:04', 'admin', '::1', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fs_pages`
--

CREATE TABLE `fs_pages` (
  `name` varchar(30) COLLATE utf8_bin NOT NULL,
  `title` varchar(40) COLLATE utf8_bin NOT NULL,
  `folder` varchar(15) COLLATE utf8_bin NOT NULL,
  `version` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `show_on_menu` tinyint(1) NOT NULL DEFAULT '1',
  `important` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `fs_pages`
--

INSERT INTO `fs_pages` (`name`, `title`, `folder`, `version`, `show_on_menu`, `important`) VALUES
('admin_agente', 'Empleado', 'admin', '2016.003', 0, 0),
('admin_agentes', 'Empleados', 'admin', '2016.003', 1, 0),
('admin_almacenes', 'Almacenes', 'admin', '2016.003', 1, 0),
('admin_divisas', 'Divisas', 'admin', '2016.003', 1, 0),
('admin_empresa', 'Empresa', 'admin', '2016.003', 1, 0),
('admin_home', 'Panel de control', 'admin', '2016.003', 1, 0),
('admin_info', 'Información del sistema', 'admin', '2016.003', 1, 0),
('admin_paises', 'Paises', 'admin', '2016.003', 1, 0),
('admin_transportes', 'Agencias de transporte', 'admin', '2016.003', 1, 0),
('admin_user', 'Usuario', 'admin', '2016.003', 0, 0),
('admin_users', 'Usuarios', 'admin', '2016.003', 1, 0),
('articulo_subcuentas', 'Subcuentas', 'ventas', '2016.003', 0, 0),
('articulo_trazabilidad', '', 'ventas', '2016.003', 0, 0),
('ayuda_home', 'Home', '', '2016.003', 1, 0),
('ayuda_imprimir', 'imprimir', 'ventas', '2016.003', 0, 0),
('backup', 'Backups', 'admin', '2016.003', 1, 0),
('base_wizard', 'Asistente de instalación', 'admin', '2016.003', 0, 0),
('clientes_sms', 'Enviar SMS', '', '2016.003', 1, 0),
('compras_actualiza_arts', 'Artículos documento', 'compras', '2016.003', 0, 0),
('compras_agrupar_remitos', 'Agrupar remitos', 'compras', '2016.003', 0, 0),
('compras_factura', 'Factura de proveedor', 'compras', '2016.003', 0, 0),
('compras_facturas', 'Facturas', 'compras', '2016.003', 1, 0),
('compras_imprimir', 'imprimir', 'compras', '2016.003', 0, 0),
('compras_proveedor', 'Proveedor', 'compras', '2016.003', 0, 0),
('compras_proveedores', 'Proveedores / Acreedores', 'compras', '2016.003', 1, 0),
('compras_remito', 'remito de proveedor', 'compras', '2016.003', 0, 0),
('compras_remitos', 'Remitos', 'compras', '2016.003', 1, 0),
('contabilidad_asiento', 'Asiento', 'contabilidad', '2016.003', 0, 0),
('contabilidad_cuenta', 'Cuenta', 'contabilidad', '2016.003', 0, 0),
('contabilidad_ejercicio', 'Ejercicio', 'contabilidad', '2016.003', 0, 0),
('contabilidad_ejercicios', 'Ejercicios', '', '2016.003', 1, 0),
('contabilidad_formas_pago', 'Formas de Pago', 'configuracion', '2016.003', 1, 0),
('contabilidad_impuestos', 'Impuestos', 'configuracion', '2016.003', 1, 0),
('contabilidad_nuevo_asiento', 'Nuevo asiento', '', '2016.003', 1, 0),
('contabilidad_series', 'Tipos de documentos', 'configuracion', '2016.003', 1, 0),
('contabilidad_subcuenta', 'Subcuenta', 'contabilidad', '2016.003', 0, 0),
('cuentas_especiales', 'Cuentas Especiales', 'contabilidad', '2016.003', 0, 0),
('dashboard', 'Escritorio', 'ventas', '2016.003', 1, 0),
('documentos_facturas', 'Documentos', 'ventas', '2016.003', 0, 0),
('editar_contrato_servicio', 'Editar contrato', 'ventas', '2016.003', 0, 0),
('fsdk_home', 'FSDK', 'admin', '2016.003', 1, 0),
('fsdk_tabla', 'Tabla', 'admin', '2016.003', 0, 0),
('imprimir_rapido', 'Imprimir Rápido', 'Servicio', '2016.003', 0, 0),
('imprimir_rapido_horizontal', 'Imprimir Rápido Horizontal', 'Servicio', '2016.003', 0, 0),
('imprimir_servicio', 'imprimir', 'ventas', '2016.003', 0, 0),
('informe_articulos', 'Artículos', 'informes', '2016.003', 1, 0),
('informe_errores', 'Errores', 'informes', '2016.003', 1, 0),
('informe_facturas', 'Facturas', 'informes', '2016.003', 1, 0),
('informe_remitos', 'Remitos', 'informes', '2016.003', 1, 0),
('megafacturador', 'Facturador', 'ventas', '2016.003', 1, 0),
('nueva_compra', 'Nueva compra...', 'compras', '2016.003', 0, 1),
('nueva_venta', 'Nueva venta...', 'ventas', '2016.003', 0, 1),
('nuevo_servicio', 'Nuevo turno...', 'ventas', '2016.003', 0, 1),
('opciones_servicios', 'Opciones', 'Servicios', '2016.003', 0, 0),
('recetas', 'Producción', 'Articulos', '2016.003', 1, 0),
('servicios_contratados', 'Servicios contratados', 'Ventas', '2016.003', 0, 0),
('subcuenta_asociada', 'Asignar subcuenta...', 'contabilidad', '2016.003', 0, 0),
('ventas_agrupar_remitos', 'Agrupar remitos', 'ventas', '2016.003', 0, 0),
('ventas_articulo', 'Articulo', 'ventas', '2016.003', 0, 0),
('ventas_articulos', 'Artículos', 'Articulos', '2016.003', 1, 0),
('ventas_articulos_masivo', 'Articulos Megamod', '', '2016.003', 1, 0),
('ventas_atributos', 'Atributod de artículos', 'ventas', '2016.003', 0, 0),
('ventas_categoria', 'Categoria', 'ventas', '2016.003', 0, 0),
('ventas_categorias', 'Categorias', 'ventas', '2016.003', 0, 0),
('ventas_cliente', 'Cliente', 'ventas', '2016.003', 0, 0),
('ventas_clientes', 'Clientes', 'ventas', '2016.003', 1, 0),
('ventas_clientes_opciones', 'Opciones', 'clientes', '2016.003', 0, 0),
('ventas_fabricante', 'Categoria', 'ventas', '2016.003', 0, 0),
('ventas_fabricantes', 'Fabricantes', 'ventas', '2016.003', 0, 0),
('ventas_factura', 'Factura de cliente', 'ventas', '2016.003', 0, 0),
('ventas_factura_devolucion', 'Devoluciones de factura de venta', 'ventas', '2016.003', 0, 0),
('ventas_facturas', 'Facturas', 'ventas', '2016.003', 1, 0),
('ventas_grupo', 'Grupo', 'ventas', '2016.003', 0, 0),
('ventas_imprimir', 'imprimir', 'ventas', '2016.003', 0, 0),
('ventas_remito', 'remito de cliente', 'ventas', '2016.003', 0, 0),
('ventas_remitos', 'Remitos', 'ventas', '2016.003', 1, 0),
('ventas_servicio', 'Servicio', 'ventas', '2016.003', 0, 0),
('ventas_servicios', 'Servicios a clientes', 'Servicios', '2016.003', 1, 0),
('ventas_servicios_calendario', 'Calendario', 'Servicios', '2016.003', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fs_users`
--

CREATE TABLE `fs_users` (
  `nick` varchar(12) COLLATE utf8_bin NOT NULL,
  `password` varchar(100) COLLATE utf8_bin NOT NULL,
  `log_key` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `codagente` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `last_login` date DEFAULT NULL,
  `last_login_time` time DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `last_browser` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `fs_page` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `css` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `fs_users`
--

INSERT INTO `fs_users` (`nick`, `password`, `log_key`, `admin`, `codagente`, `last_login`, `last_login_time`, `last_ip`, `last_browser`, `fs_page`, `css`, `email`) VALUES
('admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'cff41ea0828c4e1daad0ea97bf628490af0cf60d', 1, '1', '2016-11-29', '10:20:12', '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36', NULL, 'view/css/bootstrap-yeti.min.css', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fs_vars`
--

CREATE TABLE `fs_vars` (
  `name` varchar(35) COLLATE utf8_bin NOT NULL,
  `varchar` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `fs_vars`
--

INSERT INTO `fs_vars` (`name`, `varchar`) VALUES
('cal_fin', '20:00'),
('cal_inicio', '09:00'),
('cal_intervalo', '30'),
('install_step', '2'),
('last_download_check', '2016-11-22'),
('mail_bcc', ''),
('mail_enc', 'ssl'),
('mail_firma', '\n---\nEnviado con Sartin'),
('mail_host', 'smtp.gmail.com'),
('mail_password', ''),
('mail_port', '465'),
('mail_user', ''),
('servicios_accesorios', '0'),
('servicios_condiciones', 'Condiciones de servicio.'),
('servicios_descripcion', '0'),
('servicios_diasfin', '10'),
('servicios_fechafin', '0'),
('servicios_fechainicio', '0'),
('servicios_garantia', '0'),
('servicios_material', '0'),
('servicios_material_estado', '0'),
('servicios_mostrar_accesorios', '1'),
('servicios_mostrar_descripcion', '1'),
('servicios_mostrar_fechafin', '1'),
('servicios_mostrar_fechainicio', '1'),
('servicios_mostrar_garantia', '0'),
('servicios_mostrar_material', '1'),
('servicios_mostrar_material_estado', '1'),
('servicios_mostrar_solucion', '1'),
('servicios_solucion', '0'),
('st_accesorios', 'Area'),
('st_descripcion', 'Accion'),
('st_fechafin', 'Fecha de finalización'),
('st_fechainicio', 'Fecha de inicio'),
('st_garantia', 'Garantía'),
('st_material', 'Marca-Modelo'),
('st_material_estado', 'Descripcion del trabajo'),
('st_servicio', 'Servicio'),
('st_servicios', 'Servicios'),
('st_solucion', 'N<'),
('usar_direccion', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gruposclientes`
--

CREATE TABLE `gruposclientes` (
  `codgrupo` varchar(6) COLLATE utf8_bin NOT NULL,
  `nombre` varchar(100) COLLATE utf8_bin NOT NULL,
  `codtarifa` varchar(6) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `impuestos`
--

CREATE TABLE `impuestos` (
  `codimpuesto` varchar(10) COLLATE utf8_bin NOT NULL,
  `codsubcuentaacr` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuentadeu` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuentaivadedadue` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuentaivadevadue` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuentaivadeventue` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuentaivarepexe` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuentaivarepexp` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuentaivarepre` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuentaivasopagra` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuentaivasopexe` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuentaivasopimp` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuentarep` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuentasop` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `descripcion` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `idsubcuentaacr` int(11) DEFAULT NULL,
  `idsubcuentadeu` int(11) DEFAULT NULL,
  `idsubcuentaivadedadue` int(11) DEFAULT NULL,
  `idsubcuentaivadevadue` int(11) DEFAULT NULL,
  `idsubcuentaivadeventue` int(11) DEFAULT NULL,
  `idsubcuentaivarepexe` int(11) DEFAULT NULL,
  `idsubcuentaivarepexp` int(11) DEFAULT NULL,
  `idsubcuentaivarepre` int(11) DEFAULT NULL,
  `idsubcuentaivasopagra` int(11) DEFAULT NULL,
  `idsubcuentaivasopexe` int(11) DEFAULT NULL,
  `idsubcuentaivasopimp` int(11) DEFAULT NULL,
  `idsubcuentarep` int(11) DEFAULT NULL,
  `idsubcuentasop` int(11) DEFAULT NULL,
  `iva` double NOT NULL,
  `recargo` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `impuestos`
--

INSERT INTO `impuestos` (`codimpuesto`, `codsubcuentaacr`, `codsubcuentadeu`, `codsubcuentaivadedadue`, `codsubcuentaivadevadue`, `codsubcuentaivadeventue`, `codsubcuentaivarepexe`, `codsubcuentaivarepexp`, `codsubcuentaivarepre`, `codsubcuentaivasopagra`, `codsubcuentaivasopexe`, `codsubcuentaivasopimp`, `codsubcuentarep`, `codsubcuentasop`, `descripcion`, `idsubcuentaacr`, `idsubcuentadeu`, `idsubcuentaivadedadue`, `idsubcuentaivadevadue`, `idsubcuentaivadeventue`, `idsubcuentaivarepexe`, `idsubcuentaivarepexp`, `idsubcuentaivarepre`, `idsubcuentaivasopagra`, `idsubcuentaivasopexe`, `idsubcuentaivasopimp`, `idsubcuentarep`, `idsubcuentasop`, `iva`, `recargo`) VALUES
('IVA0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IVA 0%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0),
('IVA105', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IVA 10.5%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10.5, 1.4),
('IVA21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IVA 21%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 21, 5.2),
('IVA5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'IVA 5%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 0.5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineasfacturascli`
--

CREATE TABLE `lineasfacturascli` (
  `cantidad` double NOT NULL,
  `codimpuesto` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `descripcion` text COLLATE utf8_bin,
  `dtolineal` double DEFAULT '0',
  `dtopor` double NOT NULL,
  `idalbaran` int(11) DEFAULT NULL,
  `idfactura` int(11) NOT NULL,
  `idlinea` int(11) NOT NULL,
  `irpf` double DEFAULT NULL,
  `iva` double NOT NULL,
  `porcomision` double DEFAULT NULL,
  `pvpsindto` double NOT NULL,
  `pvptotal` double NOT NULL,
  `pvpunitario` double NOT NULL,
  `recargo` double DEFAULT NULL,
  `referencia` varchar(18) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineasfacturasprov`
--

CREATE TABLE `lineasfacturasprov` (
  `cantidad` double NOT NULL,
  `codimpuesto` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuenta` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `descripcion` text COLLATE utf8_bin,
  `dtolineal` double DEFAULT '0',
  `dtopor` double NOT NULL,
  `idalbaran` int(11) DEFAULT NULL,
  `idfactura` int(11) NOT NULL,
  `idlinea` int(11) NOT NULL,
  `idsubcuenta` int(11) DEFAULT NULL,
  `irpf` double DEFAULT NULL,
  `iva` double NOT NULL,
  `pvpsindto` double NOT NULL,
  `pvptotal` double DEFAULT NULL,
  `pvpunitario` double NOT NULL,
  `recargo` double DEFAULT NULL,
  `referencia` varchar(18) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineasservicioscli`
--

CREATE TABLE `lineasservicioscli` (
  `cantidad` double NOT NULL,
  `codimpuesto` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `descripcion` text COLLATE utf8_bin,
  `dtolineal` double DEFAULT '0',
  `dtopor` double NOT NULL,
  `idlinea` int(11) NOT NULL,
  `idlineapresupuesto` int(11) DEFAULT NULL,
  `idservicio` int(11) NOT NULL,
  `idpresupuesto` int(11) DEFAULT NULL,
  `irpf` double DEFAULT NULL,
  `iva` double NOT NULL,
  `pvpsindto` double NOT NULL,
  `pvptotal` double NOT NULL,
  `pvpunitario` double NOT NULL,
  `recargo` double DEFAULT NULL,
  `referencia` varchar(18) COLLATE utf8_bin DEFAULT NULL,
  `totalenalbaran` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `validarprov` tinyint(1) DEFAULT NULL,
  `codiso` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `bandera` text COLLATE utf8_bin,
  `nombre` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `codpais` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`validarprov`, `codiso`, `bandera`, `nombre`, `codpais`) VALUES
(NULL, 'AD', NULL, 'Andorra', 'AND'),
(NULL, 'AR', NULL, 'Argentina', 'ARG'),
(NULL, 'BO', NULL, 'Bolivia', 'BOL'),
(NULL, 'CL', NULL, 'Chile', 'CHL'),
(NULL, 'CO', NULL, 'Colombia', 'COL'),
(NULL, 'CR', NULL, 'Costa Rica', 'CRI'),
(NULL, 'CU', NULL, 'Cuba', 'CUB'),
(NULL, 'DO', NULL, 'República Dominicana', 'DOM'),
(NULL, 'EC', NULL, 'Ecuador', 'ECU'),
(NULL, 'ES', NULL, 'España', 'ESP'),
(NULL, 'GQ', NULL, 'Guinea Ecuatorial', 'GNQ'),
(NULL, 'GT', NULL, 'Guatemala', 'GTM'),
(NULL, 'HN', NULL, 'Honduras', 'HND'),
(NULL, 'MX', NULL, 'México', 'MEX'),
(NULL, 'NI', NULL, 'Nicaragua', 'NIC'),
(NULL, 'PA', NULL, 'Panamá', 'PAN'),
(NULL, 'PE', NULL, 'Perú', 'PER'),
(NULL, 'PR', NULL, 'Puerto Rico', 'PRI'),
(NULL, 'PY', NULL, 'Paraguay', 'PRY'),
(NULL, 'SV', NULL, 'El Salvador', 'SLV'),
(NULL, 'UY', NULL, 'Uruguay', 'URY'),
(NULL, 'VE', NULL, 'Venezuela', 'VEN');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `cifnif` varchar(20) COLLATE utf8_bin NOT NULL,
  `codcontacto` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `codcuentadom` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `codcuentapago` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `coddivisa` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `codpago` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codproveedor` varchar(6) COLLATE utf8_bin NOT NULL,
  `codserie` varchar(2) COLLATE utf8_bin DEFAULT NULL,
  `codsubcuenta` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `contacto` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `fax` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `idsubcuenta` int(11) DEFAULT NULL,
  `ivaportes` double DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_bin NOT NULL,
  `razonsocial` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `observaciones` text COLLATE utf8_bin,
  `recfinanciero` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `regimeniva` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `telefono1` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `telefono2` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `tipoidfiscal` varchar(25) COLLATE utf8_bin NOT NULL DEFAULT 'NIF',
  `web` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `acreedor` tinyint(1) DEFAULT '0',
  `personafisica` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recetas`
--

CREATE TABLE `recetas` (
  `codreceta` varchar(25) COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_bin NOT NULL,
  `referencia` varchar(18) COLLATE utf8_bin NOT NULL,
  `cantidad` double DEFAULT NULL,
  `codalmacen` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `codalmacen2` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `ultima_produccion` timestamp NULL DEFAULT NULL,
  `observaciones` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secuencias`
--

CREATE TABLE `secuencias` (
  `descripcion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `id` int(11) NOT NULL,
  `idsec` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_bin NOT NULL,
  `valor` int(11) DEFAULT NULL,
  `valorout` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `secuencias`
--

INSERT INTO `secuencias` (`descripcion`, `id`, `idsec`, `nombre`, `valor`, `valorout`) VALUES
('Secuencia del ejercicio 2016 y la serie C', 1, 1, 'nserviciocli', 1, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secuenciasejercicios`
--

CREATE TABLE `secuenciasejercicios` (
  `codejercicio` varchar(4) COLLATE utf8_bin NOT NULL,
  `codserie` varchar(2) COLLATE utf8_bin NOT NULL,
  `id` int(11) NOT NULL,
  `nalbarancli` int(11) NOT NULL,
  `nalbaranprov` int(11) NOT NULL,
  `nfacturacli` int(11) NOT NULL,
  `nfacturaprov` int(11) NOT NULL,
  `npedidocli` int(11) NOT NULL,
  `npedidoprov` int(11) NOT NULL,
  `npresupuestocli` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `secuenciasejercicios`
--

INSERT INTO `secuenciasejercicios` (`codejercicio`, `codserie`, `id`, `nalbarancli`, `nalbaranprov`, `nfacturacli`, `nfacturaprov`, `npedidocli`, `npedidoprov`, `npresupuestocli`) VALUES
('2016', 'C', 1, 1, 1, 1, 1, 1, 1, 1),
('2016', 'D', 2, 1, 1, 1, 1, 1, 1, 1),
('2016', 'DC', 3, 1, 1, 1, 1, 1, 1, 1),
('2016', 'V', 4, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `series`
--

CREATE TABLE `series` (
  `irpf` double DEFAULT NULL,
  `idcuenta` int(11) DEFAULT NULL,
  `codserie` varchar(2) COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `siniva` tinyint(1) DEFAULT NULL,
  `codcuenta` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `codejercicio` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `numfactura` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `series`
--

INSERT INTO `series` (`irpf`, `idcuenta`, `codserie`, `descripcion`, `siniva`, `codcuenta`, `codejercicio`, `numfactura`) VALUES
(0, NULL, 'C', 'Compras', 0, NULL, NULL, 1),
(0, NULL, 'D', 'Devoluciones', 0, NULL, NULL, 1),
(0, NULL, 'DC', 'Devoluciones Compras', 0, NULL, NULL, 1),
(0, NULL, 'V', 'Ventas', 0, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicioscli`
--

CREATE TABLE `servicioscli` (
  `apartado` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `prioridad` int(11) DEFAULT NULL,
  `cifnif` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `ciudad` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `codagente` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codalmacen` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `codcliente` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  `coddir` int(11) DEFAULT NULL,
  `coddivisa` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `codejercicio` varchar(4) COLLATE utf8_bin DEFAULT NULL,
  `codigo` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `codpago` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codpais` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `codpostal` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `codserie` varchar(2) COLLATE utf8_bin NOT NULL,
  `direccion` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `editable` tinyint(1) NOT NULL,
  `garantia` tinyint(1) DEFAULT NULL,
  `fecha` date NOT NULL,
  `hora` time DEFAULT '00:00:00',
  `femail` date DEFAULT NULL,
  `fechafin` date DEFAULT NULL,
  `horafin` time DEFAULT NULL,
  `fechainicio` date DEFAULT NULL,
  `horainicio` time DEFAULT NULL,
  `idservicio` int(11) NOT NULL,
  `idalbaran` int(11) DEFAULT NULL,
  `idprovincia` int(11) DEFAULT NULL,
  `irpf` double DEFAULT NULL,
  `neto` double NOT NULL,
  `nombrecliente` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `numero` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `numero2` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `observaciones` text COLLATE utf8_bin,
  `descripcion` text COLLATE utf8_bin,
  `solucion` text COLLATE utf8_bin,
  `usuarioep` text COLLATE utf8_bin,
  `idmep` text COLLATE utf8_bin,
  `respep` text COLLATE utf8_bin,
  `yacep` text COLLATE utf8_bin,
  `material` text COLLATE utf8_bin,
  `material_estado` text COLLATE utf8_bin,
  `accesorios` text COLLATE utf8_bin,
  `porcomision` double DEFAULT NULL,
  `provincia` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `recfinanciero` double DEFAULT NULL,
  `tasaconv` double NOT NULL,
  `total` double NOT NULL,
  `totaleuros` double DEFAULT NULL,
  `totalirpf` double NOT NULL,
  `totaliva` double NOT NULL,
  `totalrecargo` double DEFAULT NULL,
  `idestado` int(11) DEFAULT NULL,
  `numsalida` text COLLATE utf8_bin,
  `numcertificado` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `servicioscli`
--

INSERT INTO `servicioscli` (`apartado`, `prioridad`, `cifnif`, `ciudad`, `codagente`, `codalmacen`, `codcliente`, `coddir`, `coddivisa`, `codejercicio`, `codigo`, `codpago`, `codpais`, `codpostal`, `codserie`, `direccion`, `editable`, `garantia`, `fecha`, `hora`, `femail`, `fechafin`, `horafin`, `fechainicio`, `horainicio`, `idservicio`, `idalbaran`, `idprovincia`, `irpf`, `neto`, `nombrecliente`, `numero`, `numero2`, `observaciones`, `descripcion`, `solucion`, `usuarioep`, `idmep`, `respep`, `yacep`, `material`, `material_estado`, `accesorios`, `porcomision`, `provincia`, `recfinanciero`, `tasaconv`, `total`, `totaleuros`, `totalirpf`, `totaliva`, `totalrecargo`, `idestado`, `numsalida`, `numcertificado`) VALUES
('', 3, '', '', '1', 'ALG', '1', 1, 'ARS', '2016', 'SER2016C2', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '02:56:02', NULL, '0000-00-00', '00:00:00', '0000-00-00', '02:56:00', 5812, NULL, NULL, 0, 0, 'YPF', '2', '5812', '10', 'FABRICAR', '', '', '1', 'GIMENEZ O.', 'O', 'UNIVERSAL', 'ARANDELA   SEG?N  MUESTRA', 'CS', 0, '', 0, 10.83, 0, 0, 0, 0, 0, 1, '809', ''),
(NULL, 3, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C3', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '02:56:02', NULL, '0000-00-00', '00:00:00', '0000-00-00', '02:56:00', 5813, NULL, NULL, 0, 0, 'SNP', '2', '5813', '', 'MECANIZAR  Y AJUSTAR', '', '', '2', 'GIMENEZ O.', 'O', 'STORK  45-60', 'CUBOS   DE  BOMBA ', 'CS', 0, '', 0, 10.83, 0, 0, 0, 0, 0, 1, '', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C384', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5814, 0, 0, 0, 0, 'YPF', '', '5814', '', 'REPARAR', '', '', '1', 'REYES  P.', 'O', 'WUELFELL TAMET 456', 'ARTICULACI?N  DE  COLA', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '802', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C5', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5815, 0, 0, 0, 0, 'YPF', '', '5815', '', 'FABRICAR', '', '', '2', 'FLORES O.', 'O', 'UNIVERSAL', 'JUEGOS  DE  ESQUI', 'PL LH3', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10836', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C6', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5816, 0, 0, 0, 0, 'YPF', '', '5816', '', 'FABRICAR', '', '', '1', 'AYBAR  M.', 'O', 'UNIVERSAL', 'PROLONGAR  50 MM  ', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '803', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C4', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5817, 0, 0, 0, 0, 'YPF', '', '5817', '', 'AGUJEREAR  Y ROSCAR', '', 'Reyes', '1', 'REYES  P.', 'O', 'UNIVERSAL', 'BRIDA ', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C8', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5818, 0, 0, 0, 0, 'PAE', '', '5818', '', 'REPARAR', '', '', '5', 'DIAZ   V.', 'O', 'LUFKIN  456-640', 'CINTA  DE FRENO ', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '808', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C9', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5819, 0, 0, 0, 0, 'PAE', '', '5819', '', 'FABRICAR', '', '', '2', 'VEGA J.', 'O', 'UNIVERSAL', 'CANASTOS  REDONDO ', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '820', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C10', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5820, 0, 0, 0, 0, 'YPF', '', '5820', '', 'FABRICAR', '', '', '4', 'URZAGASTI  J.', 'O', 'UNIVERSAL', 'GRAMPAS  C-TRAVESA?O  Y TUERCAS 0.80 M', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10839', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C11', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5821, 0, 0, 0, 0, 'YPF', '', '5821', '', 'FABRICAR', '', '', '5', 'URZAGASTI  J.', 'O', 'UNIVERSAL', 'GRAMPAS  C-TRAVESA?O  Y TUERCAS 0.60 M', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10839', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C12', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5822, 0, 0, 0, 0, 'YPF', '', '5822', '', 'REPARAR', '', '', '1', 'AYBAR  M.', 'O', 'BBA  R8', 'BBA  LUBRICADORA', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '805', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C13', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5823, 0, 0, 0, 0, 'YPF', '', '5823', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LM.599', 'MTD  9 TN', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10894', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C14', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5824, 0, 0, 0, 0, 'YPF', '', '5824', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.890', 'LUFKIN  456-640', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10895', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C15', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5825, 0, 0, 0, 0, 'YPF', '', '5825', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.2413', 'SIAM C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10896', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C16', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5826, 0, 0, 0, 0, 'YPF', '', '5826', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.1773', 'SIAM C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10897', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C17', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5827, 0, 0, 0, 0, 'YPF', '', '5827', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'O', 'MTD  9 TN', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10898', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C18', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5828, 0, 0, 0, 0, 'YPF', '', '5828', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.1158', 'SIAM C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10899', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C19', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5829, 0, 0, 0, 0, 'YPF', '', '5829', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.1236', '7CK8', 'PATINES  DE  FRENO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10900', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C20', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5830, 0, 0, 0, 0, 'YPF', '', '5830', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.770', 'DARCO 228Y', 'PATINES  DE  FRENO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10840', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C21', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5831, 0, 0, 0, 0, 'YPF', '', '5831', '', 'FABRICAR', '', '', '1', 'URZAGASTI S.', 'LP.1963', 'SIAM C-228', 'PERNO PIE  DE  BIELA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10841', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C22', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5832, 0, 0, 0, 0, 'YPF', '', '5832', '', 'FABRICAR', '', '', '2', 'URZAGASTI S.', 'LM.X1', 'SIAM C-228', 'PERNO PIE  DE  BIELA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10842', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C23', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5833, 0, 0, 0, 0, 'YPF', '', '5833', '', 'FABRICAR', '', '', '2', 'URZAGASTI S.', 'LP.2255', 'SIAM C-228', 'PERNO PIE  DE  BIELA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10843', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C24', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5834, 0, 0, 0, 0, 'YPF', '', '5834', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.1132', 'VULCAN  M 456', 'COJINETE DE CENTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10844', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C25', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5835, 0, 0, 0, 0, 'YPF ', '', '5835', '', 'CAMBIAR  RODAM.', '', '', '1', 'URZAGASTI S.', 'LP.1158', 'SIAM C-228', 'EJE  VELOZ ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10845', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C26', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5836, 0, 0, 0, 0, 'YPF', '', '5836', '', 'CAMBIAR  RODAM.', '', '', '1', 'URZAGASTI S.', 'LP.1158', 'SIAM C-228', 'EJE INTERMEDIO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10845', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C27', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5837, 0, 0, 0, 0, 'YPF', '', '5837', '', 'MECANIZAR  Y AJUSTAR', '', '', '1', 'URZAGASTI S.', 'O', 'MTD  9 TN', 'CONO MECAN. DI?M. INT', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10893', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C28', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5838, 0, 0, 0, 0, 'YPF ', '', '5838', '', 'FABRICAR', '', '', '10', 'URZAGASTI S.', 'O', 'UNIVERSAL', 'GRAMPAS P-ANCLAJE   0.80  M', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11084', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C29', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5839, 0, 0, 0, 0, 'YPF', '', '5839', '', 'FABRICAR', '', '', '10', 'URZAGASTI S.', 'O', 'UNIVERSAL', 'GRAMPAS P-ANCLAJE   0.60  M', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11084', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C30', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5840, 0, 0, 0, 0, 'YPF', '', '5840', '', 'REPARAR', '', '', '1', 'AYBAR  M.', 'O', 'BBA  R8', 'BOMBA  LUBRICADORA', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '814', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C31', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5841, 0, 0, 0, 0, 'YPF ', '', '5841', '', 'FABRICAR', '', '', '2', 'QUINTEROS C.', 'O', 'UNIVERSAL', 'CHASIS  PCP', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10847', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C32', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5842, 0, 0, 0, 0, 'YPF', '', '5842', '', 'FABRICAR', '', '', '1', 'BORIS', 'O', 'UNIVERSAL', 'CHASIS  PCP', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10848', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C33', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5843, 0, 0, 0, 0, 'PAE', '', '5843', '', 'MECANIZAR  Y AJUSTAR', '', '', '1', 'VEGA J.', 'O', 'UNIVERSAL', 'CONO   ', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '812', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C34', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5844, 0, 0, 0, 0, 'PAE', '', '5844', '', 'SOLDAR', '', '', '1', 'VEGA J.', 'O', 'UNIVERSAL', 'CA?O  DE  COBRE', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '813', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C35', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5845, 0, 0, 0, 0, 'SNP', '', '5845', '', 'MECANIZAR DIAM. INT.', '', '', '1', 'GIMENEZ O.', 'O', 'KSB', 'ACOPLE PARA  BBA', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10912', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C36', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5846, 0, 0, 0, 0, 'PAE', '', '5846', '', 'FABRICAR', '', '', '5', 'DIAZ   V.', 'O', 'UNIVERSAL', 'CONOS  ', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '825', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C37', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5847, 0, 0, 0, 0, 'YPF', '', '5847', '', 'REPARAR', '', '', '1', 'AYBAR  M.', 'O', 'BBA  R8', 'BBA  LUBRICADORA', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '817', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C38', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5848, 0, 0, 0, 0, 'YPF', '', '5848', '', 'REPARAR', '', '', '1', 'AYBAR  M.', 'O', 'BBA  R8', 'BBA  LUBRICADORA', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 'GG', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C39', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5849, 0, 0, 0, 0, 'YPF ', '', '5849', '', 'FABRICAR', '', '', '2', 'MOSIMANN  G.', 'LC.912', 'DARCO  456', 'PERNO PIE  DE  BIELA', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10911', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C40', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5850, 0, 0, 0, 0, 'YPF', '', '5850', '', 'REPARAR', '', '', '1', 'MOSIMANN  G.', 'LC.521', 'MTD  9 TN', 'COJINETE DE CENTRO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10970', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C41', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5851, 0, 0, 0, 0, 'YPF', '', '5851', '', 'REPARAR', '', '', '1', 'MOSIMANN  G.', 'LC.635', 'LUFKIN  456-640', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10901', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C42', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5852, 0, 0, 0, 0, 'YPF ', '', '5852', '', 'REPARAR', '', '', '1', 'MOSIMANN  G.', 'LC.604', 'VULCAN 303', 'PALANCA DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C43', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5853, 0, 0, 0, 0, 'SNP', '', '5853', '', 'MECANIZAR DIAM. INT.', '', '', '1', 'GIMENEZ O.', 'O', 'WORTHIGTON', 'ACOPLE  PARA  EJE  PPAL  BBA', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C44', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5854, 0, 0, 0, 0, 'YPF', '', '5854', '', 'FABRICAR', '', '', '5', 'BORIS', 'O', 'UNIVERSAL', 'JUEGOS  DE  ESQUI', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10903', 'Jan-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C45', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5855, 0, 0, 0, 0, 'YPF', '', '5855', '', 'REPARAR', '', '', '1', 'MOSIMANN  G.', 'O', 'LUFKIN  456-640', 'COJINETE DE CENTRO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10904', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C46', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5856, 0, 0, 0, 0, 'YPF ', '', '5856', '', 'REPARAR', '', '', '1', 'CINCOTTA  A.', 'O', 'SIAM C-228', 'AIB  COMPLETO', 'LH ', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11002', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C47', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5857, 0, 0, 0, 0, 'YPF ', '', '5857', '', 'REPARAR', '', '', '1', 'COLCHI  P.', 'O', 'STORK 50-100', 'BOMBA  ALTERNATIVA', 'CE', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11003', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C48', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5858, 0, 0, 0, 0, 'YPF', '', '5858', '', 'FABRICAR', '', 'P', '5', 'QUINTEROS C.', 'O', 'JUEGOS  DE ESQUI ', 'UNIVERSALES  FABRICAR', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C49', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5859, 0, 0, 0, 0, 'PAE', '', '5859', '', 'REPARAR', '', '', '5', 'GERK G.', 'O', 'STORK  45-60', 'TAPAS  DE  V?LVULAS', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '819', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C50', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5860, 0, 0, 0, 0, 'PAE', '', '5860', '', 'FABRICAR', '', '', '2', 'VEGA J.', 'O', 'UNIVERSAL', 'POLEAS  Y CONO   PERFIL  5  V', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '835', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C51', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5861, 0, 0, 0, 0, 'PAE', '', '5861', '', 'ACONDICONAR', '', '', '1', 'VEGA J.', '0', 'UNIVERSAL', 'PROTECTOR', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C52', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5862, 0, 0, 0, 0, 'YPF', '', '5862', '', 'FABRICAR', '', '', '2', 'REYES  P.', 'O', 'WUELFEL 228', 'PERNO PIE  DE  BIELA', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '821', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C53', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5863, 0, 0, 0, 0, 'PAE', '', '5863', '', 'FABRICAR', '', '', '1', 'VEGA J.', 'O', 'UNIVERSAL', 'BUJE   SEG?N PLANO ', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '822', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C54', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5864, 0, 0, 0, 0, 'YPF ', '', '5864', '', 'FABRICAR', '', '', '1', 'QUINTEROS C.', 'O', 'UNIVERSAL', 'CHASIS  PCP', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10910', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C55', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5865, 0, 0, 0, 0, 'PAE', '', '5865', '', 'AGUJEREAR   ', '', '', '1', 'GERK G.', 'O', 'UNIVERSAL', 'MENSULA', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '823', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C56', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5866, 0, 0, 0, 0, 'YPF ', '', '5866', '', 'REPARAR', '', '', '1', 'URZAGASTI  J.', 'O', '', 'CHASIS COMPLETO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10979', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C57', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5867, 0, 0, 0, 0, 'YPF', '', '5867', '', 'REPARAR', '', '', '1', 'JEREZ  D.', 'O', 'MTD  9 TN', 'COJINETE DE CENTRO', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10976', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C58', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5868, 0, 0, 0, 0, 'YPF', '', '5868', '', 'FABRICAR', '22313 (1)', '', '1', 'JEREZ  D.', '0', 'MTD  9 TN', 'PERNO PIE  DE  BIELA', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C59', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5869, 0, 0, 0, 0, 'YPF', '', '5869', '', 'FABRICAR', '22314 (1)', '', '1', 'JEREZ  D.', '0', 'LUFKIN   228', 'PERNO PIE  DE  BIELA', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C60', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5870, 0, 0, 0, 0, 'YPF', '', '5870', '', 'REPARAR', '', '', '2', 'JEREZ  D.', 'O', 'LUFKIN 456-640', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10977', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C61', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5871, 0, 0, 0, 0, 'YPF', '', '5871', '', 'FABRICAR', '', '', '1', 'JEREZ  D.', 'O', 'DARCO  456', 'TUERCA  PARA  PERNO PIE DE BIELA', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10978', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C62', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5872, 0, 0, 0, 0, 'YPF', '', '5872', '', 'PROVEER', '', '', '80', 'JEREZ  D.', 'O', 'UNIVERSAL', 'BULONES DE 3/4"  CAB. CUADRADA', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10997', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C63', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5873, 0, 0, 0, 0, 'YPF ', '', '5873', '', 'FABRICAR', '', 'P', '1', 'JEREZ  D.', 'O', 'DARCO  456', 'TUERCA  DE GOLPE', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C64', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5874, 0, 0, 0, 0, 'YPF', '', '5874', '', 'FABRICAR', '', '', '4', 'REYES  P.', 'O', 'PUMP JACK  912', 'BULONES DE  CTE  CLA', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '619', 'May-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C65', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5875, 0, 0, 0, 0, 'PAE', '', '5875', '', 'FABRICAR', '', '', '1', 'GERK G.', 'O', 'STORK ', 'EXTRACTOR  DE  VALVULA', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '827', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C66', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5876, 0, 0, 0, 0, 'PAE', '', '5876', '', 'FABRICAR', '', '', '1', 'VEGA J.', 'O', 'UNIVERSAL', 'CONO  SEG?N  MUESTRA', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '826', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C67', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5877, 0, 0, 0, 0, 'YPF', '', '5877', '', 'FABRICAR', '22312 (2)', '', '2', 'MOSIMANN  G.', 'EG.728', 'WUELFEL 228', 'PERNO PIE  DE  BIELA', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C68', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5878, 0, 0, 0, 0, 'YPF', '', '5878', '', 'REPARAR', '', '', '1', 'MOSIMANN  G.', 'EG.696', 'SIAM C-228-SIAM 10 ', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11104', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C69', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5879, 0, 0, 0, 0, 'YPF', '', '5879', '', 'REPARAR', '', '', '1', 'MOSIMANN  G.', 'LC.879', 'LUFKIN  640', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11105', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C70', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5880, 0, 0, 0, 0, 'YPF', '', '5880', '', 'FABRICAR', '', '', '10', 'QUINTEROS C.', 'O', 'LUFKIN  456-640', 'BANDERAS  SEGUROS', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10999', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C71', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5881, 0, 0, 0, 0, 'YPF', '', '5881', '', 'FABRICAR', '', '', '10', 'QUINTEROS C.', 'O', 'DARCO 320', 'BANDERAS  SEGUROS', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10998', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C72', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5882, 0, 0, 0, 0, 'YPF', '', '5882', '', 'FABRICAR', '', '', '10', 'QUINTEROS C.', 'O', 'UNIVERSAL', 'JUEGOS  DE  ESQUI', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10999', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C73', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5883, 0, 0, 0, 0, 'YPF', '', '5883', '', 'REPARAR', '22317 (1)', '', '1', 'MOSIMANN  G.', 'EG.586', 'SIAM C-228', 'ARTICULACI?N  DE  COLA', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10980', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C74', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5884, 0, 0, 0, 0, 'YPF', '', '5884', '', 'FABRICAR', '22312 (2)', '', '2', 'MOSIMANN  G.', '0', 'WUELFEL 228', 'PERNO PIE  DE  BIELA', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10982', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C75', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5885, 0, 0, 0, 0, 'YPF', '', '5885', '', 'FABRICAR', '22313 (2)', '', '2', 'MOSIMANN  G.', 'LC.712', 'MTD  9 TN', 'PERNO PIE  DE  BIELA', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10983', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C76', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5886, 0, 0, 0, 0, 'YPF', '', '5886', '', 'FABRICAR', '23222 (1)', '', '1', 'MOSIMANN  G.', '0', 'LUFKIN  456-640', 'PERNO PIE  DE  BIELA', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10984', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C77', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5887, 0, 0, 0, 0, 'YPF ', '', '5887', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'LP.2466', 'DARCO 320', 'PARANTE DE TORRE', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C78', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5888, 0, 0, 0, 0, 'YPF', '', '5888', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'LP.1258', 'DARCO C 640', 'PARANTE DE TORRE', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C79', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5889, 0, 0, 0, 0, 'YPF', '', '5889', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.2438', 'SIAM C-228', 'ARTICULACI?N  DE  COLA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10987', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C80', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5890, 0, 0, 0, 0, 'YPF', '', '5890', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'AP.1', 'WUELFEL 228', 'ARTICULACI?N  DE  COLA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C81', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5891, 0, 0, 0, 0, 'YPF', '', '5891', '', 'FABRICAR', '', '', '6', 'ASAD   D.', 'O', 'UNIVERSAL', 'NIPLES  DE  1 "   ', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '831', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C82', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5892, 0, 0, 0, 0, 'YPF', '', '5892', '', 'FABRICAR', '', '', '2', 'LISANDRO', 'CG. 845-673', 'LUFKIN 456-640', 'PERNO PIE  DE  BIELA', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10991', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C83', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5893, 0, 0, 0, 0, 'YPF', '', '5893', '', 'FABRICAR', '', '', '1', 'LISANDRO', 'CE.691', 'LUFKIN 456-640', 'PERNO PIE  DE  BIELA', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10992', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C84', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5894, 0, 0, 0, 0, 'YPF', '', '5894', '', 'FABRICAR', '', '', '1', 'LISANDRO', 'CE.194', 'LUFKIN 456-640', 'PERNO PIE  DE  BIELA', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10993', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C467', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5895, 0, 0, 0, 0, 'YPF', '', '5895', '', 'FABRICAR', '22328 (1)', 'P', '1', 'LISANDRO', 'O', 'DARCO C 640', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11270', ''),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C86', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5896, 0, 0, 0, 0, 'SNP', '', '5896', '', 'SOLAR  Y REPARAR', '', '', '1', 'GIMENEZ O.', 'O', 'WORTHIGTON', 'BOMBA  CENTRIFUGA', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C87', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5897, 0, 0, 0, 0, 'YPF', '', '5897', '', 'FABRICAR', '', '', '3', 'FLORES O.', 'O', 'UNIVERSAL', 'ESTANTERIAS', 'PL LH3', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11025', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C88', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5898, 0, 0, 0, 0, 'YPF', '', '5898', '', 'FABRICAR', '', '', '1', 'FLORES O.', 'O', 'UNIVERSAL', 'BANCO DE  TRABAJO', 'PL LH3', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11025', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C89', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5899, 0, 0, 0, 0, 'YPF', '', '5899', '', 'REPARAR', '', '', '1', 'GUZMAN  E.', 'O', 'STORK 45-60', 'EXCENTRICO - 2  BIELAS', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11024', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C90', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5900, 0, 0, 0, 0, 'YPF', '', '5900', '', 'FABRICAR', '', '', '2', 'BORIS', 'O', 'UNIVERSAL', 'CHASIS  PCP  SOLOS', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11014', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C91', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5901, 0, 0, 0, 0, 'YPF', '', '5901', '', 'FABRICAR', '', '', '2', 'BORIS', 'O', 'UNIVERSAL', 'CHASIS COMPLETO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11013', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C92', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5902, 0, 0, 0, 0, 'YPF', '', '5902', '', 'REPARAR', '', '', '1', 'LISANDRO', 'CE.863', 'LUFKIN  640', 'POSTE MAESTRO ', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10995', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C93', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5903, 0, 0, 0, 0, 'YPF', '', '5903', '', 'REPARAR', '', '', '1', 'LERDA C.', 'O', 'MTD  9 TN', 'ARTICULACI?N  DE  COLA', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10994', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C94', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5904, 0, 0, 0, 0, 'PAE', '', '5904', '', 'REPARAR', '', '', '1', 'DIAZ   V.', 'O', 'MEP', 'EJE  FAN', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '622', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C95', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5905, 0, 0, 0, 0, 'PAE', '', '5905', '', 'FABRICAR', '', '', '2', 'DIAZ   V.', 'O', 'WSHA', 'PALANCA  DE GOBERNOR', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '829', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C96', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5906, 0, 0, 0, 0, 'YPF', '', '5906', '', 'REPARAR  INTEGRAL', '', '', '1', 'LERDA C.', 'O', 'CKH 10Tn', 'AIB  COMPLETO', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11128', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C97', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5907, 0, 0, 0, 0, 'YPF', '', '5907', '', 'ROSCADO', '', '', '1', 'ASAD   D.', 'O', '', 'CA?O GALVANIZADO DIAM 4"', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '601', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C98', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5908, 0, 0, 0, 0, 'YPF', '', '5908', '', 'MECANIZAR', '', '', '1', 'REYES  P.', 'PT 1169', 'LUFKIN M-228', 'COJINETE DE CENTRO', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '602', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C99', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5909, 0, 0, 0, 0, 'YPF', '', '5909', '', 'REPARAR', '', '', '1', 'JEREZ  D.', 'O', 'LUFKIN 456-640', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11000', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C100', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5910, 0, 0, 0, 0, 'YPF', '', '5910', '', 'REPARAR', '', '', '1', 'BORIS', 'LC.618', 'LUFKIN  456-640', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11004', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C101', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5911, 0, 0, 0, 0, 'YPF', '', '5911', '', 'REPARAR', '', '', '1', 'BORIS', 'CDG.900', 'LUFKIN  456-640', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11005', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C102', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5912, 0, 0, 0, 0, 'YPF', '', '5912', '', 'REPARAR', '', '', '1', 'BORIS', 'EG.321', 'MTD  9 TN', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11006', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C103', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5913, 0, 0, 0, 0, 'YPF', '', '5913', '', 'REPARAR', '', '', '1', 'BORIS', 'EG.347', 'MTD  9 TN', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11007', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C104', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5914, 0, 0, 0, 0, 'YPF', '', '5914', '', 'REPARAR', '', '', '1', 'BORIS', 'EG.688', 'MTD  9 TN', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11008', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C105', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5915, 0, 0, 0, 0, 'YPF', '', '5915', '', 'REPARAR', '', '', '1', 'BORIS', 'LC.711', 'SIAM C-228', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11009', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C106', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5916, 0, 0, 0, 0, 'YPF', '', '5916', '', 'REPARAR', '', '', '1', 'BORIS', 'CDG.834', 'VULCAN C-320', 'JUEGO DE PATINES', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11010', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C107', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5917, 0, 0, 0, 0, 'YPF ', '', '5917', '', 'REPARAR', '', '', '1', 'BORIS', 'EG.719', 'SIAM C-228', 'PALANCA  DE FRENO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11011', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C108', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5918, 0, 0, 0, 0, 'YPF', '', '5918', '', 'REPARAR', '', '', '1', 'BORIS', 'EG.106', 'SIAM C-228', 'PALANCA  DE FRENO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11012', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C109', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5919, 0, 0, 0, 0, 'YPF ', '', '5919', '', 'PROVEER', '', '', '15', 'QUINTEROS C.', 'O', 'UNIVERSAL', 'CHAVETAS GRANDES', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11015', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C110', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5920, 0, 0, 0, 0, 'YPF', '', '5920', '', 'PROVEER', '', '', '10', 'QUINTEROS C.', 'O', 'UNIVERSAL', 'CHAVETAS  PARTIDAS  MEDIANAS', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11016', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C111', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5921, 0, 0, 0, 0, 'YPF', '', '5921', '', 'REPARAR', '', '', '1', 'HERNANDEZ  D.', 'O', 'SBS C-228', 'AIB  COMPLETO', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C112', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5922, 0, 0, 0, 0, 'YPF', '', '5922', '', 'FABRICAR', '', '', '4', 'JUAN FERGUSON', 'O', 'UNIVERSAL', 'ARANDELA   SEG?N  MUESTRA', 'PL LH3', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C113', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5923, 0, 0, 0, 0, 'YPF', '', '5923', '', 'FABRICAR  EN GRILLON', '', '', '4', 'JUAN FERGUSON', 'O', 'UNIVERSAL', 'DEFLECTORES', 'PL LH3', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C114', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5924, 0, 0, 0, 0, 'YPF', '', '5924', '', 'FABRICAR  EN GRILLON', '', '', '8', 'GODOY  G.', 'O', 'UNIVERSAL', 'DEFLECTORES', 'PL CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C115', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5925, 0, 0, 0, 0, 'PAE', '', '5925', '', 'MECANIZAR', '', '', '1', 'VEGA J.', '0', 'UNIVERSAL', 'CONO   MECANIZAR', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C116', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5926, 0, 0, 0, 0, 'PAE', '', '5926', '', 'MECANIZAR', '', '', '8', 'VEGA J.', 'O', 'UNIVERSAL', 'BULONES  REBAJAR  MEDIA  GU?A', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '836', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C117', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5927, 0, 0, 0, 0, 'YPF', '', '5927', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.1426', 'LUFKIN  456-640', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11069', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C118', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5928, 0, 0, 0, 0, 'YPF ', '', '5928', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.2751', 'LUFKIN  456-640', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11070', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C119', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5929, 0, 0, 0, 0, 'YPF', '', '5929', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'AB.25', 'UNIVERSAL', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11071', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C120', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5930, 0, 0, 0, 0, 'YPF', '', '5930', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.2284', 'SIAM C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11072', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C121', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5931, 0, 0, 0, 0, 'YPF', '', '5931', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.2357', 'UNIVERSAL', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11073', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C122', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5932, 0, 0, 0, 0, 'YPF', '', '5932', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.361', 'SIAM C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11074', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C123', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5933, 0, 0, 0, 0, 'YPF', '', '5933', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LM. 284', 'SIAM  10  TN', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11075', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C124', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5934, 0, 0, 0, 0, 'YPF', '', '5934', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LM.611', 'SIAM  C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11076', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C125', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5935, 0, 0, 0, 0, 'YPF', '', '5935', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.1331', 'SIAM C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11077', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C126', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5936, 0, 0, 0, 0, 'YPF', '', '5936', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.2192', 'MTD  9 TN', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11022', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C127', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5937, 0, 0, 0, 0, 'YPF', '', '5937', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.1183', 'MTD  9 TN', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11021', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C128', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5938, 0, 0, 0, 0, 'YPF ', '', '5938', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.2183', 'SBS C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11078', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C129', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5939, 0, 0, 0, 0, 'YPF', '', '5939', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LM.542', 'DARCO  456', 'JUEGO DE PATINES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11017', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C130', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5940, 0, 0, 0, 0, 'YPF', '', '5940', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.1123', 'DARCO 228Y', 'JUEGO DE PATINES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11018', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C131', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5941, 0, 0, 0, 0, 'YPF', '', '5941', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.1132', 'VULCAN  M 456', 'JUEGO DE PATINES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11079', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C132', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5942, 0, 0, 0, 0, 'YPF ', '', '5942', '', 'FABRICAR', '', 'P', '1', 'URZAGASTI S.', 'LP.1056', 'MTD  9 TN', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11080', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C133', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5943, 0, 0, 0, 0, 'YPF', '', '5943', '', 'FABRICAR', '21315 (1)', '', '1', 'URZAGASTI S.', 'LM.284', 'SIAM C-228', 'PERNO PIE  DE  BIELA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11081', 'Sep-16');
INSERT INTO `servicioscli` (`apartado`, `prioridad`, `cifnif`, `ciudad`, `codagente`, `codalmacen`, `codcliente`, `coddir`, `coddivisa`, `codejercicio`, `codigo`, `codpago`, `codpais`, `codpostal`, `codserie`, `direccion`, `editable`, `garantia`, `fecha`, `hora`, `femail`, `fechafin`, `horafin`, `fechainicio`, `horainicio`, `idservicio`, `idalbaran`, `idprovincia`, `irpf`, `neto`, `nombrecliente`, `numero`, `numero2`, `observaciones`, `descripcion`, `solucion`, `usuarioep`, `idmep`, `respep`, `yacep`, `material`, `material_estado`, `accesorios`, `porcomision`, `provincia`, `recfinanciero`, `tasaconv`, `total`, `totaleuros`, `totalirpf`, `totaliva`, `totalrecargo`, `idestado`, `numsalida`, `numcertificado`) VALUES
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C134', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5944, 0, 0, 0, 0, 'YPF', '', '5944', '', 'FABRICAR', '22314 (1)', '', '1', 'URZAGASTI S.', 'LP.433', 'LUFKIN C- 228', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11082', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C135', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5945, 0, 0, 0, 0, 'YPF', '', '5945', '', 'CAMBIAR  RODAM.', '', '', '1', 'URZAGASTI S.', 'LP.2284', 'SIAM C-228', 'EJE  INTERMEDIOO  DE REDUCTOR', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '10845', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C136', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5946, 0, 0, 0, 0, 'YPF', '', '5946', '', 'CAMBIAR  RODAM.', '', '', '1', 'URZAGASTI S.', 'LP.2284', 'SIAM C-228', 'EJE  VELOZ  DE  REDUCTOR', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11019', 'Mar-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C137', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5947, 0, 0, 0, 0, 'YPF', '', '5947', '', 'REPARAR', '', '', '1', 'JEREZ  D.', 'O', 'LUFKIN  456-640', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11020', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C138', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5948, 0, 0, 0, 0, 'YPF', '', '5948', '', 'REPARAR', '', '', '1', 'LERDA C.', 'O', 'DARCO  640', 'AIB  COMPLETO', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11198', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C334', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5949, 0, 0, 0, 0, 'YPF', '', '5949', '', 'REPARAR', '', '', '1', 'GUZMAN  E.', 'O', 'DARCO PUM JACK', 'AIB  COMPLETO', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '627', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C140', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5950, 0, 0, 0, 0, 'YPF', '', '5950', '', 'FABRICAR', '', '', '10', 'JEREZ  D.', 'O', 'LUFKIN 456-640', 'REGISTRO  CABEZA  DE MULA', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11083', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C141', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5951, 0, 0, 0, 0, 'YPF ', '', '5951', '', 'FABRICAR', '', '', '2', 'AYBAR  M.', 'O', 'UNIVERSAL', 'ESTANTERIA   PARA  COMP.  BBA', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C403', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5952, 0, 0, 0, 0, 'YPF', '', '5952', '', 'REPARAR', '', '', '1', 'REYES  P.', 'O', 'LUFKIN  456-640', 'COJINETE DE CENTRO', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '848', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C143', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5953, 0, 0, 0, 0, 'PAE', '', '5953', '', 'MECANIZAR', '', '', '1', 'VEGA J.', 'O', 'ALWELLRS', 'ACOPLE GUMY  1/2  ', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '606', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C144', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5954, 0, 0, 0, 0, 'PAE', '', '5954', '', 'MECANIZAR  SEG?N MUESTRA', '', '', '1', 'VEGA J.', 'O', 'WSHA', 'PIEZA  CUPLA  ', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '606', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C441', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5955, 0, 0, 0, 0, 'YPF', '', '5955', '', 'REPARAR', '', '', '1', 'REYES  P.', 'EC.1363', 'SBS C-228', 'COJINETE DE CENTRO', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '650', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C443', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5956, 0, 0, 0, 0, 'YPF ', '', '5956', '', 'REPARAR', '', '', '1', 'REYES  P.', 'O', 'SIAM C-228', 'COJINETE DE CENTRO', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '649', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C147', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5957, 0, 0, 0, 0, 'YPF', '', '5957', '', 'REPARAR', '', '', '1', 'MOSIMANN  G.', '', 'VULCAN UP 15 TN', 'AIB  COMPLETO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11229', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C148', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5958, 0, 0, 0, 0, 'YPF ', '', '5958', '', 'PROVISION', '', '', '4', 'URZAGASTI  J.', 'O', 'SIAM C-228', 'RETENES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11030', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C149', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5959, 0, 0, 0, 0, 'YPF', '', '5959', '', 'PROVISION', '', '', '4', 'GODOY  G.', 'O', 'UNIVERSAL', 'ACOPLES  GUMMI', 'PL CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11087', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C150', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5960, 0, 0, 0, 0, 'YPF', '', '5960', '', 'FABRICAR', '', '', '32', 'AYBAR  M.', 'O', 'NATIONAL  J.275', 'SAPITOS  ', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '607', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C151', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5961, 0, 0, 0, 0, 'YPF ', '', '5961', '', 'CONSTRUCCI?N', '', '', '1', 'AYBAR  M.', 'O', 'NATIONAL  J.275', 'EXTRACTOR  PARA   BOMBA', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '607', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C152', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5962, 0, 0, 0, 0, 'YPF', '', '5962', '', 'PROVISION', '', '', '6', 'AYBAR  M.', 'O', 'UNIVERSAL', 'ACCESORIOS   DE PLASTICO  ', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '603', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C153', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5963, 0, 0, 0, 0, 'YPF ', '', '5963', '', 'REPARAR', '', '', '1', 'MOSIMANN  G.', 'O', 'VULCAN 303', 'AIB  COMPLETO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '146', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C154', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5964, 0, 0, 0, 0, 'PAE', '', '5964', '', 'FABRICAR', '', '', '1', 'VEGA J.', 'O', 'STORK', 'PRENSA  EMPAQUETADURA', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '608/613', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '', NULL, 'ARS', '2016', 'SER2016C155', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5965, 0, 0, 0, 0, '', '', '5965', '', '2612', '', '', '', '', '', 'IDEM  5953', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C156', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5966, 0, 0, 0, 0, 'YPF ', '', '5966', '', 'PROVISION', '', '', '40', 'REYES  P.', '', 'UNIVERSAL', 'BULONES   5/8"  x  1 1/2"', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '606', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C157', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5967, 0, 0, 0, 0, 'YPF', '', '5967', '', 'PROVISION', '', '', '100', 'JEREZ  D.', 'O', 'UNIVERSAL', 'BULONES  3/4"  x  2 1/2"', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11031', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C158', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5968, 0, 0, 0, 0, 'YPF ', '', '5968', '', 'PROVISION', '', '', '50', 'JEREZ  D.', 'O', 'UNIVERSAL', 'BULONES  3/8"  x  1 "', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11032', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C159', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5969, 0, 0, 0, 0, 'YPF', '', '5969', '', 'PROVISION', '', '', '100', 'URZAGASTI S.', 'O', 'UNIVERSAL', 'BULONES  3/4"  x  2 1/2"', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11034', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C160', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5970, 0, 0, 0, 0, 'SNP', '', '5970', '', 'REPARAR', '', '', '1', 'CASARIN  D.', 'O', 'LEGRAND  C 912', 'VIGA- COJINETE DE  COLA', 'EH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C161', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5971, 0, 0, 0, 0, 'YPF', '', '5971', '', 'REPARAR', '22317 (1)', 'P', '1', 'URZAGASTI S.', 'LP.2241', 'SIAM C-228', 'ARTICULACI?N  DE  COLA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C162', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5972, 0, 0, 0, 0, 'YPF ', '', '5972', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.2191', 'SIAM C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11041', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C163', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5973, 0, 0, 0, 0, 'YPF', '', '5973', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.1612', 'SIAM C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11042', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C164', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5974, 0, 0, 0, 0, 'YPF', '', '5974', '', 'FABRICAR', '', 'P', '1', 'URZAGASTI S.', 'LP.1004', 'MTD  9 TN', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11023', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C165', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5975, 0, 0, 0, 0, 'YPF', '', '5975', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.1265', 'WUELFEL 228', 'JUEGO DE PATINES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11044', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C166', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5976, 0, 0, 0, 0, 'YPF', '', '5976', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.2284', 'SIAM C-228', 'EJE  VELOZ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11045', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C167', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5977, 0, 0, 0, 0, 'YPF ', '', '5977', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.2176', 'VULCAN 303', 'ARTICULACI?N  DE  COLA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11046', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C168', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5978, 0, 0, 0, 0, 'YPF', '', '5978', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'O', 'VULCAN UP 15 TN', 'CHASIS', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11047', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C169', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5979, 0, 0, 0, 0, 'YPF', '', '5979', '', 'FABRICAR', '', 'P', '1', 'URZAGASTI S.', 'O', 'MTD  9 TN', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11048', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C170', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5980, 0, 0, 0, 0, 'YPF', '', '5980', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'O', 'MTD  9 TN', 'COJINETE DE CENTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11049', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C171', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5981, 0, 0, 0, 0, 'YPF', '', '5981', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.1331', 'DARCO 228Y', 'JUEGO DE PATINES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11050', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C172', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5982, 0, 0, 0, 0, 'YPF', '', '5982', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.1301', 'DARCO  456', 'JUEGO DE PATINES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11051', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C173', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5983, 0, 0, 0, 0, 'YPF', '', '5983', '', 'MANIVELAS DESPLAZADAS', '', '', '1', 'URZAGASTI S.', 'LP.204', 'LUFKIN M-640', 'REDUCTOR', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11052', 'May-16'),
('', 0, '', '', '1', 'ALG', '', NULL, 'ARS', '2016', 'SER2016C174', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5984, 0, 0, 0, 0, '', '', '5984', '', '', '', '', '', '', '', 'ANULADO', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C175', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5985, 0, 0, 0, 0, 'YPF', '', '5985', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'O', 'MTD  9 TN', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11053', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C176', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5986, 0, 0, 0, 0, 'YPF ', '', '5986', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'O', 'SIAM C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11054', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C177', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5987, 0, 0, 0, 0, 'YPF', '', '5987', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'O', 'LUFKIN   228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11055', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C178', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5988, 0, 0, 0, 0, 'YPF', '', '5988', '', 'REPARAR', '', '', '1', 'JEREZ  D.', 'ECH.161', 'LUFKIN M 640', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11058', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C179', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5989, 0, 0, 0, 0, 'YPF', '', '5989', '', 'REPARAR', '', '', '1', 'JEREZ  D.', 'ECH.21', 'AMSCOT  640', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11060', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C180', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5990, 0, 0, 0, 0, 'YPF', '', '5990', '', 'REPARAR', '', '', '1', 'JEREZ  D.', 'ECH.177', 'DARCO 320', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11056', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C181', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5991, 0, 0, 0, 0, 'YPF', '', '5991', '', 'FABRICAR', '', '', '1', 'JEREZ  D.', 'CV.54', 'LUFKIN  M 456-640', 'PERNO PIE  DE  BIELA', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11057', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C182', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5992, 0, 0, 0, 0, 'YPF', '', '5992', '', 'REPARAR', '', '', '1', 'JEREZ  D.', 'CGVN 74', 'DARCO 320', 'PARANTE DE TORRE', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11061', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C183', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5993, 0, 0, 0, 0, 'YPF', '', '5993', '', 'REPARAR', '', '', '1', 'JEREZ  D.', 'CPORX 1', 'LUFKIN  M 912', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11059', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C184', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5994, 0, 0, 0, 0, 'YPF', '', '5994', '', 'MECANIZAR DIAM. INT.', '', '', '6', 'URZAGASTI  J.', 'O', 'DEUTZ', 'VOLANTE ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11085', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C185', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5995, 0, 0, 0, 0, 'PAE', '', '5995', '', 'AJUSTAR INTERIOR', '', '', '2', 'VEGA J.', '0', 'UNIVERSAL', 'CONOS  Y POLEA', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C186', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5996, 0, 0, 0, 0, 'SNP', '', '5996', '', 'CAMBIO DE RODAMIENTO', '', '', '1', 'GIMENEZ O.', 'O', 'LUFKIN M 912', 'REDUCTOR', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11272', ''),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C187', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5997, 0, 0, 0, 0, 'SNP', '', '5997', '', 'MECANIZAR', '', '', '1', 'QUIROZ  R.', 'O', 'FAVRA', 'VALVULA  ASIENTO Y  APOYO JUNTA', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C188', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5998, 0, 0, 0, 0, 'YPF', '', '5998', '', 'PROVISION  COMPLETA', '', '', '500', 'URZAGASTI  J.', 'O', 'UNIVERSAL', 'BULONES  DE 3/8"  x  1  "', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11086', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C189', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 5999, 0, 0, 0, 0, 'YPF', '', '5999', '', 'CONSTRUCCI?N', '', '', '1', 'LERDA C.', 'O', 'LUFKIN  M 912', 'CUBRECORREA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11088', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C190', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6000, 0, 0, 0, 0, 'YPF', '', '6000', '', 'FABRICAR  NUEVA', '', '', '4', 'SILVESTRE D.', 'O', 'UNIVERSAL', 'BRIDAS  ', 'PL LH3', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '620', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C191', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6001, 0, 0, 0, 0, 'YPF ', '', '6001', '', 'PROVISI?N  ', '', '', '2', 'GODOY  G.', 'O', 'GRORANO  S.A.', 'BOMBAS  DE  ACEITE', 'PL CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11059', 'Apr-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C192', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6002, 0, 0, 0, 0, 'YPF ', '', '6002', '', 'FABRICAR', '21315 (2)', '', '2', 'BORIS', 'EG.750', 'SIAM C-228', 'PERNO PIE  DE  BIELA', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11096', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C193', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6003, 0, 0, 0, 0, 'YPF', '', '6003', '', 'FABRICAR', '', '', '1', 'BORIS', 'EG.73', 'SIAM C-228', 'PERNO PIE  DE  BIELA', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11095', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C194', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6004, 0, 0, 0, 0, 'YPF ', '', '6004', '', 'FABRICAR', '', '', '1', 'BORIS', 'O', 'DARCO C 640', 'PPB', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11094', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C195', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6005, 0, 0, 0, 0, 'YPF', '', '6005', '', 'REPARAR', '', 'P', '2', 'BORIS', 'O', 'SIAM C-228', 'PALANCAS  DE   FRENO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11093', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C196', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6006, 0, 0, 0, 0, 'YPF', '', '6006', '', 'REPARAR', '', '', '2', 'BORIS', 'LC.140/516', 'SIAM C-228', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11092', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C197', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6007, 0, 0, 0, 0, 'YPF', '', '6007', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'O', 'VULCAN UP 15 TN', 'COJINETE DE CENTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11103', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C198', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6008, 0, 0, 0, 0, 'YPF', '', '6008', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'O', 'VULCAN 303', 'COJINETE DE CENTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11102', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C199', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6009, 0, 0, 0, 0, 'YPF ', '', '6009', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'O', 'DARCO  456', 'PARANTE DE TORRE', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11101', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C200', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6010, 0, 0, 0, 0, 'YPF', '', '6010', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'O', 'DARCO 228Y', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11100', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C201', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6011, 0, 0, 0, 0, 'YPF', '', '6011', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'O', 'DARCO 228Y', 'COJINETE DE CENTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11099', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C202', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6012, 0, 0, 0, 0, 'YPF', '', '6012', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'LM.111', 'VULCAN 303', 'COJINETE DE CENTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11097', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C203', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6013, 0, 0, 0, 0, 'YPF', '', '6013', '', 'FABRICAR', '', 'P', '1', 'URZAGASTI S.', 'O', 'CKH 10Tn', 'CHASIS  SOLO  P/ PCP  ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11098', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C262', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6014, 0, 0, 0, 0, 'YPF', '', '6014', '', 'REFORMAR', '', '', '1', 'GUZMAN  E.', 'O', 'SBS C-320', 'COJINETE DE CENTRO', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11347', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C205', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6015, 0, 0, 0, 0, 'YPF', '', '6015', '', 'PROVEER PARA SIST. FRENO', '', '', '26', 'URZAGASTI S.', '0', 'WUELFEL 228', 'RESORTES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '669', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C7', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6016, 0, 0, 0, 0, 'YPF ', '', '6016', '', 'FABRICAR', '', 'Reyes', '4', 'REYES  P.', 'O', 'UNIVERSAL', 'REDUCCI?N DE  TAPON     5 -8', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '679', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C207', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6017, 0, 0, 0, 0, 'YPF ', '', '6017', '', 'FABRICAR', '', '', '1', 'METRAL   SICTEL', 'O', 'BRIDA  BUJE P/ inst.', '', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11090', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C208', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6018, 0, 0, 0, 0, 'YPF', '', '6018', '', 'FABRICAR', '', '', '2', 'URZAGASTI S.', 'O', 'UNIVERSAL', 'CHASIS  P/ pcp  COMPLETO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C209', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6019, 0, 0, 0, 0, 'YPF', '', '6019', '', 'FABRICAR', '', '', '2', 'URZAGASTI S.', 'O', 'UNIVERSAL', 'CHASIS P / pcp  COMPLETO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11134', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C210', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6020, 0, 0, 0, 0, 'YPF', '', '6020', '', 'FABRICAR', '', '', '7', 'QUINTEROS P.', 'O', 'UNIVERSAL', 'JUEGOS  DE  ESQUI', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11244', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C211', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6021, 0, 0, 0, 0, 'YPF', '', '6021', '', 'FABRICAR', '', '', '5', 'QUINTEROS C.', 'O', 'UNIVERSAL', 'JUEGOS  DE  ESQUI', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11269', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C212', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6022, 0, 0, 0, 0, 'YPF ', '', '6022', '', 'FABRICAR', '', '', '2', 'AYBAR  M.', 'O', 'UNIVERSAL', 'BANDEJAS   40 X  40 X15   60X60X15', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C213', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6023, 0, 0, 0, 0, 'YPF', '', '6023', '', 'FABRICAR', '', '', '1', 'AYBAR  M.', 'O', 'UNIVERSAL', 'EXTRACTOR  GRANDE', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C214', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6024, 0, 0, 0, 0, 'YPF ', '', '6024', '', 'FABRICAR', '', '', '1', 'AYBAR  M.', 'O', 'UNIVERSAL', 'EXCTRACTOR  DE VALVULAS', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C215', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6025, 0, 0, 0, 0, 'YPF ', '', '6025', '', 'FABRICAR', '', '', '1', 'AYBAR  M.', 'O', 'UNIVERSAL', 'CUBRECORREA  ', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C216', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6026, 0, 0, 0, 0, 'PAE', '', '6026', '', 'CONSTRUCCI?N', '', '', '1', 'VEGA J.', '0', 'UNIVERSAL', 'SOPORTE  DE BATERIA', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '677', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C217', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6027, 0, 0, 0, 0, 'YPF', '', '6027', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', 'LC.609', 'SIAM C-228', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11114', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C218', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6028, 0, 0, 0, 0, 'YPF ', '', '6028', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', 'LC.792', 'SIAM C-228', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11115', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C219', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6029, 0, 0, 0, 0, 'YPF ', '', '6029', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', 'EG.593', 'SEM  C-228', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11116', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C220', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6030, 0, 0, 0, 0, 'YPF', '', '6030', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', 'LC.947', 'LUFKIN 640', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11117', 'May-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C221', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6031, 0, 0, 0, 0, 'PAE', '', '6031', '', 'PINTAR', '', '', '1', 'VEGA J.', 'O', 'UNIVERSAL', 'CAJON  DE  HERRAMIENTAS', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '624', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '', NULL, 'ARS', '2016', 'SER2016C222', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6032, 0, 0, 0, 0, '', '', '6032', '', '', '', '', '', '', '', 'ANULADO', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C223', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6033, 0, 0, 0, 0, 'YPF', '', '6033', '', 'FABRICAR', '22314 (1)', '', '1', 'QUINTEROS P.', '0', 'LUFKIN C- 228', 'PPB', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11108', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C224', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6034, 0, 0, 0, 0, 'YPF ', '', '6034', '', 'FABRICAR', '', 'P', '1', 'QUINTEROS P.', 'O', 'MTD  9 TN', 'PPB', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11109', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C225', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6035, 0, 0, 0, 0, 'YPF', '', '6035', '', 'FABRICAR', '', '', '1', 'QUINTEROS P.', 'LC.943', 'DARCO  456-640', 'PPB', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11110', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C226', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6036, 0, 0, 0, 0, 'YPF', '', '6036', '', 'FABRICAR', '21315 (2)', 'P', '2', 'QUINTEROS P.', 'LC.870', 'SIAM C-228', 'PPB', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11111', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C227', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6037, 0, 0, 0, 0, 'YPF ', '', '6037', '', 'FABRICAR', '', '', '1', 'QUINTEROS P.', 'EG.475', 'LUFKIN   228', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11112', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C228', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6038, 0, 0, 0, 0, 'YPF ', '', '6038', '', 'FABRICAR', '', '', '1', 'QUINTEROS P.', 'LC.723', 'LUFKIN  640', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11113', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C229', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6039, 0, 0, 0, 0, 'YPF ', '', '6039', '', 'REPARAR', '', 'P', '1', 'QUINTEROS P.', 'LC.816', 'VULCAN  320', 'COJINETE DE CENTRO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C230', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6040, 0, 0, 0, 0, 'YPF ', '', '6040', '', 'FABRICAR', '', '', '2', 'QUINTEROS P.', 'LC.706', 'DARCO  640', 'PERNO PIE  DE  BIELA', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11118', 'May-16'),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C231', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6041, 0, 0, 0, 0, 'SNP', '', '6041', '', 'REFORMAR', '', '', '1', 'CASTILLO  O.', '', 'SBS C-228', 'COJINETE DE CENTRO', 'LH3', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C232', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6042, 0, 0, 0, 0, 'SNP', '', '6042', '', 'REFORMAR', '', '', '1', 'CASTILLO  O.', '', 'SBS  C -228', 'COJINETE  DE COLA', 'LH3', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C233', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6043, 0, 0, 0, 0, 'YPF', '', '6043', '', 'REPARAR', '', '', '1', 'AYBAR  M.', '', 'UNIVERSAL', 'BOMBA  LUBRICADORA', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C302', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6044, 0, 0, 0, 0, 'YPF ', '', '6044', '', 'REPARAR', '', '', '1', 'REYES  P.', '', 'CKH 10Tn', 'AIB  COMPLETO', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '680', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C235', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6045, 0, 0, 0, 0, 'PAE', '', '6045', '', 'REPARAR', '', '', '4', 'MARCE?UK  M.', '0', 'UNIVERSAL', ' RUEDAS ', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '646', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C236', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6046, 0, 0, 0, 0, 'PAE', '', '6046', '', 'FABRICAR', '', '', '3', 'GERK G.', '0', 'UNIVERSAL', 'POLEAS   SEG?N PLANO D.320', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C237', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6047, 0, 0, 0, 0, 'PAE', '', '6047', '', 'FABRICAR', '', '', '3', 'GERK G.', '0', 'UNIVERSAL', 'POLEAS SEG?N PLANO  D. 188 MM', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '676', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C238', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6048, 0, 0, 0, 0, 'YPF ', '', '6048', '', 'FABRICAR', '', '', '1', 'FLORES O.', 'O', 'BBA CENTRIFUGA', 'CONST. BULON  CON ARANDELA', 'PL LH3', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C239', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6049, 0, 0, 0, 0, 'YPF', '', '6049', '', 'ROSCAR', '', '', '1', 'AYBAR  M.', 'O', 'UNIVERSAL', 'VARILLA DE   3/4 "   con  ROSCA -ROSCA', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 'Ferguson Juan', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C240', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6050, 0, 0, 0, 0, 'PAE', '', '6050', '', 'FABRICAR', '', '', '2', 'GERK G.', '0', 'CATTERPILLER', 'SOPORTES  RODAMIENTOS ', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '678', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C241', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6051, 0, 0, 0, 0, 'YPF ', '', '6051', '', 'FABRICAR', '', '', '2', 'VEGA F.', 'ME.1122', 'PIGNONE 9Tn', 'PERNO PIE  DE  BIELA', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11129', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C242', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6052, 0, 0, 0, 0, 'YPF', '', '6052', '', 'FABRICAR', '', '', '34', 'URZAGASTI  J.', 'O', 'UNIVERSAL', 'ANCLAJES  DE  0.80  / 0.60  mts.', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11119', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C243', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6053, 0, 0, 0, 0, 'YPF', '', '6053', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', 'EG.941', 'AMSCOT  320', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11120', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C244', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6054, 0, 0, 0, 0, 'YPF ', '', '6054', '', 'PROVEER', '', '', '6', 'AYBAR  M.', 'O', 'UNIVERSAL', '4.ESTANTERIAS    /  2. GABINETES', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 'Carpeta', 'Feb-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C245', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6055, 0, 0, 0, 0, 'YPF ', '', '6055', '', 'PROVEER', '', '', '4', 'SILVESTRE D.', 'O', 'UNIVERSAL', 'CAJAS DE HERRAMIENTAS', 'PL LH3', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11123/24/25', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C246', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6056, 0, 0, 0, 0, 'YPF ', '', '6056', '', 'FABRICAR', '', '', '4', 'AYBAR  M.', 'O', 'SIAM  J  275', 'CUBRE CORREA', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11126', 'May-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C247', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6057, 0, 0, 0, 0, 'YPF', '', '6057', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', '0', 'VULCAN  UP 15 TN', 'REDUCTOR-CHASIS-F CHASIS .ESC.  PPBIELA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11245', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C248', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6058, 0, 0, 0, 0, 'YPF', '', '6058', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', '0', 'MTD  9 TN', 'VIGA-CTE CTO-', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11132', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C249', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6059, 0, 0, 0, 0, 'YPF ', '', '6059', '', 'REPARAR', '22322 K (1)', '', '1', 'URZAGASTI S.', '0', 'VULCAN 303', 'ARTICULACI?N  DE  COLA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11133', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C139', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6060, 0, 0, 0, 0, 'YPF', '', '6060', '', 'REPARAR', '', 'Hernandez', '1', 'HERNANDEZ  D.', '312- ED2.', 'SIAM C-228', 'CINTA  DE FRENO ', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '682', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C263', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6061, 0, 0, 0, 0, 'YPF', '', '6061', '', 'REPARAR', '', '', '1', 'GUZMAN  E.', 'EC.1363', 'SBS  C-228', 'COJINETE DE CENTRO', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11348', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C252', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6062, 0, 0, 0, 0, 'YPF ', '', '6062', '', 'REPARAR', '', '', '12', 'AYBAR  M.', 'O', 'SIAM J 275', 'CAJA PRENSA  EMPAQUETADURA', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '846', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C253', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6063, 0, 0, 0, 0, 'SNP', '', '6063', '', 'REPARAR', '', '', '1', 'LOPEZ  J.', 'CS. 2236', 'LUFKIN  456 ', 'REDUCTOR', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C254', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6064, 0, 0, 0, 0, 'SNP', '', '6064', '', 'REPARAR', '', '', '1', 'LOPEZ  J.', 'CS 3011', 'AMPSCOT 912 ', 'REDUCTOR', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C255', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6065, 0, 0, 0, 0, 'SNP', '', '6065', '', 'REPARAR', '', '', '1', 'LOPEZ  J.', 'CO 29', 'WUELFEL 228', 'ARTICULACI?N  DE  COLA', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C256', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6066, 0, 0, 0, 0, 'YPF', '', '6066', '', 'ADAPTAR GUMMY-CHASIS', '', '', '1', 'SILVESTRE D.', 'O', 'UNIVERSAL', 'BOMBA  -MOTOR ', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '631/850', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C257', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6067, 0, 0, 0, 0, 'YPF ', '', '6067', '', 'REPARAR', '', '', '1', 'LERDA C.', '', 'DARCO  640', 'REDUCTOR  REEMPL.  64001-016', 'LH  ', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11198', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C258', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6068, 0, 0, 0, 0, 'YPF   ', '', '6068', '', 'REPARAR', '', '', '1', 'CINCOTTA  A.', 'O', 'VULCAN UP 15 TN', '1?  PARTE: viga-cte cto -torre-contrapesos 2 ', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11280', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C259', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6069, 0, 0, 0, 0, 'YPF ', '', '6069', '', 'FABRICAR', '', '', '2', 'RAMIREZ  A.', 'Av.15311881', 'LUFKIN   228', 'PERNO PIE  DE  BIELA', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11218', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C260', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6070, 0, 0, 0, 0, 'YPF ', '', '6070', '', 'FABRICAR', '', '', '2', 'RAMIREZ  A.', 'Av.15311891', 'LUFKIN  320', 'PERNO PIE  DE  BIELA', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11219', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C261', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6071, 0, 0, 0, 0, 'YPF ', '', '6071', '', 'FABRICAR', '', '', '2', 'RAMIREZ  A.', 'Av.15311928', 'LUFKIN  456', 'PERNO PIE  DE  BIELA', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11220', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C431', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6072, 0, 0, 0, 0, 'YPF', '', '6072', '', 'FABRICAR', '', '', '2', 'RAMIREZ  A.', 'Av.15311931', 'LUFKIN 640', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11091', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C432', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6073, 0, 0, 0, 0, 'YPF', '', '6073', '', 'FABRICAR', '', '', '2', 'RAMIREZ  A.', 'Av.15311925', 'LUFKIN 912', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C264', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6074, 0, 0, 0, 0, 'YPF', '', '6074', '', 'REPARAR', '', '', '2', 'HERNANDEZ  D.', 'PT1077 28T', 'LUFKIN  320', 'CINTA  DE FRENO ', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '635', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C265', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6075, 0, 0, 0, 0, 'YPF ', '', '6075', '', 'REPARAR', '', '', '1', 'BORIS ', '', 'VULCAN M  456', 'REDUCTOR :PERDIDA  ACEITE  ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '639', 'Oct-16');
INSERT INTO `servicioscli` (`apartado`, `prioridad`, `cifnif`, `ciudad`, `codagente`, `codalmacen`, `codcliente`, `coddir`, `coddivisa`, `codejercicio`, `codigo`, `codpago`, `codpais`, `codpostal`, `codserie`, `direccion`, `editable`, `garantia`, `fecha`, `hora`, `femail`, `fechafin`, `horafin`, `fechainicio`, `horainicio`, `idservicio`, `idalbaran`, `idprovincia`, `irpf`, `neto`, `nombrecliente`, `numero`, `numero2`, `observaciones`, `descripcion`, `solucion`, `usuarioep`, `idmep`, `respep`, `yacep`, `material`, `material_estado`, `accesorios`, `porcomision`, `provincia`, `recfinanciero`, `tasaconv`, `total`, `totaleuros`, `totalirpf`, `totaliva`, `totalrecargo`, `idestado`, `numsalida`, `numcertificado`) VALUES
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C266', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6076, 0, 0, 0, 0, 'YPF', '', '6076', '', 'REPARAR', '', '', '1', 'BORIS ', '', 'LUFKIN  M 640', 'REDUCTOR    ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11315', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C267', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6077, 0, 0, 0, 0, 'YPF', '', '6077', '', 'FABRICAR', '', '', '12', 'AYBAR  M.', '', 'UNIVERSAL', 'BANDEJAS   SEG?N PLANO', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '642', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C268', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6078, 0, 0, 0, 0, 'YPF', '', '6078', '', 'MECANIZAR', '', '', '2', 'AYBAR  M.', '', 'UNIVERSAL', 'CONO   D:  44  10 .  6 ', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C269', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6079, 0, 0, 0, 0, 'YPF', '', '6079', '', 'REPARAR', '', '', '1', 'AYBAR  M.', '', 'UNIVERSAL', 'BOMBA   CENTRIFUGA', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '846', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C270', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6080, 0, 0, 0, 0, 'YPF', '', '6080', '', 'MECANIZAR', '', '', '1', 'JUAN FERGUSON ', '', 'UNIVERSAL', 'MECANIZAR DIAM INT CONO  Y AJUSTAR CHAVETA', 'PL PT ', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '633', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C271', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6081, 0, 0, 0, 0, 'YPF', '', '6081', '', 'MECANIZAR', '', '', '1', 'JUAN FERGUSON ', '', 'UNIVERSAL', 'CONO . MECANIZAR  DIAM INT.', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '633', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C272', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6082, 0, 0, 0, 0, 'YPF ', '', '6082', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', '', 'MTD  9 TN', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11139', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C273', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6083, 0, 0, 0, 0, 'YPF', '', '6083', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', 'EG.750', 'LUFKIN   228', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11140', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C274', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6084, 0, 0, 0, 0, 'YPF ', '', '6084', '', 'FABRICAR', '', '', '1', 'QUINTEROS P.', 'O', 'DARCO 320', 'PERNO PIE  DE  BIELA', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11217', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C275', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6085, 0, 0, 0, 0, 'YPF ', '', '6085', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', '', 'DARCO 640', 'JUEGO DE PATINES', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11141', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C276', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6086, 0, 0, 0, 0, 'YPF ', '', '6086', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', '', 'SIAM C-228', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11142', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C277', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6087, 0, 0, 0, 0, 'YPF', '', '6087', '', 'REPARAR', '', 'P', '1', 'QUINTEROS P.', '', 'LUFKIN  456', 'COJINETE DE CENTRO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11221', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C278', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6088, 0, 0, 0, 0, 'YPF', '', '6088', '', 'FABRICAR', '', '', '1', 'QUINTEROS P.', '', 'VULCAN UP 15 TN', 'PERNO PIE  DE  BIELA', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11175', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C279', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6089, 0, 0, 0, 0, 'YPF', '', '6089', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', '', 'DARCO 228Y', 'JUEGO DE PATINES', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11143', 'Jun-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C280', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6090, 0, 0, 0, 0, 'YPF', '', '6090', '', 'FABRICAR', '', '', '1', 'QUINTEROS P.', '', 'UNIVERSAL', 'CABALLETES  P. PPB  GRANDES', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C281', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6091, 0, 0, 0, 0, 'YPF', '', '6091', '', 'FABRICAR', '', '', '1', 'QUINTEROS P.', '', 'UNIVERSAL', 'CABALLETES  P. PPB CHICOS', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C282', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6092, 0, 0, 0, 0, 'YPF', '', '6092', '', 'FABRICAR', '', '', '1', 'AYBAR  M.', 'O', 'UNIVERSAL', 'CONO   SEG?N  POLEA', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '846', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C283', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6093, 0, 0, 0, 0, 'SNP', '', '6093', '', 'REPARAR ', '', '', '1', 'LOPEZ  J.', 'O', 'CKH 10Tn', 'REDUCTOR', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C284', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6094, 0, 0, 0, 0, 'PAE ', '', '6094', '', 'FABRICAR', '', '', '2', 'LOPEZ  J.', '0', 'UNIVERSAL', 'TAPAS ', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C285', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6095, 0, 0, 0, 0, 'PAE', '', '6095', '', 'FABRICAR', '', '', '5', 'VEGA J.', '0', 'WSHA', 'TAPAS  PARA  MOTOR', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '842/638', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C286', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6096, 0, 0, 0, 0, 'YPF ', '', '6096', '', 'FABRICAR', '23222 (2)', '', '4', 'LISANDRO', 'CE.970-1319-1321', 'LUFKIN  640', 'PERNO PIE  DE  BIELA', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11161/11159', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C287', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6097, 0, 0, 0, 0, 'YPF', '', '6097', '', 'REPARAR', '', '', '1', 'CINCOTTA  A.', '', 'SIAM M 320', 'AIB  COMPLETO', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11261', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C288', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6098, 0, 0, 0, 0, 'YPF ', '', '6098', '', 'REPARAR', '', '', '1', 'JEREZ  D.', '', 'LUFKIN 456/640', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11144', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C289', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6099, 0, 0, 0, 0, 'YPF', '', '6099', '', 'REPARAR', '', '', '1', 'JEREZ  D.', '', 'SIAM C-228', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11144', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C290', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6100, 0, 0, 0, 0, 'YPF ', '', '6100', '', 'REPARAR', '', '', '1', 'JEREZ  D.', '', 'LUFKIN  456', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11177', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C291', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6101, 0, 0, 0, 0, 'YPF', '', '6101', '', 'REPARAR', '', '', '1', 'JEREZ  D.', '', 'PUMP JACK  912', 'JUEGO DE PATINES', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11178', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C292', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6102, 0, 0, 0, 0, 'YPF', '', '6102', '', 'FABRICAR', '', '', '1', 'JEREZ  D.', '', 'LIUFKIN  640', 'PERNO PIE  DE  BIELA', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11176', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C293', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6103, 0, 0, 0, 0, 'YPF ', '', '6103', '', 'FABRICAR', '', 'P', '1', 'JEREZ  D.', 'CV.74', 'DARCO 320', 'PPB', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C294', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6104, 0, 0, 0, 0, 'YPF', '', '6104', '', 'FABRICAR', '', 'P', '1', 'JEREZ  D.', '', 'VULCAN 303', 'PPB', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C295', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6105, 0, 0, 0, 0, 'YPF', '', '6105', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', 'EG. 515', 'LUFKIN   228', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11162', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C296', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6106, 0, 0, 0, 0, 'YPF', '', '6106', '', 'REPARAR', '', '', '2', 'QUINTEROS P.', 'EG. 237 LC.788', 'SIAM C-228', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11163', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C297', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6107, 0, 0, 0, 0, 'PAE', '', '6107', '', 'FABRICAR', '', '', '3', 'VEGA J.', '0', 'WSHA', 'SEPARADORES  S. MUESTRA', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '640', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C298', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6108, 0, 0, 0, 0, 'YPF ', '', '6108', '', 'ROSCADO DIAM INT.', '', '', '2', 'SICTEL ', '', 'UNIVERSAL', 'BRIDAS  ', 'TTE', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '843', 'falta certificar'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C299', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6109, 0, 0, 0, 0, 'YPF', '', '6109', '', 'FABRICAR', '', '', '1', 'REYES  P.', 'EC.1504', 'SIAM C-228', 'PERNO PIE  DE  BIELA', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '641', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C300', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6110, 0, 0, 0, 0, 'YPF', '', '6110', '', 'FABRICAR', '', '', '2', 'QUINTEROS P.', 'EG.1504', 'DARCO 320', 'PERNO PIE  DE  BIELA', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11228', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C406', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6111, 0, 0, 0, 0, 'YPF ', '', '6111', '', 'REPARACI?N INTEGRAL', '', 'P', '1', 'CINCOTTA  A.', '', 'CKH 10Tn', 'AIB  COMPLETO', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11233', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C369', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6112, 0, 0, 0, 0, 'YPF ', '', '6112', '', 'REPARACI?N INTEGRAL', '', 'P', '1', 'HERNANDEZ  D.', 'PT.1014', 'SEM  C-228', 'AIB  COMPLETO', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '4', NULL, 'ARS', '2016', 'SER2016C303', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6113, 0, 0, 0, 0, 'GOLCORP', '', '6113', '', 'MECANIZADO', '', '', '1', 'FERNANDEZ  .', '', 'SWING  LEVER', 'REPARACI?N  DE UN EXTREMO', 'C?N', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11225', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C472', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6114, 0, 0, 0, 0, 'YPF ', '', '6114', '', 'FABRICAR', '22313 (2)', 'P', '2', 'LISANDRO', 'CE.259', 'MTD  9 TN', 'PPB', 'CE', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11232', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C468', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6115, 0, 0, 0, 0, 'YPF', '', '6115', '', 'FABRICAR', '22317 (2)', 'P', '2', 'LISANDRO', 'CE.1124', 'LUFKIN   320', 'PPB', 'CE', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11296', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C477', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6116, 0, 0, 0, 0, 'YPF', '', '6116', '', 'FABRICAR', '22318 (2)', 'P', '2', 'LISANDRO', 'CE.1248', 'SIAM M 320', 'PPB', 'CE', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11297', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C488', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6117, 0, 0, 0, 0, 'YPF', '', '6117', '', 'FABRICAR', '22320 (2)', 'P', '2', 'LISANDRO', 'CE. 259', 'VULCAN  320', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 'POZO', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C308', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6118, 0, 0, 0, 0, 'YPF', '', '6118', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.89', 'VULCAN 303', 'ARTICULACI?N  DE  COLA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11191', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C309', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6119, 0, 0, 0, 0, 'YPF', '', '6119', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'LP.1186', 'VULCAN 303', 'ARTICULACI?N  DE  COLA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11190', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C310', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6120, 0, 0, 0, 0, 'YPF', '', '6120', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'LM.47', 'VULCAN 303', 'ARTICULACI?N  DE  COLA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11189', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C311', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6121, 0, 0, 0, 0, 'YPF', '', '6121', '', 'REPARACI?N  ', '', '', '1', 'URZAGASTI S.', 'LM.47', 'VULCAN 303', 'COJINETE  DE COLA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11188', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C312', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6122, 0, 0, 0, 0, 'YPF', '', '6122', '', 'REPARACI?N  ', '', '', '1', 'URZAGASTI S.', 'LM.47', 'VULCAN 303', 'JUEGO DE PATINES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11187', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C313', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6123, 0, 0, 0, 0, 'YPF', '', '6123', '', 'REPARACI?N  ', '', '', '1', 'URZAGASTI S.', 'LM.152', 'VULCAN 456-640', 'COJINETE DE CENTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11186', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C314', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6124, 0, 0, 0, 0, 'YPF', '', '6124', '', 'REPARACI?N  ', '', '', '1', 'URZAGASTI S.', 'LP.98', 'DARCO 320', 'COJINETE DE CENTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11185', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C315', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6125, 0, 0, 0, 0, 'YPF', '', '6125', '', 'REPARACI?N  ', '', 'P', '1', 'URZAGASTI S.', 'O', 'LUFKIN  456', 'COJINETE DE CENTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11184', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C316', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6126, 0, 0, 0, 0, 'YPF', '', '6126', '', 'FABRICAR', '', 'P', '2', 'URZAGASTI S.', 'LP.224  ', 'DARCO 320', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11183', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C317', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6127, 0, 0, 0, 0, 'YPF', '', '6127', '', 'FABRICAR', '', 'P', '1', 'URZAGASTI S.', 'LP.144', 'DARCO 320', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11182', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C318', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6128, 0, 0, 0, 0, 'YPF', '', '6128', '', 'FABRICAR', '', 'P', '1', 'URZAGASTI S.', 'LP.2040', 'DARCO 320', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11181', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C319', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6129, 0, 0, 0, 0, 'YPF', '', '6129', '', 'FABRICAR', '', '', '1', 'URZAGASTI S.', 'LP.241', 'SIAM  M 320', 'PERNO PIE  DE  BIELA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11180', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C320', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6130, 0, 0, 0, 0, 'YPF', '', '6130', '', 'FABRICAR', '', 'P', '2', 'URZAGASTI S.', 'LP.2455', 'MTD  9 TN', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11179', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C321', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6131, 0, 0, 0, 0, 'YPF', '', '6131', '', 'REPARACI?N ', '', '', '1', 'URZAGASTI S.', 'LP.1303', 'DARCO 228Y', 'JUEGO DE PATINES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11174', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C322', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6132, 0, 0, 0, 0, 'YPF', '', '6132', '', 'REPARACI?N', '', '', '1', 'URZAGASTI S.', 'LP.2749', 'DARCO 320', 'JUEGO DE PATINES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11173', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C323', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6133, 0, 0, 0, 0, 'YPF', '', '6133', '', 'REPARACI?N', '', '', '1', 'URZAGASTI S.', 'LP. 167', 'DARCO 640', 'JUEGO DE PATINES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11172', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C324', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6134, 0, 0, 0, 0, 'YPF', '', '6134', '', 'REPARACI?N ', '', '', '1', 'URZAGASTI S.', 'LP.1388', 'SIAM C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11165', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C325', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6135, 0, 0, 0, 0, 'YPF', '', '6135', '', 'REPARACI?N ', '', '', '1', 'URZAGASTI S.', 'LP.2379', 'SIAM C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11166', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C326', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6136, 0, 0, 0, 0, 'YPF', '', '6136', '', 'REPARACI?N ', '', '', '1', 'URZAGASTI S.', 'LP.1629', 'SBS C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11167', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C327', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6137, 0, 0, 0, 0, 'YPF', '', '6137', '', 'REPARACI?N ', '', '', '1', 'URZAGASTI S.', 'LP.1896', 'MTD  9 TN', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11168', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C328', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6138, 0, 0, 0, 0, 'YPF', '', '6138', '', 'REPARACI?N ', '', '', '1', 'URZAGASTI S.', 'LP.170', 'LUFKIN 456/640', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11169', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C329', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6139, 0, 0, 0, 0, 'YPF', '', '6139', '', 'REPARACI?N ', '', '', '1', 'URZAGASTI S.', '', 'LUFKIN 456/640', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11170', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C330', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6140, 0, 0, 0, 0, 'YPF', '', '6140', '', 'REPARACI?N ', '', '', '1', 'URZAGASTI S.', 'LP.2174', 'SIAM C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11171', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C331', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6141, 0, 0, 0, 0, 'YPF', '', '6141', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.270', 'LUFKIN   228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11171', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C332', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6142, 0, 0, 0, 0, 'YPF', '', '6142', '', 'REPARACI?N ', '', 'P', '1', 'URZAGASTI S.', 'LP.1100', 'MTD  9 TN', 'COJINETE DE CENTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C407', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6143, 0, 0, 0, 0, 'YPF', '', '6143', '', 'REPARACI?N INTEGRAL', '', '', '1', 'CINCOTTA  A.', '', 'CKH 10Tn', 'AIB  COMPLETO', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11285', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C380', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6144, 0, 0, 0, 0, 'YPF ', '', '6144', '', 'REPARAR', '', '', '1', 'REYES  P.', 'PT.1032', 'SIAM C-228', 'ARTICULACI?N  DE  COLA', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C335', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6145, 0, 0, 0, 0, 'PAE', '', '6145', '', 'FABRICAR', '', '', '2', 'GERK G.', '0', 'UNIVERSAL', 'EJE  TOMA DE FUERZA', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C336', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6146, 0, 0, 0, 0, 'YPF ', '', '6146', '', '', '', '', '8', 'AYBAR  M.', '0', 'BULONES  1 1/2" x 4 1/2"', 'proveer', 'PL PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '847', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C337', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6147, 0, 0, 0, 0, 'PAE', '', '6147', '', '', '', '', '10', 'VEGA J.', '0', 'BULONES  3/4 x 1 "', 'PROVEER', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '845', ''),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C338', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6148, 0, 0, 0, 0, 'SNP ', '', '6148', '', 'REPARAR', '', '', '1', 'CASTILLO  O.', '', 'SBS C-228', 'COJINETE DE CENTRO', 'LH3', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C339', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6149, 0, 0, 0, 0, 'YPF', '', '6149', '', 'FABRICAR', '', '', '1', 'HERNANDEZ  D.', 'EC.1504', 'SIAM C-228', 'PERNO PIE  DE  BIELA', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '837', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C340', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6150, 0, 0, 0, 0, 'YPF', '', '6150', '', 'FABRICAR', '', '', '20', 'URZAGASTI S.', 'O', 'UNIVERSAL', 'ANCLAJES  DE  0.80  / 0.60  mts.', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11197', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C341', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6151, 0, 0, 0, 0, 'YPF', '', '6151', '', 'FABRICAR', '', 'Romero J.', '1', 'VEGA F.', '15463862', 'PUMP JACK  640', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11230', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C342', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6152, 0, 0, 0, 0, 'YPF', '', '6152', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', 'EG.696', 'SIAM C-228', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11201', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C343', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6153, 0, 0, 0, 0, 'YPF', '', '6153', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', '', 'SIAM C-228', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11202', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C344', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6154, 0, 0, 0, 0, 'YPF', '', '6154', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', '', 'SIAM  10  TN', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11203', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C345', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6155, 0, 0, 0, 0, 'YPF', '', '6155', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', '', 'LUFKIN   228', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11204', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C346', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6156, 0, 0, 0, 0, 'YPF', '', '6156', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', 'LC.848', 'LUFKIN 114', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11205', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C347', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6157, 0, 0, 0, 0, 'YPF', '', '6157', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', 'EG.287', 'IDECO  114', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11122', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C348', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6158, 0, 0, 0, 0, 'YPF ', '', '6158', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', 'EG.765', 'DARCO 228Y', 'JUEGO DE PATINES', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11206', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C349', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6159, 0, 0, 0, 0, 'YPF', '', '6159', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', 'LC. 900', 'DARCO 320', 'JUEGO DE PATINES', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11207', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C350', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6160, 0, 0, 0, 0, 'YPF ', '', '6160', '', 'REPARAR', '', 'P', '1', 'QUINTEROS P.', 'O', 'SIAM C-228', 'PALANCAS  DE   FRENO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11208', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C351', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6161, 0, 0, 0, 0, 'YPF', '', '6161', '', 'REPARAR', '', 'P', '1', 'QUINTEROS P.', '', 'SIAM C-228', 'PALANCAS  DE   FRENO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11209', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C352', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6162, 0, 0, 0, 0, 'YPF', '', '6162', '', 'REPARAR', '', 'P', '1', 'QUINTEROS P.', 'O', 'VULCAN 303', 'PALANCA  DE FRENO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11210', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C353', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6163, 0, 0, 0, 0, 'YPF', '', '6163', '', 'REPARAR', '', 'P', '1', 'QUINTEROS P.', 'O', 'WUELFEL 228', 'JUEGO DE PATINES', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11211', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C354', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6164, 0, 0, 0, 0, 'YPF', '', '6164', '', 'FABRICAR', '', '', '1', 'QUINTEROS P.', 'O', 'DARCO 320', 'PERNO PIE  DE  BIELA', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11212', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C355', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6165, 0, 0, 0, 0, 'YPF', '', '6165', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.954', 'SBS C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11213', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C356', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6166, 0, 0, 0, 0, 'YPF', '', '6166', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.253', 'SIAM C-228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11214', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C357', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6167, 0, 0, 0, 0, 'YPF', '', '6167', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP.1348', 'MTD  9 TN', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11215', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C358', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6168, 0, 0, 0, 0, 'YPF', '', '6168', '', 'FABRICAR', '', 'P', '1', 'URZAGASTI S.', 'LP.1875', 'MTD  9 TN', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11216', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C359', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6169, 0, 0, 0, 0, 'YPF', '', '6169', '', 'FABRICAR', '', '', '2', 'ROMERO J.', '', 'PIGNONE 9Tn', 'PERNO PIE  DE  BIELA', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11223', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C360', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6170, 0, 0, 0, 0, 'YPF', '', '6170', '', 'FABRICAR', '', '', '2', 'ROMERO J.', '', 'SIAM C-228', 'PERNO PIE  DE  BIELA', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11224', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C361', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6171, 0, 0, 0, 0, 'YPF', '', '6171', '', 'CONSTRUCCI?N', '', '', '1', 'BORIS ', '', '', 'CHASIS', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11199', 'Jul-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C362', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6172, 0, 0, 0, 0, 'PAE', '', '6172', '', 'MODIFICACI?N ', '', '', '2', 'VEGA J.', '0', 'WSHA', 'FILTROS DE AIRE ', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '849', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C363', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6173, 0, 0, 0, 0, 'YPF ', '', '6173', '', 'REPARAR', '', '', '1', 'CINCOTTA  A.', '', 'CKH 10Tn', 'AIB  COMPLETO', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11328', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C364', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6174, 0, 0, 0, 0, 'YPF', '', '6174', '', 'FABRICAR', '', '', '4', 'URZAGASTI S.', '', 'UNIVERSAL', 'ANCLAJES  DE  0.80 mts.', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11200', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C365', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6175, 0, 0, 0, 0, 'YPF', '', '6175', '', 'REPARAR', '', '', '1', 'SANCHEZ', '', 'PUMP JUCK  320', 'AIB  COMPLETO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C85', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6176, 0, 0, 0, 0, 'YPF ', '', '6176', '', 'PIN+REP.CONO+ARANDELA+CHAVETA', '', 'P', '1', 'COLCHI  P.', 'C?E-1270', 'VULCAN 320', 'CAMPANA DE FRENO + CHAVETA', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11289', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C367', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6177, 0, 0, 0, 0, 'PAE', '', '6177', '', 'REPARACI?N Y COLOCAR INCERTO', '', '', '1', 'GERK G.', '0', '', 'TAPA VALVULA', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C368', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6178, 0, 0, 0, 0, 'YPF ', '', '6178', '', 'PROVISI?N+ANGULOS', '', '', '1', 'AYBAR  M.', '', '', 'FILTRO MALLA', 'PLANTA PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C142', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6179, 0, 0, 0, 0, 'YPF ', '', '6179', '', 'REPARAR', '', 'Reyes', '1', 'REYES  P.', '', 'DARCO 456', 'JUEGO DE PATINES', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C370', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6180, 0, 0, 0, 0, 'YPF', '', '6180', '', 'MECANIZADO DIAM. INT.', '', '', '1', 'AYBAR  M.', '', '', 'BOLUTA', 'PLANTA PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C371', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6181, 0, 0, 0, 0, 'YPF', '', '6181', '', 'FABRICAR', '', '', '1', 'BORIS-QUINTEROS', '', 'VULCAN UP 15 TN', 'PPB', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C372', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6182, 0, 0, 0, 0, 'YPF', '', '6182', '', 'FABRICAR', '', '', '2', 'ROMERO J.', '', 'WUELFEL 320', 'PERNO PIE  DE  BIELA', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11226', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C373', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6183, 0, 0, 0, 0, 'PAE', '', '6183', '', 'CONSTRUCCI?N S/PLANO', '', '', '10', 'VEGA J.', '0', '', 'BRIDAS SOPORTE', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C374', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6184, 0, 0, 0, 0, 'PAE', '', '6184', '', 'CONSTRUCCI?N 8MM S/PLANO', '', '', '28', 'VEGA J.', '0', '', 'CHAPAS', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C375', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6185, 0, 0, 0, 0, 'SNP', '', '6185', '', 'REPARAR', '', '', '1', 'MALLEA E.', '', 'LUFKIN 912', 'POSTE MAESTRO +TORRE', 'CM', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11272', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C376', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6186, 0, 0, 0, 0, 'PAE', '', '6186', '', 'FABRICAR MANCHON Y MECANIZADO DE 1/2 ACOPLE', '', '', '2', 'VEGA J.', '0', '', 'GUMMY', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C377', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6187, 0, 0, 0, 0, 'PAE', '', '6187', '', 'FABRICACI?N S/MIESTRA', '', '', '2', 'GERCK G.', '0', '', 'JUEGO PIEZAS DE BBA', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C206', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6188, 0, 0, 0, 0, 'YPF', '', '6188', '', 'REPARAR', '', 'Hernandez', '1', 'REYES  P.', 'PT-146/5T', 'SIAM C-228', 'CINTA  DE FRENO ', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C460', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6189, 0, 0, 0, 0, 'YPF', '', '6189', '', 'REPARACI?N', '', '', '1', 'REYES  P.', '', '7CK8', 'JUEGO DE PATINES', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C518', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6190, 0, 0, 0, 0, 'YPF', '', '6190', '', 'ALARGAR EL DOBLE LA ROSCA', '', '', '3', 'REYES  P.', '', 'SIAM C-228', 'VARILLAS VERTICALES', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C381', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6191, 0, 0, 0, 0, 'YPF', '', '6191', '', 'PROVISI?N', '', '', '100', 'URZAGASTI S.', '', '', 'BULONES 3/4 x 3"', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11227', 'Aug-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C382', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6192, 0, 0, 0, 0, 'YPF', '', '6192', '', 'PROVISI?N', '', '', '8', 'QUINTEROS P.', '', '', 'BULONES 1 x 4" ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11228', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C383', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6193, 0, 0, 0, 0, 'YPF', '', '6193', '', 'FABRICAR', '', 'P', '1', 'LEIVA O.', '', 'LUFKIN 912', 'CHAVETA POLEA MANDO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 'S/N? remito', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C234', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6194, 0, 0, 0, 0, 'YPF', '', '6194', '', 'REPARAR', '', 'Hernandez', '1', 'REYES  P.', '', 'DARCO 228Y', 'JUEGO DE PATINES', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C385', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6195, 0, 0, 0, 0, 'YPF', '', '6195', '', 'REPARAR', '', '', '1', 'JEREZ  D.', '', 'LUFKIN 456/640', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11268', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C386', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6196, 0, 0, 0, 0, 'YPF', '', '6196', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP-176', 'LUFKIN 456', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11234', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C387', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6197, 0, 0, 0, 0, 'YPF', '', '6197', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP-479', 'LUFKIN 456', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11235', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C388', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6198, 0, 0, 0, 0, 'YPF', '', '6198', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP-551', 'SIAM 228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11236', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C389', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6199, 0, 0, 0, 0, 'YPF', '', '6199', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP-2274', 'SIAM 228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11237', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C390', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6200, 0, 0, 0, 0, 'YPF', '', '6200', '', 'FABRICAR', '', 'P', '1', 'URZAGASTI S.', 'LP-22526', 'DARCO 320', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11238', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C391', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6201, 0, 0, 0, 0, 'YPF', '', '6201', '', 'FABRICAR', '', '', '1', 'URZAGASTI S.', 'Recinto', 'SIAM 10', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11239', 'P/REZAGO'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C392', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6202, 0, 0, 0, 0, 'YPF', '', '6202', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP-303', 'VULCAN 320', 'JUEGO DE PATINES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11240', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C393', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6203, 0, 0, 0, 0, 'YPF', '', '6203', '', 'REPARAR', '22317 (1)', 'P', '1', 'URZAGASTI S.', 'LP-1823', 'SIAM 228', 'ARTICULACI?N', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11241', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C394', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6204, 0, 0, 0, 0, 'YPF', '', '6204', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'LP-2266', 'SIAM 228', 'ARTICULACI?N', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11242', ''),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C395', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6205, 0, 0, 0, 0, 'SNP', '', '6205', '', 'REPARAR', '', '', '4', 'DOMINGUEZ J.', '', '', 'BRIDA', 'EH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C396', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6206, 0, 0, 0, 0, 'YPF', '', '6206', '', 'REPARAR ROSCA', '', 'P', '2', 'JEREZ D.', 'ECH-70', 'DARCO 320', 'VARILLA DE FRENO', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '5', NULL, 'ARS', '2016', 'SER2016C397', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6207, 0, 0, 0, 0, 'JOMAR', '', '6207', '', 'REPARAR', '', '', '3', 'ENRIQUE-BARBOZA', '', '', 'CA?OS 4"', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 'OK'),
('', 0, '', '', '1', 'ALG', '4', NULL, 'ARS', '2016', 'SER2016C398', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6208, 0, 0, 0, 0, 'GOLCORP', '', '6208', '', 'REPARAR', '', '', '1', 'VEGA-FERNANDEZ', '', '', 'SWING LEVER', 'PM', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '');
INSERT INTO `servicioscli` (`apartado`, `prioridad`, `cifnif`, `ciudad`, `codagente`, `codalmacen`, `codcliente`, `coddir`, `coddivisa`, `codejercicio`, `codigo`, `codpago`, `codpais`, `codpostal`, `codserie`, `direccion`, `editable`, `garantia`, `fecha`, `hora`, `femail`, `fechafin`, `horafin`, `fechainicio`, `horainicio`, `idservicio`, `idalbaran`, `idprovincia`, `irpf`, `neto`, `nombrecliente`, `numero`, `numero2`, `observaciones`, `descripcion`, `solucion`, `usuarioep`, `idmep`, `respep`, `yacep`, `material`, `material_estado`, `accesorios`, `porcomision`, `provincia`, `recfinanciero`, `tasaconv`, `total`, `totaleuros`, `totalirpf`, `totaliva`, `totalrecargo`, `idestado`, `numsalida`, `numcertificado`) VALUES
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C399', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6209, 0, 0, 0, 0, 'YPF', '', '6209', '', 'FABRICAR', '', 'Romero J.', '2', 'RETAMOSO', 'C/ROD 22317', 'PIGNONE 9Tn', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11231', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C400', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6210, 0, 0, 0, 0, 'SNP', '', '6210', '', 'REPARACI?N', '', '', '1', 'JOSE LUIS LOPEZ', '', 'WUELFEL 320', 'COJINETE DE CENTRO', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C401', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6211, 0, 0, 0, 0, 'YPF ', '', '6211', '', 'REPARACI?N', '', '', '1', 'PABLO REYES', '', 'SIAM M-456', 'CAJA REDUCTORA C/2 MANIVELAS + 2 PPB', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C486', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6212, 0, 0, 0, 0, 'YPF ', '', '6212', '', 'FABRICAR', '', '', '1', 'HERNANDEZ  D.', 'EC-463 (198)', 'LUFKIN M-456', 'PPB', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C402', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6213, 0, 0, 0, 0, 'YPF ', '', '6213', '', 'MEC. CAMPANA +REPARACION PATINES', '', '', '1', 'HERNANDEZ  D.', 'EC-472 (198)', 'WUELFEL 320', 'CAMPANA DE FRENO + PATINES DE FRENO', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C404', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6214, 0, 0, 0, 0, 'YPF ', '', '6214', '', 'FABRICAR ', '', 'P', '2', 'QUINTEROS P.', '', 'WUELFEL 228', 'PPB', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C305', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6215, 0, 0, 0, 0, 'YPF ', '', '6215', '', 'FABRICAR', '23222 (1)', '', '1', 'LISANDRO', 'CE-1207', 'LUFKIN M-912', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11292', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C304', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6216, 0, 0, 0, 0, 'YPF ', '', '6216', '', 'REPARAR', '', '', '1', 'LISANDRO', 'CG-461', 'MTD  9 TN', 'COJINETE DE CENTRO', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C306', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6217, 0, 0, 0, 0, 'YPF ', '', '6217', '', 'FABRICAR', '23222 (1)', '', '1', 'LISANDRO', 'CE-430', 'LUFKIN M-912', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C301', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6218, 0, 0, 0, 0, 'YPF ', '', '6218', '', 'REPARAR', '', '', '1', 'LISANDRO', 'CE-329', 'LUFKIN 640', 'CINTA  DE FRENO ', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11286', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C409', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6219, 0, 0, 0, 0, 'YPF ', '', '6219', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP-2466', 'DARCO 320', 'ARTICULACI?N ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C410', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6220, 0, 0, 0, 0, 'YPF ', '', '6220', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP-2466', 'DARCO 320', 'COJINETE DE CENTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C411', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6221, 0, 0, 0, 0, 'YPF ', '', '6221', '', 'FABRICAR', '', '', '1', 'URZAGASTI S.', 'LP-176', 'LUFKIN 640', 'PERNO PIE  DE  BIELA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11251', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C412', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6222, 0, 0, 0, 0, 'YPF ', '', '6222', '', 'FABRICAR', '', 'P', '1', 'URZAGASTI S.', 'LM-571', 'SBS 228', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C413', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6223, 0, 0, 0, 0, 'YPF ', '', '6223', '', 'FABRICAR', '21315 (2)', '', '2', 'URZAGASTI S.', 'LM-425', 'SIAM C-228', 'PERNO PIE  DE  BIELA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11253', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C414', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6224, 0, 0, 0, 0, 'YPF ', '', '6224', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LM-702', 'LUFKIN   228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11254', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C415', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6225, 0, 0, 0, 0, 'YPF ', '', '6225', '', 'REPARAR', '', '', '3', 'URZAGASTI S.', 'LP-1769/LP-981/S/N?', 'MTD  9 TN', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11255', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C416', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6226, 0, 0, 0, 0, 'YPF ', '', '6226', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', 'LP-1169', 'DARCO 228Y', 'JUEGO DE PATINES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11256', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C417', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6227, 0, 0, 0, 0, 'YPF ', '', '6227', '', 'FABRICAR', '21315 (1)', 'P', '1', 'URZAGASTI S.', 'LM-612', 'SIAM C-228', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C418', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6228, 0, 0, 0, 0, 'YPF ', '', '6228', '', 'FABRICAR', '', 'P', '1', 'URZAGASTI S.', 'LP-310', 'MAG 228', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C419', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6229, 0, 0, 0, 0, 'YPF ', '', '6229', '', 'REPARAR', '21315 (2)', '', '1', 'URZAGASTI S.', 'LP-238', 'SIAM 228', 'EJE VELOZ C/POLEA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11259', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C420', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6230, 0, 0, 0, 0, 'YPF ', '', '6230', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'Recinto', 'VULCAN 456-640', 'ARTICULACI?N', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C421', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6231, 0, 0, 0, 0, 'YPF ', '', '6231', '', 'FABRICAR', '', 'P', '1', 'QUINTEROS P.', 'EG-766', 'SEM 228', 'PPB', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C422', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6232, 0, 0, 0, 0, 'YPF ', '', '6232', '', 'FABRICAR', '', 'P', '1', 'QUINTEROS P.', 'LC-800', 'VULCAN 303', 'PPB', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C423', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6233, 0, 0, 0, 0, 'YPF ', '', '6233', '', 'REPARAR', '', '', '1', 'QUINTEROS P.', 'EG-666', 'SIAM 228', 'CINTA  DE FRENO ', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11248', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C424', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6234, 0, 0, 0, 0, 'YPF ', '', '6234', '', 'FABRICAR', '', '', '20', 'URZAGASTI S.', '', 'UNVERSAL', 'ANCLAJES DE 80 CM', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11317', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C425', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6235, 0, 0, 0, 0, 'YPF ', '', '6235', '', 'FABRICAR', '', '', '12', 'URZAGASTI S.', '', 'CKH 10Tn', 'ANCLAJES P/AIB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11318', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C426', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6236, 0, 0, 0, 0, 'YPF ', '', '6236', '', 'FABRICAR', '', 'P', '20', 'URZAGASTI S.', '0', 'UNIVERSAL', 'TENSORES P/MOTOR', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C427', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6237, 0, 0, 0, 0, 'YPF ', '', '6237', '', 'FABRICAR', '', 'P', '5', 'URZAGASTI S.', '', 'SIAM 228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C428', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6238, 0, 0, 0, 0, 'YPF ', '', '6238', '', 'FABRICAR', '', 'P', '5', 'URZAGASTI S.', '', 'MTD  9 TN', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C429', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6239, 0, 0, 0, 0, 'PAE', '', '6239', '', 'CONST. SEG?N MUESTRA ', '', '', '2', 'JORGE V.', '0', '', '', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '4', NULL, 'ARS', '2016', 'SER2016C430', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6240, 0, 0, 0, 0, 'GOLCORP', '', '6240', '', 'REPARAR', '', '', '1', 'ASTIGUETA F.', '0', '', '', 'PM', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11279', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C433', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6241, 0, 0, 0, 0, 'YPF', '', '6241', '', 'FABRICAR', '22312 (2)', 'P', '2', 'ROMERO J.', '', 'WUELFEL 228', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C434', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6242, 0, 0, 0, 0, 'YPF', '', '6242', '', 'FABRICAR', '22317 (2)', 'P', '2', 'ROMERO J.', '', 'PIGNONE 9Tn', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C435', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6243, 0, 0, 0, 0, 'YPF', '', '6243', '', 'FABRICAR', '22322 (1)', 'P', '1', 'ROMERO J.', '', 'DARCO 320', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C436', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6244, 0, 0, 0, 0, 'YPF', '', '6244', '', 'FABRICAR', '22314 (1)', 'P', '1', 'ROMERO J.', '', 'SBS 320', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C437', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6245, 0, 0, 0, 0, 'YPF', '', '6245', '', 'FABRICAR', '21315 (1)', 'P', '1', 'ROMERO J.', '', 'SIAM 10Tn', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C438', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6246, 0, 0, 0, 0, 'YPF', '', '6246', '', 'FABRICAR', '22312 (2)', 'P', '2', 'ROMERO J.', '', 'WUELFEL 228', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C439', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6247, 0, 0, 0, 0, 'YPF', '', '6247', '', 'FABRICAR', '21315 (1)', 'P', '1', 'ROMERO J.', '', 'SEM 228', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C204', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6248, 0, 0, 0, 0, 'YPF', '', '6248', '', 'FABRICAR', '22316 (2)', '', '2', 'ROMERO J.', '', 'CKH 10Tn', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C440', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6249, 0, 0, 0, 0, 'YPF', '', '6249', '', 'FABRICAR', '23222 (2)', 'P', '1', 'ROMERO J.', '', 'LUFKIN 640', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C459', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6250, 0, 0, 0, 0, 'YPF', '', '6250', '', 'FABRICAR', '22317 (3)', 'P', '2', 'ROMERO J.', '', 'LUFKIN 320', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C145', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6251, 0, 0, 0, 0, 'YPF', '', '6251', '', 'PROLONGAR ROSCA Y CORTAR SEG?N INDICACIONES', '', 'Reyes', '4', 'HERNANDEZ  D.', '', 'LUFKIN  228', 'BULONES C/TUERCA P/CONTRAPESOS', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C442', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6252, 0, 0, 0, 0, 'YPF', '', '6252', '', 'REPARACI?N INTEGRAL', '', '', '1', 'JEREZ D.', '', 'SBS 320', 'AIB  COMPLETO', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C378', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6253, 0, 0, 0, 0, 'YPF', '', '6253', '', 'REPARACI?N INTEGRAL', '', '', '1', 'HERNANDEZ  D.', '', 'SEM 228', 'AIB PARA COMPLETAR EL AIB EP:6112', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C444', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6254, 0, 0, 0, 0, 'YPF', '', '6254', '', 'FABRICAR NUEVAS', '', 'P', '10', 'JEREZ D.', '', 'SIAM 228', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C445', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6255, 0, 0, 0, 0, 'YPF', '', '6255', '', 'FABRICAR NUEVAS (NORMAL)', '', 'P', '5', 'JEREZ D.', '', 'MTD  9 TN', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C446', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6256, 0, 0, 0, 0, 'YPF', '', '6256', '', 'FABRICAR NUEVAS (0,5cm mas corta)', '', 'P', '5', 'JEREZ D.', '', 'MTD  9 TN', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C447', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6257, 0, 0, 0, 0, 'YPF', '', '6257', '', 'FABRICAR NUEVAS (NORMAL)', '', 'P', '5', 'JEREZ D.', '', 'SBS 228', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C146', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6258, 0, 0, 0, 0, 'YPF', '', '6258', '', 'MECANIZADO A 4MM', '', 'Reyes', '1', 'REYES  P.', '', '', 'TAPON', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C449', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6259, 0, 0, 0, 0, 'YPF', '', '6259', '', 'REPARACI?N', '', 'P', '1', 'QUINTEROS P.', '', 'SIAM 228', 'ARTICULACI?N  DE  COLA', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C450', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6260, 0, 0, 0, 0, 'SNP', '', '6260', '', 'REPARACI?N', '', '', '1', 'CASTILLO  O.', '', 'SIAM 228', 'ARTICULACI?N  DE  COLA', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C451', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6261, 0, 0, 0, 0, 'SNP', '', '6261', '', 'REPARACI?N', '', '', '1', 'CASTILLO  O.', '', 'SIAM 228', 'ARTICULACI?N  DE  COLA', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C452', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6262, 0, 0, 0, 0, 'PAE', '', '6262', '', 'FABRICACI?N S/MUESTRA', '', '', '1', 'GERK G.', '0', '', 'SOPORTE SUPLEMENTO SEGURO + PORTA HERRAMIENTA', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '654', ''),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C453', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6263, 0, 0, 0, 0, 'SNP', '', '6263', '', 'REPARACI?N  RELLENO SOLDADURA + CONST. TUERCA', '', '', '1', 'CASTILLO  O.', '', 'MOYNO', 'EJE PPAL', 'EH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C454', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6264, 0, 0, 0, 0, 'YPF', '', '6264', '', 'MECANIZADO + CONST. DE CUPLA', '', '', '24', 'GATTI', '', '', 'TESTIGOS DE ALUMINIO', 'PTA PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C455', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6265, 0, 0, 0, 0, 'YPF', '', '6265', '', 'FABRICACI?N', '', '', '1', 'FLORES O.', '', '', 'RED CONTRA INCENDIO', 'PTA LH3', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 'FLORES', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C456', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6266, 0, 0, 0, 0, 'SNP', '', '6266', '', 'PROVISI?N P/ARMAR ARTICULACI?N EP: 6259/6260', '', '', '4', 'CASTILLO  O.', '', 'SIAM 228', 'BIELAS +CTE CLA', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C457', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6267, 0, 0, 0, 0, 'YPF', '', '6267', '', 'REPARACI?N', '', '', '1', 'PABLO REYES', '', 'VULCAN 303', '', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C458', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6268, 0, 0, 0, 0, 'YPF', '', '6268', '', 'PROVISI?N ENVIADO POR SPORTMAN', '', '', '16', 'URZAGASTI S.', '', '', '', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11263', 'Sep-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C251', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6269, 0, 0, 0, 0, 'YPF', '', '6269', '', 'FABRICAR', '22315 (2)', '', '2', 'ROMERO J.', '', 'VULCAN 303', 'PPB', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C379', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6270, 0, 0, 0, 0, 'YPF', '', '6270', '', 'REPARACI?N', '', '', '1', 'GUZMAN  E.', 'PT 2114/BAT 193', 'VULCAN 303', 'AIB  COMPLETO', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C461', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6271, 0, 0, 0, 0, 'YPF', '', '6271', '', 'REPARACI?N', '', 'CRUZ', '1', 'JEREZ D.', '', 'MTD  9 TN', 'CINTA  DE FRENO ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11276', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C462', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6272, 0, 0, 0, 0, 'YPF', '', '6272', '', 'REPARACION', '', '', '1', 'JEREZ D.', '', 'STORCK 50/80', 'BOMBA', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C463', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6273, 0, 0, 0, 0, 'YPF', '', '6273', '', 'REPARACI?N', '', 'P', '1', 'JEREZ D.', 'Aviso: 15547248', 'DARCO 320 PUMP JACK', 'ARTICULACI?N  DE  COLA', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C475', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6274, 0, 0, 0, 0, 'YPF', '', '6274', '', 'FABRICACI?N', '22318 (2)', 'P', '2', 'LERDA C.', 'LH-1234', 'SIAM C III', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11294', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C470', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6275, 0, 0, 0, 0, 'YPF', '', '6275', '', 'FABRICACI?N', '23222 (2)', 'P', '2', 'LERDA C.', 'LH-913', 'LUFKIN 456/640', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11293', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C473', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6276, 0, 0, 0, 0, 'YPF', '', '6276', '', 'FABRICACI?N', '21315 (1)', 'P', '1', 'LERDA C.', 'LH-1112', 'SIAM 7Tn', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '1288', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C469', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6277, 0, 0, 0, 0, 'YPF', '', '6277', '', 'FABRICACI?N', '22317 (2)', 'P', '2', 'LERDA C.', 'LH-1175', 'LUFKIN 228/320', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C333', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6278, 0, 0, 0, 0, 'YPF', '', '6278', '', 'FABRICACI?N', '22316 (2)', '', '2', 'LERDA C.', 'CG-177', 'CKH 10Tn', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11194', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C408', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6279, 0, 0, 0, 0, 'YPF', '', '6279', '', 'REPARACI?N', '', 'P', '1', 'LERDA C.', 'LH-1080', 'SBS 228', 'ARTICULACI?N  CTE CTRO-CTE CLA', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11284', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C471', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6280, 0, 0, 0, 0, 'YPF', '', '6280', '', 'FABRICACI?N', '23222 (1)', 'P', '1', 'LERDA C.', '', 'LUFKIN 456/640', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11282', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C466', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6281, 0, 0, 0, 0, 'YPF', '', '6281', '', 'FABRICACI?N', '22322 (2)', 'P', '2', 'LERDA C.', '', 'DARCO 320', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11287', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C366', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6282, 0, 0, 0, 0, 'YPF', '', '6282', '', 'FABRICACI?N', '22314 (1)', '', '1', 'LERDA C.', '0', 'LUFKIN C- 228', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11193', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C474', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6283, 0, 0, 0, 0, 'YPF', '', '6283', '', 'FABRICACI?N', '21315 (1)', 'P', '2', 'LERDA C.', '', 'SIAM 7Tn', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11283', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C476', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6284, 0, 0, 0, 0, 'YPF', '', '6284', '', 'FABRICACI?N', '22318 (1)', 'P', '1', 'LERDA C.', '', 'SIAM C III', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11290', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C405', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6285, 0, 0, 0, 0, 'YPF', '', '6285', '', 'FABRICACI?N', '22320 (2)', '', '2', 'LERDA C.', '', 'VULCAN 320', 'PPB', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11271', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C465', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6286, 0, 0, 0, 0, 'YPF', '', '6286', '', 'REPARACI?N', '', 'P', '1', 'LERDA C.', '', 'VULCAN 303', 'COJINETE DE CENTRO', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11291', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C464', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6287, 0, 0, 0, 0, 'YPF', '', '6287', '', 'REPARACI?N', '', 'P', '1', 'LERDA C.', '', 'SIAM 228', 'COJIENTE COLA', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11195', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C478', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6288, 0, 0, 0, 0, 'YPF', '', '6288', '', 'REPARACI?N', '', '', '1', 'QUINTEROS C.', '', 'STORCK 75/120', 'BOMBA ', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C479', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6289, 0, 0, 0, 0, 'YPF', '', '6289', '', 'REPARACI?N', '', '', '1', 'URZAGASTI S.', '', 'VULCAN UP 15 TN', 'CAJA REDUCTORA C/2 MANIVELAS + 2 PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '2', NULL, 'ARS', '2016', 'SER2016C480', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6290, 0, 0, 0, 0, 'SNP', '', '6290', '', 'REPARACI?N', '', '', '1', 'LOPEZ  J.', '', 'LUFKIN 640', 'COJENTE DE COLA (CHANCHITO)', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C481', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6291, 0, 0, 0, 0, 'YPF', '', '6291', '', 'PROVISI?N', '', '', '4', 'GODOY  G.', '', '', 'PLACAS', 'PL CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11281', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C482', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6292, 0, 0, 0, 0, 'YPF', '', '6292', '', 'REPARACI?N', '', '', '1', 'JEREZ D.', '', 'SIAM 228', 'AIB  COMPLETO', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C483', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6293, 0, 0, 0, 0, 'YPF', '', '6293', '', 'FABRICAR', '', '', '2', 'QUINTEROS P.', '', 'SIAM 228', 'CHASIS P/PCP', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11314', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C484', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6294, 0, 0, 0, 0, 'PAE', '', '6294', '', 'CAMBIO RUEDAS', '', '', '1', 'MARCE?UK  M.', '0', '', 'CARRO', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C485', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6295, 0, 0, 0, 0, 'PAE', '', '6295', '', 'SOLDAR Y MODIFICAR', '', '', '1', 'MARCE?UK  M.', '0', '', 'LLAVE GOLPE', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C448', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6296, 0, 0, 0, 0, 'YPF', '', '6296', '', 'REPARACI?N S/INDICACIONES', '', '', '1', 'HERNANDEZ  D.', '', '', 'FILTRO', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C250', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6297, 0, 0, 0, 0, 'YPF', '', '6297', '', 'AGUJEREADO S/INDICACIONES', '', 'Reyes', '2', 'REYES  P.', '', '', 'PLACAS', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C307', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6298, 0, 0, 0, 0, 'YPF', '', '6298', '', 'MODIFICACI?N', '', '', '1', 'CINCOTTA  A.', '', '', 'SISTEMA DE FRENO', 'LH', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11196', ''),
('', 0, '', '', '1', 'ALG', '3', NULL, 'ARS', '2016', 'SER2016C489', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6299, 0, 0, 0, 0, 'PAE', '', '6299', '', 'SOLDADO', '', '', '2', 'VEGA J.', '', '', 'PERFILES', 'KK', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 'VEGA', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C490', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6300, 0, 0, 0, 0, 'YPF', '', '6300', '', 'REPARACI?N (VIGA-CTE-CTRO-TRAVESA?O-BIELAS-PSB)', '', '', '1', 'REYES  P.', '', 'SIAM 228', 'ARTICULACI?N', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '683', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C491', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6301, 0, 0, 0, 0, 'YPF', '', '6301', '', 'REPARACI?N ', '', '', '1', 'JEREZ D.', '', 'SIAM 228', 'ARTICULACI?N', 'YT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11313', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C492', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6302, 0, 0, 0, 0, 'YPF', '', '6302', '', 'CAMBIO FERRODO', '', '', '1', 'URZAGASTI S.', 'LP-1802', 'SIAM 228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11302', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C493', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6303, 0, 0, 0, 0, 'YPF', '', '6303', '', 'CAMBIO FERRODO', '', '', '1', 'URZAGASTI S.', 'LP-214', 'MTD  9 TN', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11303', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C494', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6304, 0, 0, 0, 0, 'YPF', '', '6304', '', 'CAMBIO FERRODO', '', '', '1', 'URZAGASTI S.', 'LP-2330', 'LUFKIN 228/320', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11304', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C495', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6305, 0, 0, 0, 0, 'YPF', '', '6305', '', 'CAMBIO FERRODO', '', '', '1', 'URZAGASTI S.', 'LP-1137', 'DARCO 228Y', 'JGO PATINES DE FRENO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11305', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C496', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6306, 0, 0, 0, 0, 'YPF', '', '6306', '', 'CAMBIO FERRODO', '', '', '1', 'URZAGASTI S.', 'LP-557', 'DARCO 456', 'JGO PATINES DE FRENO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11306', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C497', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6307, 0, 0, 0, 0, 'YPF', '', '6307', '', 'CAMBIO FERRODO', '', 'P', '1', 'URZAGASTI S.', 'LP-2258', 'W-320', 'JGO PATINES DE FRENO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C498', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6308, 0, 0, 0, 0, 'YPF', '', '6308', '', 'CONST. ESPIGA', '', 'P', '1', 'URZAGASTI S.', 'LP-2370', 'LUFKIN 320', 'PPB', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C499', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6309, 0, 0, 0, 0, 'YPF', '', '6309', '', 'CAMBIO FERRODO', '', '', '1', 'URZAGASTI S.', 'LP-2023', 'SIAM 228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11300', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C500', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6310, 0, 0, 0, 0, 'YPF', '', '6310', '', 'PROVISION', '', '', '12', 'URZAGASTI S.', '', '', 'RETENES', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 'SPORTMAN', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C501', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6311, 0, 0, 0, 0, 'YPF', '', '6311', '', 'CAMBIO FERRODO', '', '', '1', 'URZAGASTI S.', '', 'SIAM 228', 'CINTA  DE FRENO ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11298', 'Oct-16'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C502', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6312, 0, 0, 0, 0, 'YPF', '', '6312', '', 'REPARAR ', '', 'P', '1', 'URZAGASTI S.', 'LP-238', 'SIAM C 228', 'ARTICULACI?N', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11320', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C503', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6313, 0, 0, 0, 0, 'YPF', '', '6313', '', 'REPARAR ', '', 'P', '1', 'URZAGASTI S.', 'LP-237', 'SIAM C 228', 'ARTICULACI?N', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11321', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C504', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6314, 0, 0, 0, 0, 'YPF', '', '6314', '', 'REPARAR ', '', '', '1', 'URZAGASTI S.', 'LP-1331', 'SIAM C 228', 'ARTICULACI?N', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11331', 'RETIRO TIP'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C505', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6315, 0, 0, 0, 0, 'YPF', '', '6315', '', 'REPARAR ', '', '', '1', 'URZAGASTI S.', 'LP-226', 'SIAM C 228', 'ARTICULACI?N COMPLETA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11331', 'RETIRO TIP'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C506', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6316, 0, 0, 0, 0, 'YPF', '', '6316', '', 'REPARAR ', '', '', '1', 'URZAGASTI S.', 'LM-210', 'SBS 320', 'ARTICULACI?N S/COJINETE COLA', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11331', 'RETIRO TIP'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C507', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6317, 0, 0, 0, 0, 'YPF', '', '6317', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'LC-186', 'LUFKIN 320', 'POSTE MAESTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11325', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C508', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6318, 0, 0, 0, 0, 'YPF', '', '6318', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'LP-20', 'LUFKIN 320', 'POSTE MAESTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11326', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C509', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6319, 0, 0, 0, 0, 'YPF', '', '6319', '', 'REPARAR', '', 'P', '1', 'URZAGASTI S.', 'AB-34', 'LUFKIN 320', 'COJINETE DE CENTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '11327', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C510', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6320, 0, 0, 0, 0, 'YPF', '', '6320', '', 'REPARAR', '', '', '1', 'URZAGASTI S.', '', 'VULCAN UP 15', 'CHASIS P/CAJA REDUCTORA N? 95223', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 'P/EP: 6289'),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C487', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6321, 0, 0, 0, 0, 'YPF', '', '6321', '', 'FABRICAR', '', 'P', '1', 'REYES  P.', '', 'SIAM 228', 'PPB', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C512', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6322, 0, 0, 0, 0, 'YPF', '', '6322', '', 'REPARAR', '', '', '1', 'SANCHEZ', '', 'VULCAN 303', 'AIB  COMPLETO', 'EG', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 1, '', '', '1', 'ALG', '1', 1, 'ARS', '2016', 'SER2016C513', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '21:00:00', '0000-00-00', '00:00:00', 6323, 0, 0, 0, 0, 'YPF', '', '6323', '', 'FREZAR CUBETA DIAM. EXT. S/MUESTRA', '', '', '1', 'RAMOS GUILLERMO', '', 'UNIVERSAL', 'RODAMIENTOS', 'CS', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C514', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6324, 0, 0, 0, 0, 'YPF', '', '6324', '', 'CAMBIO FERRODO', '', 'P', '1', 'URZAGASTI S.', '', 'DARCO 456', 'JGO PATINES DE FRENO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C515', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6325, 0, 0, 0, 0, 'YPF', '', '6325', '', 'REPARACION', '22222 (2)', 'P', '1', 'URZAGASTI S.', '', 'LUFKIN 320', 'COJINETE DE CENTRO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C516', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6326, 0, 0, 0, 0, 'YPF', '', '6326', '', 'REPARACION', '21315 (2)', 'P', '1', 'URZAGASTI S.', '', 'SIAM 228', 'EJE INTERMEDIO', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C517', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6327, 0, 0, 0, 0, 'YPF', '', '6327', '', 'REPARACION', '', 'P', '1', 'URZAGASTI S.', '', 'SIAM 228', 'EJE VELOZ', 'LP', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '1', 'ALG', '1', NULL, 'ARS', '2016', 'SER2016C511', 'CONT', 'ARG', '', 'C', '', 1, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6328, 0, 0, 0, 0, 'YPF', '', '6328', '', 'CONSTRUCCI?N CON DIAM. 4 Y6', '', '', '2', 'REYES  P.', '', '', 'TAPON ', 'PT', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', ''),
('', 0, '', '', '', '', '', 0, '', '', '', '', '', '', '', '', 0, 0, '0000-00-00', '00:00:00', '0000-00-00', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 6329, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarifas`
--

CREATE TABLE `tarifas` (
  `incporcentual` double NOT NULL,
  `inclineal` double NOT NULL,
  `aplicar_a` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `nombre` varchar(50) COLLATE utf8_bin NOT NULL,
  `mincoste` tinyint(1) DEFAULT '0',
  `maxpvp` tinyint(1) DEFAULT '0',
  `codtarifa` varchar(6) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agenciastrans`
--
ALTER TABLE `agenciastrans`
  ADD PRIMARY KEY (`codtrans`);

--
-- Indices de la tabla `agentes`
--
ALTER TABLE `agentes`
  ADD PRIMARY KEY (`codagente`);

--
-- Indices de la tabla `albaranescli`
--
ALTER TABLE `albaranescli`
  ADD PRIMARY KEY (`idalbaran`),
  ADD UNIQUE KEY `uniq_codigo_albaranescli` (`codigo`),
  ADD KEY `ca_albaranescli_series2` (`codserie`),
  ADD KEY `ca_albaranescli_ejercicios2` (`codejercicio`);

--
-- Indices de la tabla `albaranesprov`
--
ALTER TABLE `albaranesprov`
  ADD PRIMARY KEY (`idalbaran`),
  ADD UNIQUE KEY `uniq_codigo_albaranesprov` (`codigo`),
  ADD KEY `ca_albaranesprov_series2` (`codserie`),
  ADD KEY `ca_albaranesprov_ejercicios2` (`codejercicio`);

--
-- Indices de la tabla `almacenes`
--
ALTER TABLE `almacenes`
  ADD PRIMARY KEY (`codalmacen`);

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`referencia`),
  ADD KEY `ca_articulos_impuestos2` (`codimpuesto`);

--
-- Indices de la tabla `articulosprov`
--
ALTER TABLE `articulosprov`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uniq_articulo_proveedor2` (`codproveedor`,`refproveedor`);

--
-- Indices de la tabla `atributos`
--
ALTER TABLE `atributos`
  ADD PRIMARY KEY (`codatributo`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`codcliente`);

--
-- Indices de la tabla `contratoservicioscli`
--
ALTER TABLE `contratoservicioscli`
  ADD PRIMARY KEY (`idcontrato`);

--
-- Indices de la tabla `co_asientos`
--
ALTER TABLE `co_asientos`
  ADD PRIMARY KEY (`idasiento`),
  ADD KEY `ca_co_asientos_ejercicios2` (`codejercicio`);

--
-- Indices de la tabla `co_conceptospar`
--
ALTER TABLE `co_conceptospar`
  ADD PRIMARY KEY (`idconceptopar`);

--
-- Indices de la tabla `co_cuentas`
--
ALTER TABLE `co_cuentas`
  ADD PRIMARY KEY (`idcuenta`),
  ADD UNIQUE KEY `uniq_codcuenta` (`codcuenta`,`codejercicio`),
  ADD KEY `ca_co_cuentas_ejercicios` (`codejercicio`),
  ADD KEY `ca_co_cuentas_epigrafes2` (`idepigrafe`);

--
-- Indices de la tabla `co_cuentasesp`
--
ALTER TABLE `co_cuentasesp`
  ADD PRIMARY KEY (`idcuentaesp`);

--
-- Indices de la tabla `co_epigrafes`
--
ALTER TABLE `co_epigrafes`
  ADD PRIMARY KEY (`idepigrafe`),
  ADD KEY `ca_co_epigrafes_ejercicios` (`codejercicio`),
  ADD KEY `ca_co_epigrafes_gruposepigrafes2` (`idgrupo`);

--
-- Indices de la tabla `co_gruposepigrafes`
--
ALTER TABLE `co_gruposepigrafes`
  ADD PRIMARY KEY (`idgrupo`),
  ADD KEY `ca_co_gruposepigrafes_ejercicios` (`codejercicio`);

--
-- Indices de la tabla `co_regiva`
--
ALTER TABLE `co_regiva`
  ADD PRIMARY KEY (`idregiva`);

--
-- Indices de la tabla `co_secuencias`
--
ALTER TABLE `co_secuencias`
  ADD PRIMARY KEY (`idsecuencia`),
  ADD KEY `ca_co_secuencias_ejercicios` (`codejercicio`);

--
-- Indices de la tabla `co_subcuentas`
--
ALTER TABLE `co_subcuentas`
  ADD PRIMARY KEY (`idsubcuenta`),
  ADD UNIQUE KEY `uniq_codsubcuenta` (`codsubcuenta`,`codejercicio`),
  ADD KEY `ca_co_subcuentas_ejercicios` (`codejercicio`),
  ADD KEY `ca_co_subcuentas_cuentas2` (`idcuenta`);

--
-- Indices de la tabla `co_subcuentascli`
--
ALTER TABLE `co_subcuentascli`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ca_co_subcuentascli_ejercicios` (`codejercicio`),
  ADD KEY `ca_co_subcuentascli_clientes` (`codcliente`);

--
-- Indices de la tabla `cuentasbanco`
--
ALTER TABLE `cuentasbanco`
  ADD PRIMARY KEY (`codcuenta`);

--
-- Indices de la tabla `cuentasbcocli`
--
ALTER TABLE `cuentasbcocli`
  ADD PRIMARY KEY (`codcuenta`),
  ADD KEY `ca_cuentasbcocli_clientes` (`codcliente`);

--
-- Indices de la tabla `cuentasbcopro`
--
ALTER TABLE `cuentasbcopro`
  ADD PRIMARY KEY (`codcuenta`),
  ADD KEY `ca_cuentasbcopro_proveedores` (`codproveedor`);

--
-- Indices de la tabla `detalles_servicios`
--
ALTER TABLE `detalles_servicios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ca_detalle_servicio` (`idservicio`);

--
-- Indices de la tabla `dirclientes`
--
ALTER TABLE `dirclientes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ca_dirclientes_clientes` (`codcliente`);

--
-- Indices de la tabla `divisas`
--
ALTER TABLE `divisas`
  ADD PRIMARY KEY (`coddivisa`);

--
-- Indices de la tabla `documentosfac`
--
ALTER TABLE `documentosfac`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ejercicios`
--
ALTER TABLE `ejercicios`
  ADD PRIMARY KEY (`codejercicio`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estados_servicios`
--
ALTER TABLE `estados_servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `fabricantes`
--
ALTER TABLE `fabricantes`
  ADD PRIMARY KEY (`codfabricante`);

--
-- Indices de la tabla `facturascli`
--
ALTER TABLE `facturascli`
  ADD PRIMARY KEY (`idfactura`),
  ADD UNIQUE KEY `uniq_codigo_facturascli` (`codigo`),
  ADD KEY `ca_facturascli_series2` (`codserie`),
  ADD KEY `ca_facturascli_ejercicios2` (`codejercicio`),
  ADD KEY `ca_facturascli_asiento2` (`idasiento`),
  ADD KEY `ca_facturascli_asientop` (`idasientop`);

--
-- Indices de la tabla `facturasprov`
--
ALTER TABLE `facturasprov`
  ADD PRIMARY KEY (`idfactura`),
  ADD UNIQUE KEY `uniq_codigo_facturasprov` (`codigo`),
  ADD KEY `ca_facturasprov_series2` (`codserie`),
  ADD KEY `ca_facturasprov_ejercicios2` (`codejercicio`),
  ADD KEY `ca_facturasprov_asiento2` (`idasiento`),
  ADD KEY `ca_facturasprov_asientop` (`idasientop`);

--
-- Indices de la tabla `familias`
--
ALTER TABLE `familias`
  ADD PRIMARY KEY (`codfamilia`);

--
-- Indices de la tabla `formaspago`
--
ALTER TABLE `formaspago`
  ADD PRIMARY KEY (`codpago`);

--
-- Indices de la tabla `fs_extensions2`
--
ALTER TABLE `fs_extensions2`
  ADD PRIMARY KEY (`name`,`page_from`),
  ADD KEY `ca_fs_extensions2_fs_pages` (`page_from`);

--
-- Indices de la tabla `fs_logs`
--
ALTER TABLE `fs_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `fs_pages`
--
ALTER TABLE `fs_pages`
  ADD PRIMARY KEY (`name`);

--
-- Indices de la tabla `fs_users`
--
ALTER TABLE `fs_users`
  ADD PRIMARY KEY (`nick`),
  ADD KEY `ca_fs_users_pages` (`fs_page`);

--
-- Indices de la tabla `fs_vars`
--
ALTER TABLE `fs_vars`
  ADD PRIMARY KEY (`name`);

--
-- Indices de la tabla `gruposclientes`
--
ALTER TABLE `gruposclientes`
  ADD PRIMARY KEY (`codgrupo`);

--
-- Indices de la tabla `impuestos`
--
ALTER TABLE `impuestos`
  ADD PRIMARY KEY (`codimpuesto`);

--
-- Indices de la tabla `lineasfacturascli`
--
ALTER TABLE `lineasfacturascli`
  ADD PRIMARY KEY (`idlinea`),
  ADD KEY `ca_linea_facturascli2` (`idfactura`);

--
-- Indices de la tabla `lineasfacturasprov`
--
ALTER TABLE `lineasfacturasprov`
  ADD PRIMARY KEY (`idlinea`),
  ADD KEY `ca_linea_facturasprov2` (`idfactura`);

--
-- Indices de la tabla `lineasservicioscli`
--
ALTER TABLE `lineasservicioscli`
  ADD PRIMARY KEY (`idlinea`),
  ADD KEY `ca_lineasservicioscli_servicioscli` (`idservicio`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`codpais`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`codproveedor`);

--
-- Indices de la tabla `recetas`
--
ALTER TABLE `recetas`
  ADD PRIMARY KEY (`codreceta`);

--
-- Indices de la tabla `secuencias`
--
ALTER TABLE `secuencias`
  ADD PRIMARY KEY (`idsec`),
  ADD KEY `ca_secuencias_secuenciasejercicios` (`id`);

--
-- Indices de la tabla `secuenciasejercicios`
--
ALTER TABLE `secuenciasejercicios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ca_secuenciasejercicios_ejercicios` (`codejercicio`);

--
-- Indices de la tabla `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`codserie`);

--
-- Indices de la tabla `servicioscli`
--
ALTER TABLE `servicioscli`
  ADD PRIMARY KEY (`idservicio`),
  ADD UNIQUE KEY `uniq_codigo_servicioscli` (`codigo`),
  ADD KEY `ca_servicioscli_series` (`codserie`),
  ADD KEY `ca_servicioscli_ejercicios` (`codejercicio`),
  ADD KEY `ca_servicios_albaranescli` (`idalbaran`);

--
-- Indices de la tabla `tarifas`
--
ALTER TABLE `tarifas`
  ADD PRIMARY KEY (`codtarifa`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `albaranescli`
--
ALTER TABLE `albaranescli`
  MODIFY `idalbaran` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `albaranesprov`
--
ALTER TABLE `albaranesprov`
  MODIFY `idalbaran` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `articulosprov`
--
ALTER TABLE `articulosprov`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `contratoservicioscli`
--
ALTER TABLE `contratoservicioscli`
  MODIFY `idcontrato` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `co_asientos`
--
ALTER TABLE `co_asientos`
  MODIFY `idasiento` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `co_cuentas`
--
ALTER TABLE `co_cuentas`
  MODIFY `idcuenta` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `co_epigrafes`
--
ALTER TABLE `co_epigrafes`
  MODIFY `idepigrafe` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `co_gruposepigrafes`
--
ALTER TABLE `co_gruposepigrafes`
  MODIFY `idgrupo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `co_regiva`
--
ALTER TABLE `co_regiva`
  MODIFY `idregiva` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `co_secuencias`
--
ALTER TABLE `co_secuencias`
  MODIFY `idsecuencia` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `co_subcuentas`
--
ALTER TABLE `co_subcuentas`
  MODIFY `idsubcuenta` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `co_subcuentascli`
--
ALTER TABLE `co_subcuentascli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `detalles_servicios`
--
ALTER TABLE `detalles_servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `dirclientes`
--
ALTER TABLE `dirclientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `documentosfac`
--
ALTER TABLE `documentosfac`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `facturascli`
--
ALTER TABLE `facturascli`
  MODIFY `idfactura` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `facturasprov`
--
ALTER TABLE `facturasprov`
  MODIFY `idfactura` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `fs_logs`
--
ALTER TABLE `fs_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT de la tabla `lineasfacturascli`
--
ALTER TABLE `lineasfacturascli`
  MODIFY `idlinea` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `lineasfacturasprov`
--
ALTER TABLE `lineasfacturasprov`
  MODIFY `idlinea` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `lineasservicioscli`
--
ALTER TABLE `lineasservicioscli`
  MODIFY `idlinea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `secuencias`
--
ALTER TABLE `secuencias`
  MODIFY `idsec` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `secuenciasejercicios`
--
ALTER TABLE `secuenciasejercicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `servicioscli`
--
ALTER TABLE `servicioscli`
  MODIFY `idservicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6358;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `albaranescli`
--
ALTER TABLE `albaranescli`
  ADD CONSTRAINT `ca_albaranescli_ejercicios2` FOREIGN KEY (`codejercicio`) REFERENCES `ejercicios` (`codejercicio`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ca_albaranescli_series2` FOREIGN KEY (`codserie`) REFERENCES `series` (`codserie`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `albaranesprov`
--
ALTER TABLE `albaranesprov`
  ADD CONSTRAINT `ca_albaranesprov_ejercicios2` FOREIGN KEY (`codejercicio`) REFERENCES `ejercicios` (`codejercicio`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ca_albaranesprov_series2` FOREIGN KEY (`codserie`) REFERENCES `series` (`codserie`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD CONSTRAINT `ca_articulos_impuestos2` FOREIGN KEY (`codimpuesto`) REFERENCES `impuestos` (`codimpuesto`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `articulosprov`
--
ALTER TABLE `articulosprov`
  ADD CONSTRAINT `ca_articulosprov_proveedores` FOREIGN KEY (`codproveedor`) REFERENCES `proveedores` (`codproveedor`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `co_asientos`
--
ALTER TABLE `co_asientos`
  ADD CONSTRAINT `ca_co_asientos_ejercicios2` FOREIGN KEY (`codejercicio`) REFERENCES `ejercicios` (`codejercicio`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `co_cuentas`
--
ALTER TABLE `co_cuentas`
  ADD CONSTRAINT `ca_co_cuentas_ejercicios` FOREIGN KEY (`codejercicio`) REFERENCES `ejercicios` (`codejercicio`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ca_co_cuentas_epigrafes2` FOREIGN KEY (`idepigrafe`) REFERENCES `co_epigrafes` (`idepigrafe`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `co_epigrafes`
--
ALTER TABLE `co_epigrafes`
  ADD CONSTRAINT `ca_co_epigrafes_ejercicios` FOREIGN KEY (`codejercicio`) REFERENCES `ejercicios` (`codejercicio`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ca_co_epigrafes_gruposepigrafes2` FOREIGN KEY (`idgrupo`) REFERENCES `co_gruposepigrafes` (`idgrupo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `co_gruposepigrafes`
--
ALTER TABLE `co_gruposepigrafes`
  ADD CONSTRAINT `ca_co_gruposepigrafes_ejercicios` FOREIGN KEY (`codejercicio`) REFERENCES `ejercicios` (`codejercicio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `co_secuencias`
--
ALTER TABLE `co_secuencias`
  ADD CONSTRAINT `ca_co_secuencias_ejercicios` FOREIGN KEY (`codejercicio`) REFERENCES `ejercicios` (`codejercicio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `co_subcuentas`
--
ALTER TABLE `co_subcuentas`
  ADD CONSTRAINT `ca_co_subcuentas_cuentas2` FOREIGN KEY (`idcuenta`) REFERENCES `co_cuentas` (`idcuenta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ca_co_subcuentas_ejercicios` FOREIGN KEY (`codejercicio`) REFERENCES `ejercicios` (`codejercicio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `co_subcuentascli`
--
ALTER TABLE `co_subcuentascli`
  ADD CONSTRAINT `ca_co_subcuentascli_clientes` FOREIGN KEY (`codcliente`) REFERENCES `clientes` (`codcliente`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ca_co_subcuentascli_ejercicios` FOREIGN KEY (`codejercicio`) REFERENCES `ejercicios` (`codejercicio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cuentasbcocli`
--
ALTER TABLE `cuentasbcocli`
  ADD CONSTRAINT `ca_cuentasbcocli_clientes` FOREIGN KEY (`codcliente`) REFERENCES `clientes` (`codcliente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cuentasbcopro`
--
ALTER TABLE `cuentasbcopro`
  ADD CONSTRAINT `ca_cuentasbcopro_proveedores` FOREIGN KEY (`codproveedor`) REFERENCES `proveedores` (`codproveedor`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `detalles_servicios`
--
ALTER TABLE `detalles_servicios`
  ADD CONSTRAINT `ca_detalle_servicio` FOREIGN KEY (`idservicio`) REFERENCES `servicioscli` (`idservicio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `dirclientes`
--
ALTER TABLE `dirclientes`
  ADD CONSTRAINT `ca_dirclientes_clientes` FOREIGN KEY (`codcliente`) REFERENCES `clientes` (`codcliente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `facturascli`
--
ALTER TABLE `facturascli`
  ADD CONSTRAINT `ca_facturascli_asiento2` FOREIGN KEY (`idasiento`) REFERENCES `co_asientos` (`idasiento`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `ca_facturascli_asientop` FOREIGN KEY (`idasientop`) REFERENCES `co_asientos` (`idasiento`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `ca_facturascli_ejercicios2` FOREIGN KEY (`codejercicio`) REFERENCES `ejercicios` (`codejercicio`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ca_facturascli_series2` FOREIGN KEY (`codserie`) REFERENCES `series` (`codserie`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `facturasprov`
--
ALTER TABLE `facturasprov`
  ADD CONSTRAINT `ca_facturasprov_asiento2` FOREIGN KEY (`idasiento`) REFERENCES `co_asientos` (`idasiento`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `ca_facturasprov_asientop` FOREIGN KEY (`idasientop`) REFERENCES `co_asientos` (`idasiento`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `ca_facturasprov_ejercicios2` FOREIGN KEY (`codejercicio`) REFERENCES `ejercicios` (`codejercicio`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ca_facturasprov_series2` FOREIGN KEY (`codserie`) REFERENCES `series` (`codserie`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `fs_extensions2`
--
ALTER TABLE `fs_extensions2`
  ADD CONSTRAINT `ca_fs_extensions2_fs_pages` FOREIGN KEY (`page_from`) REFERENCES `fs_pages` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `fs_users`
--
ALTER TABLE `fs_users`
  ADD CONSTRAINT `ca_fs_users_pages` FOREIGN KEY (`fs_page`) REFERENCES `fs_pages` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `lineasfacturascli`
--
ALTER TABLE `lineasfacturascli`
  ADD CONSTRAINT `ca_linea_facturascli2` FOREIGN KEY (`idfactura`) REFERENCES `facturascli` (`idfactura`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `lineasfacturasprov`
--
ALTER TABLE `lineasfacturasprov`
  ADD CONSTRAINT `ca_linea_facturasprov2` FOREIGN KEY (`idfactura`) REFERENCES `facturasprov` (`idfactura`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `lineasservicioscli`
--
ALTER TABLE `lineasservicioscli`
  ADD CONSTRAINT `ca_lineasservicioscli_servicioscli` FOREIGN KEY (`idservicio`) REFERENCES `servicioscli` (`idservicio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `secuencias`
--
ALTER TABLE `secuencias`
  ADD CONSTRAINT `ca_secuencias_secuenciasejercicios` FOREIGN KEY (`id`) REFERENCES `secuenciasejercicios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `secuenciasejercicios`
--
ALTER TABLE `secuenciasejercicios`
  ADD CONSTRAINT `ca_secuenciasejercicios_ejercicios` FOREIGN KEY (`codejercicio`) REFERENCES `ejercicios` (`codejercicio`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
